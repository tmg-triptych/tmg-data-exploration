-- Code for quick research  - see the difference between 1439520 and 1433166, I don�t know who Jake downloaded for
-- In the database [dbMarekEF_production].[dbo].[DownloadHistories]dl 
-- This action is recorded to userid that is associated with [dbMarekCommerceManager_production].[dbo].[aspnet_Membership]a
SELECT
	[Id]
	,[EpiLineItemId]
	,dl.[UserId]
	,[CmbrPath]
	,[SiteId]
	,[Filename]
	,[VersionName]
	,[IsRetired]
	,cls.Email as USERACCOUNT
	,a.Email AS THEPERSONDOINGTHEIMPERSONATION
FROM [dbMarekEF_production].[dbo].[DownloadHistories]dl
left join  [dbMarekCommerceManager_production].[dbo].[cls_Contact] cls with(nolock) on dl.UserId = cls.ContactId
LEFT JOIN [dbMarekCommerceManager_production].[dbo].[aspnet_Membership]a with(nolock) on dl.UserId = a.UserId
where siteid = 79119
--where cls.Email IS NULL
order by creationdate DESC

-- I also used this query to try and learn more about the use activities and where and when we tie the 2 different membership databases
GO
SELECT TOP (5000)
  cls.email,
	mb.Email as PersonWhoIsDoingTheStuff,
	ua.* ,
	uat.Description,
	uat.EnumName 
FROM [dbMarekTrackingEF_production].[dbo].[UserActivities] ua with(nolock)
LEFT JOIn [dbMarekTrackingEF_production].[dbo].[UserActivitiesTypes] uat with(nolock) on uat.id = ua.ActivityTypeId
LEFT JOIN dbMarekCommerceManager_production.[dbo].[cls_Contact] cls with(nolock) on ua.UserId = cls.ContactId
left join dbMarekCommerceManager_production.[dbo].aspnet_Membership mb with(nolock) on ua.ImpersonationId = mb.UserId
where ua.ImpersonationId IS NOT NULL and siteid = 79119  --123399-
order by CreationDate DESC
--and siteid = 79119

-- Where are using this code to track CART orders to Impersonation scenarions

select distinct 
  cls.Email as UserLogon
	,mb.Email as ImpersonationUserLogon
	,ut.OrderId
	,ut.OrderTrackNumber
	,ut.UserId as ContactID
	,ut.ImpersonationUserId
	,ut.AdditionInformation
	,oh.SiteId
	,s.CatalogId
	,dateadd(hh,-6,ua.CreationDate) as ImpersonationDate
from [dbMarekTrackingEF_production].[dbo].[UserActivities] ua with(nolock)
join [dbMarekTrackingEF_production].[dbo].[Utilizations] ut with(nolock) on ua.Id = ut.UserActivityId
join dbMarekCommerceManager_production.[dbo].[cls_Contact] cls with(nolock) on ua.UserId = cls.ContactId
left join dbMarekCommerceManager_production.[dbo].aspnet_Membership mb with(nolock) on ua.ImpersonationId = mb.UserId
join [dbMarekEF_production].[dbo].[OrderHistories] oh with(nolock) on ut.OrderTrackNumber = oh.OrderTrackId
join (
	select sd.StartPage as SiteId
	, CatalogId
	, c.Name 
	from [dbMarekCMS_production].[dbo].tblContentProperty cp with(nolock)
	inner join [dbMarekCommerceManager_production].[dbo].[Catalog] c with(nolock) on cp.LinkGuid = c.ContentGuid
	inner join [dbMarekCMS_production].[dbo].tblSiteDefinition sd with(nolock) on sd.StartPage = cp.fkContentId
	where cp.fkPropertyDefinitionID = (
		SELECT [pkID]  
		FROM [dbMarekCMS_production].[dbo].[tblPropertyDefinition]   with(nolock)
		where name = 'CommerceCatalog'  and fkContentTypeID = (
			select pkId 
			from [dbMarekCMS_production].dbo.tblContentType  with(nolock)
			where name = 'StartPage')
			)
) as s on s.SiteId = oh.SiteId
where ActivityTypeId = 1
	and oh.SiteId = 79119

-------

SELECT TOP 5 * FROM dbMarekEF_production..OrderHistories oh ORDER BY EpiOrderId DESC
SELECT TOP 5 * FROM dbMarekEF_production..DownloadHistories dh ORDER BY Id DESC
SELECT TOP 5 * FROM dbMarekTrackingEF_production..UserActivities ORDER BY Id DESC
SELECT TOP 5 * FROM dbMarekTrackingEF_production..Engagements ORDER BY Id DESC
SELECT TOP 5 * FROM dbMarekTrackingEF_production..Utilizations ORDER BY Id DESC

-- User: ContactId = B1452180-2B21-49CB-A178-1BB597EFFF56, UserId = 4A15692D-C7A6-4626-BF4C-7310683172CA
SELECT
  up.UserId, am.UserId, am.Email, cc.Email, cc.ContactId
FROM dbMarekCommerceManager_production..aspnet_Membership am
JOIN dbMarekEF_production..UserProfile up ON up.UserId = am.UserId
JOIN dbMarekCommerceManager_production..cls_Contact cc ON am.Email = cc.Email 
WHERE cc.ContactId = 'B1452180-2B21-49CB-A178-1BB597EFFF56'


--	Download ID 1433166 is Jake doing a standard order on my own behalf. No Impersonation. 
-- in UserId column, user X is referenced by ContactId
SELECT * FROM dbMarekEF_production..DownloadHistories dh WHERE Id = 1433166
-- UserId = B1452180-2B21-49CB-A178-1BB597EFFF56

--	Download ID 1439520 was Jake downloading an item while impersonating Darryl   (this is scenario in question)
-- in UserId column, user jacob.gould@triptych.com is referenced by UserId
SELECT * FROM dbMarekEF_production..DownloadHistories dh WHERE Id = 1439520
-- UserId = 4A15692D-C7A6-4626-BF4C-7310683172CA



-- Mine
-- ua records with impersonation after release
SELECT
  *
FROM dbMarekTrackingEF_production..UserActivities ua
WHERE ImpersonationId IS NOT NULL AND ActivityTypeId = 10 AND  CreationDate > '2021-10-27 08:30' ORDEr BY CreationDate DESC

SELECT
  DownloadProductUrl
FROM dbMarekTrackingEF_production..Engagements
WHERE UserActivityId = 10684919

SELECT
  *
FROM dbMarekTrackingEF_production..Engagements

