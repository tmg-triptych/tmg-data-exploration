-- Initial code by Jake
DROP TABLE IF EXISTS #test;
SELECT
  dh.Id AS dhId,
	eng.Id AS engId,
	ua.Id AS uaId,
	uat.Id AS uatId,
	dh.CreationDate AS dhCreationDate,
	ua.CreationDate AS uaCreationDate
INTO #test
FROM [dbMarekEF_production].[dbo].[DownloadHistories] dh with(nolock)
LEFT JOIN [dbMarekTrackingEF_production].[dbo].[Engagements] eng with(nolock) on
  dh.CmbrPath = eng.DownloadProductUrl
left join [dbMarekTrackingEF_production].[dbo].[UserActivities] ua with(nolock) on
  eng.UserActivityId = ua.Id and
	datepart(hour,dh.CreationDate) = datepart(hour,ua.CreationDate) and
	datepart(minute,dh.CreationDate) = datepart(minute,ua.CreationDate) 
	--and datepart(SECOND,dh.CreationDate) = datepart(second,ua.CreationDate)
left join [dbMarekTrackingEF_production].[dbo].[UserActivitiesTypes] uat with(nolock) on 
	uat.Id = ua.ActivityTypeId
where
  dh.CreationDate > '2021-10-27' and
  dh.SiteId= 123399 and
  ua.ImpersonationId IS NOT NULL and
  ua.ActivityTypeId = 10

-- For now there's no problems
SELECT COUNT(*), COUNT(DISTINCT dhId), COUNT(DISTINCT engId), COUNT(DISTINCT uaId), COUNT(DISTINCT uatId) FROM #test
-- 106	106	106	106	1

SELECT TOP 1 * FROM dbMarekEF_production..DownloadHistories;
SELECT TOP 1 * FROM dbMarekTrackingEF_production..Engagements;
SELECT TOP 1 * FROM dbMarekTrackingEF_production..UserActivities;
SELECT TOP 1 * FROM dbMarekTrackingEF_production..UserActivitiesTypes;


-- dbMarekTrackingEF_production..Engagements can be joined just by UserActivityId
SELECT COUNT(UserActivityId), COUNT(DISTINCT UserActivityId) FROM dbMarekTrackingEF_production..Engagements;
-- 1648338	1648338
SELECT COUNT(DownloadProductUrl), COUNT(DISTINCT DownloadProductUrl) FROM dbMarekTrackingEF_production..Engagements;
-- 1553426	590834

-- compare two dates
SELECT res, COUNT(*) FROM (SELECT CASE WHEN uaCreationDate >= dhCreationDate THEN 1 ELSE 0 END AS res FROM #test) c GROUP BY res;

-- New version
DROP TABLE IF EXISTS #test2;
WITH DhUa_cte (dhId, uaId) AS (
  SELECT
	  dh.Id AS dhId,
		MIN(ua.Id) AS uaId
	FROM dbMarekEF_production..DownloadHistories dh
	LEFT JOIN dbMarekTrackingEF_production..UserActivities ua ON
	  ua.UserId = dh.UserId AND
		ua.CreationDate >= dh.CreationDate AND
    ua.ActivityTypeId = 10
	GROUP BY dh.Id
)
SELECT
  dh.Id AS dhId,
	eng.Id AS engId,
	ua.Id AS uaId,
	dh.CreationDate AS dhCreationDate,
	ua.CreationDate AS uaCreationDate
INTO #test2
FROM dbMarekEF_production..DownloadHistories dh with(nolock)
LEFT JOIN DhUa_cte ON DhUa_cte.dhId = dh.Id
LEFT JOIN dbMarekTrackingEF_production..UserActivities ua with(nolock) on ua.Id = DhUa_cte.uaId
LEFT JOIN dbMarekTrackingEF_production..Engagements eng ON eng.UserActivityId = ua.Id
WHERE
  dh.CreationDate > '2021-10-27' and
  dh.SiteId= 123399 and
  ua.ImpersonationId IS NOT NULL and
  ua.ActivityTypeId = 10

-- TEST RESULTS
SELECT TOP 5 * FROM #test;
SELECT TOP 5 * FROM #test2;
SELECT
  COUNT(*),
	COUNT(old.engId),
	COUNT(DISTINCT old.engId),
	COUNT(new.engId),
	COUNT(DISTINCT new.engId)
FROM #test old
FULL OUTER JOIN #test2 new ON
  old.dhId = new.dhId AND
	old.engId = new.engId AND
	old.uaId = new.uaId
;