SELECT TOP 5 * FROM dbMarekEF_UAT..OrderHistories oh ORDER BY EpiOrderId DESC
SELECT TOP 5 * FROM dbMarekEF_UAT..DownloadHistories dh ORDER BY Id DESC
SELECT TOP 5 * FROM dbMarekTrackingEF_UAT..UserActivities ORDER BY Id DESC

-- User X ("admin@yourcompany.com", ContactId = 'FB43820E-6037-45A0-968E-F635AA05C499', UserId = 'A56A6424-0555-494B-9B2D-AD20300529C1')
SELECT
  up.UserId, am.UserId, am.Email, cc.Email, cc.ContactId
FROM dbMarekCommerceManager_UAT..aspnet_Membership am
JOIN dbMarekEF_UAT..UserProfile up ON up.UserId = am.UserId
JOIN dbMarekCommerceManager_UAT..cls_Contact cc ON am.Email = cc.Email 
WHERE cc.ContactId = 'FB43820E-6037-45A0-968E-F635AA05C499'

-- User X ("admin@yourcompany.com", ContactId = '37901574-1780-4102-A632-72613E1B9BC3', UserId = 'CCE991D6-A0E4-40C3-B2A9-3C906AB0F03F')
SELECT
  up.UserId, am.UserId, am.Email, cc.Email, cc.ContactId
FROM dbMarekCommerceManager_UAT..aspnet_Membership am
JOIN dbMarekEF_UAT..UserProfile up ON up.UserId = am.UserId
JOIN dbMarekCommerceManager_UAT..cls_Contact cc ON am.Email = cc.Email 
WHERE cc.ContactId = '37901574-1780-4102-A632-72613E1B9BC3'

--Scenario 1: User X just downloads "Build TV Spot & Video", at 2021-10-28 16:12 (UTC)

-- in UserId column, user X is referenced by ContactId
SELECT * FROM dbMarekEF_UAT..DownloadHistories WHERE Id = 1119
-- UserId = FB43820E-6037-45A0-968E-F635AA05C499 - X is referenced by ContactId

-- in UserId column, user X is referenced by ContactId
SELECT * FROM dbMarekTrackingEF_UAT..UserActivities WHERE Id = 32464
-- ImpersonationId = NULL
-- UserId = FB43820E-6037-45A0-968E-F635AA05C499 - X is referenced by ContactId


--Scenario 2: User X Impersonates as Y downloads "Build TV Spot & Video", at 2021-10-28 18:30 (UTC)

-- in UserId column, user Y is referenced by ContactId
SELECT * FROM dbMarekEF_UAT..DownloadHistories WHERE Id = 1122
-- UserId =          37901574-1780-4102-A632-72613E1B9BC3 (Y referenced by ContactId)

-- in UserId column, user Y is referenced by ContactId
-- in ImpersonationId column, user X is referenced by ContactId
SELECT * FROM dbMarekTrackingEF_UAT..UserActivities WHERE Id = 32476 AND ActivityTypeId = 10 -- EngagementDownload
-- ImpersonationId = A56A6424-0555-494B-9B2D-AD20300529C1 (X referenced by ContactId)
-- UserId =          37901574-1780-4102-A632-72613E1B9BC3 (Y referenced by ContactId)


---------- Link DownloadHistories with UserActivities for impersonated records
SELECT * FROM dbMarekEF_UAT..DownloadHistories WHERE Id = 1122 -- 2021-10-28 18:30:30.393
SELECT * FROM dbMarekTrackingEF_UAT..Engagements WHERE DownloadProductUrl = 'cmbr://reference/a790b644-023d-4576-9efe-00b3d1a5eaee' ORDER BY UserActivityId DESC
SELECT * FROM dbMarekTrackingEF_UAT..UserActivities WHERE Id = 32476 AND ActivityTypeId = 10 -- 2021-10-28 18:30:30.410

