USE ExploreData

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
DROP PROCEDURE IF EXISTS Show_Cardinalities;
GO
CREATE PROCEDURE Show_Cardinalities @t1 VARCHAR(MAX), @t2 VARCHAR(MAX), @t1Id VARCHAR(MAX), @t2Id VARCHAR(MAX), @joinKey VARCHAR(MAX)
AS
BEGIN
  DROP TABLE IF EXISTS tempCounters;
	DECLARE @sql VARCHAR(MAX);
	SET @sql = '
	SELECT * INTO tempCounters FROM (
		
		SELECT ''' + @t1 + ''' AS tableName, ''TotalRecords'' AS metric, COUNT(*) value FROM ' + @t1 + '
		
		UNION ALL
		
		SELECT
		  ''' + @t1 + ''' AS tableName,
			''..having ''+ t2RecordsCount + '' matching records in ' + @t2 + '''  AS metric,
			COUNT(*) AS value
		FROM (
			SELECT
				t1.Id,
				CASE
					WHEN COUNT(DISTINCT t2.' + @t2Id + ') = 0 THEN ''0''
					WHEN COUNT(DISTINCT t2.' + @t2Id + ') = 1 THEN ''1''
					WHEN COUNT(DISTINCT t2.' + @t2Id + ') > 1 THEN ''many''
				END AS t2RecordsCount
			FROM ' + @t1 + ' t1
			LEFT JOIN ' + @t2 + ' t2 ON ' + @joinKey + '
			GROUP BY t1.Id
		) res
		GROUP BY t2RecordsCount
	) r;'
	EXEC (@sql)
	SELECT * FROM tempCounters ORDER BY metric DESC;

END
GO


-- Usage example
EXECUTE ExploreData..Show_Cardinalities @t1 = 'dbMarekEF_production..CoopBudgets', @t2 = 'dbMarekEF_production..CoopBudgetUserPercentLogs', @t1Id='Id', @t2Id='Id', @joinKey = 't1.Id = t2.CoopBudgetId'
EXECUTE ExploreData..Show_Cardinalities @t1 = 'dbMarekEF_production..CoopBudgetUserPercentLogs', @t2 = 'dbMarekEF_production..CoopBudgets', @t1Id='Id', @t2Id='Id', @joinKey = 't1.CoopBudgetId = t2.Id'


