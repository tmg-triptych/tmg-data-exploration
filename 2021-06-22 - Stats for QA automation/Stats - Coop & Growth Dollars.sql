USE ExploreData;

-- Orders by Site & Coop usage
DROP TABLE #OrdersTable;
SELECT
  sd.Name AS SiteName,
	oh.SiteId,
	YEAR(oh.CreationDate) AS OrderYear, 
	CASE WHEN oh.Coop > 0 THEN 1 ELSE 0 END AS IsUsingCoop,
	oh.Total AS OrderTotal,
  oh.Coop AS OrderCoop,
	CASE
	  WHEN COALESCE(oh.Total, 0) + COALESCE(oh.Coop, 0) > 0 THEN 
			ROUND(100*COALESCE(oh.Total, 0)/(COALESCE(oh.Total, 0) + COALESCE(oh.Coop, 0)), 0)
		ELSE 0
	END AS OrderPercPaidByCoop
INTO #OrdersTable
FROM dbMarekEF_production..OrderHistories oh 
LEFT JOIN dbMarekCMS_production..tblSiteDefinition sd ON sd.StartPage = oh.SiteId
;

-- Build pivot
SELECT
  SiteName,
	OrderYear,
	COUNT(*) AS OrdersCount,
	SUM(IsUsingCoop) AS OrdersPaidByCoopCount,
	SUM(OrderTotal) AS OrdersTotalSum,
  SUM(OrderCoop) AS OrdersCoopSum
FROM #OrdersTable
GROUP BY SiteName, OrderYear
ORDER BY SiteName, OrderYear
;