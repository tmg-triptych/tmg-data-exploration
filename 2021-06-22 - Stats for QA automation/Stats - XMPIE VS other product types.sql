USE ExploreData;

-- Products in Catalogs by Catalog
DROP TABLE #ProductsTable;
SELECT
  c.Name,
	ce.CatalogEntryId,
	ce.ClassTypeId,
	CASE
	  WHEN mc.FriendlyName IN (
		  'Xmpie Downloaded Variation',
			'XMPie VDP Variation',
			'XMPieCampaignProduct',
			'XMPieProduct'
		) THEN mc.FriendlyName
		ELSE 'Other type'
	END AS ProductType
INTO #ProductsTable
FROM dbMarekCommerceManager_production..CatalogEntry ce
LEFT JOIN dbMarekCommerceManager_production..Catalog c ON c.CatalogId = ce.CatalogId
LEFT JOIN dbMarekCommerceManager_production..MetaClass mc ON ce.MetaClassId = mc.MetaClassId
;

-- Build pivot
SELECT
  Name,
	ClassTypeId,
	ProductType,
	COUNT(*)
FROM #ProductsTable
WHERE ClassTypeId IN (
  'Product',
  'Variation'
)
GROUP BY ClassTypeId, Name, ProductType
ORDER BY ClassTypeId, Name, ProductType
;

-- Products selled by Site
DROP TABLE #ProductsSelledTable;
SELECT
	sd.Name AS SiteName,
	YEAR(po.CreationDate) AS OrderYear, 
	ce.CatalogEntryId,
	CASE
	  WHEN mc.FriendlyName IN (
		  'Xmpie Downloaded Variation',
			'XMPie VDP Variation',
			'XMPieCampaignProduct',
			'XMPieProduct'
		) THEN mc.FriendlyName
		ELSE 'Other type'
	END AS ProductType,
	po.Quantity
INTO #ProductsSelledTable
FROM (
  SELECT
	  oh.SiteId, oich.ToolId, oich.Quantity*oih.Quantity AS Quantity, oh.CreationDate
  FROM dbMarekEF_production..OrderHistories oh
  JOIN dbMarekEF_production..OrderItemsHistories oih ON oih.EpiOrderId = oh.EpiOrderId
  JOIN dbMarekEF_production..OrderItemComponentsHistories oich ON oih.EpiLineItemId = oich.EpiLineItemId
	UNION ALL
	SELECT
	  SiteId, ToolId, 1 AS Quantity, CreationDate
	FROM dbMarekEF_production..DownloadHistories
  WHERE EpiLineItemId IS NULL
) po
JOIN dbMarekCommerceManager_production..CatalogEntry ce ON ce.ContentGuid = po.ToolId
LEFT JOIN dbMarekCommerceManager_production..MetaClass mc ON ce.MetaClassId = mc.MetaClassId
LEFT JOIN dbMarekCMS_production..tblSiteDefinition sd ON sd.StartPage = po.SiteId
;

-- Build pivot
SELECT
  SiteName,
	OrderYear,
	ProductType,
	ROUND(SUM(Quantity), 0) AS SelledQuantity
FROM #ProductsSelledTable
WHERE SiteName IS NOT NULL
GROUP BY SiteName, OrderYear, ProductType
ORDER BY SiteName, OrderYear DESC, ProductType DESC
;