------------------------------------------------------
-- How much minutes is there between events?
-- first treated it like standart distribution, but it's not
-- if we just take mean + 3stddev, there atill will be events with more time since last, because of weekends & holidays
-- expectation has to be adjusted to max historical delay time

-- dbMarekCMS_production..tblWorkContent
-- ERROR if last update is > than 120 hours
-- WARNING if last update is > than 9 hours
SELECT
  ROUND(MAX(diff_minutes) / 60, 1) AS max_hours,
  ROUND(AVG(diff_minutes) / 60, 1) AS avg_hours,
  ROUND(STDEV(diff_minutes) / 60, 1)  AS stddev_hours
FROM (
	SELECT
	  DATEPART(YEAR, Created) yyyyy,
	  DATEPART(MONTH, Created) mm,
	  DATEPART(HOUR, Created) hh,
	  DATEDIFF(MINUTE, LAG(Created) OVER (PARTITION BY 1 ORDER BY Created), Created) diff_minutes
	FROM dbMarekCMS_production..tblWorkContent
	WHERE Created > '2019-01-01'
) res
;
-- max_hours	avg_hours	stddev_hours
-- 112	0	2,9

-- dbMarekTrackingEF_production..UserActivities
-- ERROR if last update is > than 8 hours
-- WARNING if last update is > than 1 hours
SELECT
  ROUND(MAX(diff_minutes) / 60, 1) AS max_hours,
  ROUND(AVG(diff_minutes) / 60, 1) AS avg_hours,
  ROUND(STDEV(diff_minutes) / 60, 1)  AS stddev_hours
FROM (
	SELECT
	  DATEPART(YEAR, CreationDate) yyyyy,
	  DATEPART(MONTH, CreationDate) mm,
	  DATEPART(HOUR, CreationDate) hh,
	  DATEDIFF(MINUTE, LAG(CreationDate) OVER (PARTITION BY 1 ORDER BY CreationDate), CreationDate) diff_minutes
	FROM dbMarekTrackingEF_production..UserActivities
	WHERE CreationDate > '2019-01-01'
) res
;
-- max_hours	avg_hours	stddev_hours
-- 8	0	0

-- dbMarekCommerceManager_production..OrderGroup_PurchaseOrder
-- ERROR if last update is > than 24 hours
-- WARNING if last update is > than 2 hours
SELECT
  ROUND(MAX(diff_minutes) / 60, 1) AS max_hours,
  ROUND(AVG(diff_minutes) / 60, 1) AS avg_hours,
  ROUND(STDEV(diff_minutes) / 60, 1)  AS stddev_hours
FROM (
	SELECT
	  DATEPART(YEAR, Created) yyyyy,
	  DATEPART(MONTH, Created) mm,
	  DATEPART(HOUR, Created) hh,
	  DATEDIFF(MINUTE, LAG(Created) OVER (PARTITION BY 1 ORDER BY Created), Created) diff_minutes
	FROM dbMarekCommerceManager_production..OrderGroup_PurchaseOrder
	WHERE Created > '2019-01-01'
) res
;
-- max_hours	avg_hours	stddev_hours
-- 23	0	0,4

-- dbMarekEF_production..DownloadHistories
-- ERROR if last update is > than 12 hours
-- WARNING if last update is > than 1 hours
SELECT
  ROUND(MAX(diff_minutes) / 60, 1) AS max_hours,
  ROUND(AVG(diff_minutes) / 60, 1) AS avg_hours,
  ROUND(STDEV(diff_minutes) / 60, 1)  AS stddev_hours
FROM (
	SELECT
	  DATEPART(YEAR, CreationDate) yyyyy,
	  DATEPART(MONTH, CreationDate) mm,
	  DATEPART(HOUR, CreationDate) hh,
	  DATEDIFF(MINUTE, LAG(CreationDate) OVER (PARTITION BY 1 ORDER BY CreationDate), CreationDate) diff_minutes
	FROM dbMarekEF_production..DownloadHistories
	WHERE CreationDate > '2019-01-01'
) res
;
-- max_hours	avg_hours	stddev_hours
-- 11	0	0,1
