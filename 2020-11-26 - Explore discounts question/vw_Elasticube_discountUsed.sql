USE ExploreData;


select --distinct top 1000
	oh.SiteId
	,oh.EpiOrderId as OrderGroupId
	,oh.OrderTrackId as OrderNumber
	,oh.PaymentMethod
	,pin.PinNames  as DiscountCode
	,oh.DiscountSum
from dbMarekEF_production..OrderHistories oh with(nolock)
--join dbMarekEF_production..OrderItemsHistories oih with(nolock) on oh.EpiOrderId = oih.EpiOrderId
--left join dbMarekCommerceManager_production.dbo.OrderGroup_PurchaseOrder ogp with(nolock) on oh.EpiOrderId = ogp.ObjectId -- OrderDate
--left join dbMarekCommerceManager_production..Cls_Contact clc with(nolock) on oh.UserId = clc.ContactId
--left join dbMarekEF_production..OrderDiscounts od with(nolock) on od.EpiOrderId = oh.EpiOrderId
left join dbMarekCommerceManager_production..OrderForm ofd with(nolock) on oh.EpiOrderId = ofd.OrderGroupId
left join (
	select Distinct
	pin.OrderFormId
	,stuff(
	(
	SELECT '|' + pin1.name
	FROM dbMarekCommerceManager_production..PromotionInformation pin1 with(nolock) WHERE pin1.orderformid = pin.orderformid
	FOR XML PATH('')
	), 1, 1, ''
) as PinNames
from dbMarekCommerceManager_production..PromotionInformation pin with(nolock)) pin on ofd.OrderFormId = pin.OrderFormId
	--where 		oh.SiteId = 1992
	where oh.DiscountSum > 0
	order by oh.EpiOrderId
--------------

SELECT TOP 10 * FROM dbMarekEF_production..OrderDiscounts od with(nolock)
-- tied to EpiLineItemId

SELECT TOP 10 * FROM  dbMarekCommerceManager_production..PromotionInformation pin with(nolock) 
-- tracked: order level or lineitem level?
-- tied to orderform

DROP TABLE IF EXISTS Discounts;
SELECT
	oh.EpiOrderId,
	oh.CreationDate,
	ofd.OrderFormId,
	oh.OrderTrackId AS OrderNumber,
	od.cnt AS OrderDiscounts_Records,
	pin.cnt AS PromotionInformation_Records,
	od.code AS OrderDiscounts_Codes,
	pin.couponcode AS PromotionInformation_CouponCode,
	pin.name AS PromotionInformation_Names,
	ofd.DiscountAmount AS OrderForm_DiscountAmount,
	od.Amount AS OrderDiscounts_Amount,
	pin.OrderLevelSavedAmount AS PromotionInformation_OrderLevelSavedAmount,
	CASE
		  WHEN COALESCE(od.Amount , 0) = COALESCE(pin.OrderLevelSavedAmount, 0) THEN '1. ok'
			WHEN COALESCE(od.Amount , 0) > COALESCE(pin.OrderLevelSavedAmount, 0) THEN '2. EF > Commerce'
			WHEN COALESCE(od.Amount , 0) < COALESCE(pin.OrderLevelSavedAmount, 0) THEN '3. EF < Commerce'
		END AS t
INTO Discounts
FROM dbMarekEF_production..OrderHistories oh with(nolock)
FULL OUTER JOIN dbMarekCommerceManager_production..OrderForm ofd with(nolock) ON
   oh.EpiOrderId = ofd.OrderGroupId
LEFT JOIN (
  SELECT
	  EpiOrderId,
		COUNT(*) cnt,
		SUM(Amount) Amount,
		STRING_AGG(code, ', ') code
	FROM dbMarekEF_production..OrderDiscounts od with(nolock)
	GROUP BY EpiOrderId
) od on od.EpiOrderId = oh.EpiOrderId
LEFT JOIN (
  SELECT
	  OrderFormId,
		COUNT(*) cnt,
		SUM(OrderLevelSavedAmount) OrderLevelSavedAmount,
		STRING_AGG(couponcode, ', ') couponcode,
		STRING_AGG(name, ', ') name
	FROM dbMarekCommerceManager_production..PromotionInformation pin with(nolock)
	GROUP BY OrderFormId
) pin ON pin.OrderFormId = ofd.OrderFormId
WHERE
  -- full list of orders with discounts
  (od.cnt > 1 OR pin.cnt > 1)

	-- od only, last 2020-09-26 02:53:23.130
	--(COALESCE(od.cnt, 0) > COALESCE(pin.cnt, 0))

	-- pin only, last ??? - order forms wo orders
	--(COALESCE(od.cnt, 0) < COALESCE(pin.cnt, 0))

	-- sum mismatch
	--(
	--  COALESCE(ofd.DiscountAmount, 0) != COALESCE(od.amnt, 0) OR 
	--	COALESCE(ofd.DiscountAmount, 0) != COALESCE(pin.amnt, 0)
	--)
ORDER BY oh.CreationDate DESC
;

SELECT COUNT(*) FROM Discounts;
--1228

SELECT
  t,
	COUNT(*)
FROM Discounts
GROUP BY t
ORDER BY t
;
--1. ok	40
--2. EF > Commerce	1163
--3. EF < Commerce	25
