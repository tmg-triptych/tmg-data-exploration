-- OLD, no Transformation
SELECT
	PaymentMethod, 
	STRING_AGG(SiteId, ', '), 
	SUM(Cnt) 
FROM (
	SELECT
		SiteId,
		PaymentMethod,
		COUNT(*) as Cnt
	FROM dbMarekEF_production..OrderHistories oh
	GROUP BY SiteId, PaymentMethod
) res
GROUP BY PaymentMethod
ORDER BY SUM(Cnt) DESC
;

-- OLD, with Transformation
SELECT
	PaymentMethod, 
	STRING_AGG(SiteId, ', '), 
	SUM(Cnt) 
FROM (
	SELECT
		SiteId,
		PaymentMethod,
		COUNT(*) as Cnt
	FROM (
		SELECT 
		    SiteId,
		    CASE
				WHEN PaymentMethod LIKE ' ' THEN 'No Payment' 
				WHEN LOWER(PaymentMethod) LIKE '%card%' THEN 'Card'
				WHEN LOWER(PaymentMethod) LIKE '%visa%' THEN 'Card'
				WHEN LOWER(PaymentMethod) LIKE '%americanexpress%' THEN 'Card'
				WHEN PaymentMethod LIKE 'Co-Op' THEN 'Coop'
				ELSE PaymentMethod
			END AS PaymentMethod 
			FROM dbMarekEF_production..OrderHistories
	) res
	GROUP BY SiteId, PaymentMethod
) res
GROUP BY PaymentMethod
ORDER BY SUM(Cnt) DESC
;

-- NEW, no Transformation
SELECT
	PaymentMethodName, 
	STRING_AGG(SiteId, ', '), 
	SUM(Cnt) 
FROM (
	SELECT
		oh.SiteId,
		ofp.PaymentMethodName,
		COUNT(*) as Cnt
	FROM dbMarekEF_production..OrderHistories oh
	JOIN dbMarekCommerceManager_production..OrderFormPayment ofp ON ofp.OrderGroupId = oh.EpiOrderId
	GROUP BY oh.SiteId, ofp.PaymentMethodName
) res
GROUP BY PaymentMethodName
ORDER BY SUM(Cnt) DESC
;



-- NEW, with Transformation
SELECT
	PaymentMethodId,
	PaymentConfigurationId,
	PaymentMethodName, 
	STRING_AGG(SiteId, ', ') as Sites, 
	SUM(Cnt) as RecordsCount
FROM (
	SELECT
		SiteId,
		PaymentConfigurationId,
		PaymentMethodId,
		PaymentMethodName,
		COUNT(*) as Cnt
	FROM (
		SELECT
			SiteId,
			PaymentConfigurationId,
			PaymentMethodId,
			CASE
				WHEN LOWER(PaymentMethodName) LIKE '%card%' THEN 'Card'
				WHEN LOWER(PaymentMethodName) LIKE '%visa%' THEN 'Card'
				WHEN LOWER(PaymentMethodName) LIKE '%americanexpress%' THEN 'Card'
				ELSE PaymentMethodName
			END AS PaymentMethodName 
		FROM dbMarekEF_production..OrderHistories oh
		JOIN dbMarekCommerceManager_production..OrderFormPayment ofp ON ofp.OrderGroupId = oh.EpiOrderId
		JOIN dbMarekCommerceManager_production..OrderFormPayment_CustomPayment ofpc ON ofp.PaymentId = ofpc.ObjectId
	) res
	WHERE SiteId = '31820'
	GROUP BY SiteId, PaymentConfigurationId, PaymentMethodId, PaymentMethodName
) res
GROUP BY PaymentConfigurationId, PaymentMethodId, PaymentMethodName
ORDER BY PaymentMethodId, SUM(Cnt) DESC
;

SELECT TOP 3 * FROM  dbMarekCommerceManager_production..OrderFormPayment;
SELECT TOP 3 * FROM  dbMarekCommerceManager_production..OrderFormPayment_CustomPayment;

-- What do we know about custom payment methods?
-- C6DB4FD2-4EE5-4E26-A877-6AA0584D30FD	Bausch + Lomb Bulk Order	7937	7636	4380
-- let's see this payment method

SELECT
  *
FROM [dbMarekCMS_production].[dbo].[tblContent]
WHERE pkID = 7937
-- ContentGiud FBA36494-2D1B-4B31-9BB0-90E84490D25D
	

select top 100 pkId, fkContentId as SiteId, LongString 
from [dbMarekCMS_production].[dbo].[tblContentProperty]
where [fkPropertyDefinitionID] = (SELECT TOP 1 pkID
FROM [dbMarekCMS_production].[dbo].[tblPropertyDefinition]
where Name like '%PaymentMethodData%' AND LongString LIKE LOWER('%FBA36494-2D1B-4B31-9BB0-90E84490D25D%'))
--<div data-classid="36f4349b-8093-492b-b616-05d8964e4c89" data-contentgroup="" data-contentguid="cce9cc12-cf50-42f8-aa59-0e443e48ecb3" data-contentname="Co-op Funds">{}</div><div data-classid="36f4349b-8093-492b-b616-05d8964e4c89" data-contentguid="fba36494-2d1b-4b31-9bb0-90e84490d25d" data-contentname="Bausch + Lomb Bulk Order">{}</div>

