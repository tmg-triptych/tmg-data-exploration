USE ExploreData;

DROP TABLE IF EXISTS #orders;
SELECT
  oh.EpiOrderId AS EF_EpiOrderId,
  ogp.ObjectId AS Comm_ObjectId
INTO #orders
FROM dbMarekEF_production..OrderHistories oh WITH(NOLOCK)
FULL OUTER JOIN dbMarekCommerceManager_production..OrderGroup_PurchaseOrder ogp WITH(NOLOCK) ON oh.EpiOrderId = ogp.ObjectId
;  

SELECT COUNT(*), COUNT(DISTINCT EpiOrderId), COUNT(DISTINCT ObjectId) FROM #orders;
-- 329 000	
-- 328 788	
-- 328 991

SELECT
  EF_EpiOrderId,
	Comm_ObjectId,
	COUNT(Comm_ObjectId) OVER (PARTITION BY EF_EpiOrderId) AS Comm_card,
	COUNT(EF_EpiOrderId) OVER (PARTITION BY Comm_ObjectId) AS EF_card
INTO #orders_02
FROM #orders
;

SELECT Comm_card, COUNT(*) FROM #orders_02 GROUP BY Comm_card ORDER BY Comm_card;
-- 0	9
-- 1	328780
-- 212	212

SELECT EF_card, COUNT(*) FROM #orders_02 GROUP BY EF_card ORDER BY EF_card;
-- 0	9
-- 1	328780
-- 212	212

SELECT * FROM dbMarekEF_production..OrderHistories oh WHERE EpiOrderId IN (SELECT EF_EpiOrderId FROM #orders_02 WHERE Comm_card = 0) ORDER BY CreationDate;

SELECT * FROM dbMarekCommerceManager_production..OrderGroup_PurchaseOrder WHERE ObjectId IN (SELECT Comm_ObjectId FROM #orders_02 WHERE EF_card = 0) ORDER BY Created;