------------------
-- Original data
------------------

--  CoopBudgetUserPercents
-- Budget and Balance field is Obsolete � we left them right now to be able to fix migration if something went wrong during it, but those fields will not be updated from UI or API anymore and will be removed soon
--  CoopBudgetUserPercents has one-to-many relation to new CoopBudgets table (for old coop it will be one-to-one in fact)
SELECT TOP 1 * FROM dbMarekEF_production..CoopBudgetUserPercents;

--  CoopBudgetUserPercents
-- Budget and Balance field is Obsolete � we left them right now to be able to fix migration if something went wrong during it, but those fields will not be updated from UI or API anymore and will be removed soon
-- CoopBudgetUserPercents has one-to-many relation to new CoopBudgets table (for old coop it will be one-to-one in fact)
SELECT TOP 1 * FROM dbMarekEF_production..CoopBudgets;

--  CoopBudgetUserPercentLogs
-- CoopBudgetUserPercentId � reference to CoopBudgetUserPercents
-- CoopBudgetId � reference to CoopBudgets (NULL if admin edit allocations values)
SELECT TOP 1 * FROM dbMarekEF_production..CoopBudgetUserPercentLogs;


--  CoopBudgetUserPayments
-- Now have a reference to CoopBudgets instead CoopBudgetUserPercents
-- If payment was perform using several CoopBudgets several records will be added to CoopBudgetUserPayments
-- In payment tables (CommerceManager datatabase) as TransactionID will be used comma separated list of affected CoopBudgetUserPayments records
SELECT TOP 1 * FROM dbMarekEF_production..CoopBudgetUserPayments;

SELECT TOP 1 * FROM DWH_Stage_CurrentStateLayer..CS_CoopLog;
SELECT TOP 1 * FROM DWH_Stage_CurrentStateLayer..CS_Client;

-- ����:
-- �� ����������� - ������ 20�� �������� ����� ��� ������ - ����� �������. ������� ������� ��-������� ��������
-- � ���, ������ ������ (�� ������� ����� �������) ������� � ������ ��������� ������/��. ��������
-- ��� ��� ���������� ������ ����� ����������� � ����
-- � ������� CoopBudgetUserPercentLog->SourceType
-- ����� � CoopBudgetUserPercentLog ��������� 2 �������: ExpirationDate (��������� ���� ����������)  � ActivityCount
-- � CoopBudgetUserPercent ��������� ������� IsEnabled (����������/��������� ������ ��� �����)
-- �  CoopBudget ��������� ������� AdditionalNote. ��������� ������� � ���������� (������ ���������)
-- ���� ����� ���������� �� CoopBudgetUserPercentLog->SourceType
-- ������ ����� �������� �� �������� 6 (rewards) � 7 (buying) ���. ����� ���������� ��������