-- Problem 1. Strange record in dbMarekEF_production..OrderHistories

SELECT
  * 
FROM dbMarekEF_production..OrderHistories
WHERE EpiOrderId = 988829
-- Coop = 5.72

SELECT
  * 
FROM dbMarekEF_production..OrderItemsHistories
WHERE EpiOrderId = 988829
-- Price = 286.00


SELECT
  * 
FROM dbMarekCommerceManager_production..OrderFormPayment
WHERE OrderGroupId = 988829
-- Amount = 286.000000000, PaymentMethodName = Growth Dollar Payment

------------------- Question: how does it look in DWH? Order & Payment properties

SELECT
  * 
FROM DWH_CurrentStateLayer..CS_Order
WHERE OrderId = 988829
-- Price_Coop = 5.72

SELECT
  * 
FROM DWH_CurrentStateLayer..CS_OrderLineItem
WHERE OrderId = 988829
-- Price = 286.00

SELECT
  * 
FROM DWH_CurrentStateLayer..CS_Payment
WHERE OrderId = 988829
-- Amount = 286.00

-- Result - it looks the same


--------------- How do original records on Coop Look for EpiOrderId = 988829, UserId = A31E14B9-8612-44F6-86F3-01F767F68C18, CreationDate = 2021-10-27 17:58:08.160

SELECT * FROM dbMarekEF_production..CoopBudgetUserPercents WHERE UserId = 'A31E14B9-8612-44F6-86F3-01F767F68C18';
-- Id     UserId                                SiteId  Budget Balance   OrderPercent LastUpdate              IsDeleted FromExternal ShippingPercent CoopType IsEnabled
-- 595843 A31E14B9-8612-44F6-86F3-01F767F68C18  136945  NULL   NULL      0.00         2021-09-07 15:51:42.937 0         0            0.00            1        1          <- CoopType = 1 => Coop
-- 595844 A31E14B9-8612-44F6-86F3-01F767F68C18  136945  NULL   NULL    100.00         2021-11-01 16:34:05.207 0         0            100.00          2        1          <- CoopType = 2 => GrowthDollars

SELECT * FROM dbMarekEF_production..CoopBudgets WHERE CoopBudgetUserPercentId = 595844 ORDER BY CreationDate DESC;
-- Id      Budget  Balance ExpirationDate           CreationDate             UpdateDate               CoopBudgetUserPercentId IsDeleted IsExpired AdditionalName
-- 290141  20.00   20.00   2022-11-01 00:00:00.000  2021-11-01 16:34:05.207  NULL                     595844                  0         0         Anniversary Points
-- 290107  1.00     1.00   2022-10-27 00:00:00.000  2021-10-27 16:11:11.843  NULL                     595844                  0         0         Thanks for all of the help and all of the other things you do to keep us.
-- 289675  5.14     2.54   2022-09-13 00:00:00.000  2021-09-13 16:16:26.277  2021-10-27 17:58:07.723  595844                  0         0         Diamond Reward points
-- 289508  283.40   0.00   2022-09-07 00:00:00.000  2021-09-07 21:04:24.623  2021-10-27 17:58:07.723  595844                  0         0         NULL

SELECT * FROM dbMarekEF_production..CoopBudgetUserPercentLogs WHERE CoopBudgetUserPercentId = 595844 ORDER BY Id DESC;
-- Id     CoopBudgetUserPercentId UserId                                ChangedByUserId                       OldOrderPercent NewOrderPercent OldBudget NewBudget  LatestUpdate             Bonus OldShippingPercent NewShippingPercent CoopBudgetId  OldBalance NewBalance SourceType ActivityId ActivityCount IsEnabled ExpirationDate
-- 28739  595844                  A31E14B9-8612-44F6-86F3-01F767F68C18  E0F657BF-6554-4002-A5EA-FF36CDA7B950  100.00          100.00               1.00     20.00  2021-11-01 16:34:05.413  20.00             100.00             100.00       290141        1.00      20.00          1       NULL          NULL      NULL           NULL  <- SourceType = 1 => Managemane
-- 28368  595844                  A31E14B9-8612-44F6-86F3-01F767F68C18  A31E14B9-8612-44F6-86F3-01F767F68C18  100.00          100.00               5.14      5.14  2021-10-27 17:58:07.723  NULL              100.00             100.00       289675        5.14       2.54          4       NULL          NULL      NULL           NULL  <- SourceType = 4 => Payment
-- 28367  595844                  A31E14B9-8612-44F6-86F3-01F767F68C18  A31E14B9-8612-44F6-86F3-01F767F68C18  100.00          100.00             283.40    283.40  2021-10-27 17:58:07.723  NULL              100.00             100.00       289508      283.40       0.00          4       NULL          NULL      NULL           NULL  <- SourceType = 4 => Payment
-- 28343  595844                  A31E14B9-8612-44F6-86F3-01F767F68C18  391E342A-A575-4C53-A233-9008F47DB115  100.00          100.00               5.14      1.00  2021-10-27 16:11:12.127  1.00              100.00             100.00       290107        5.14       1.00          1       NULL          NULL      NULL           NULL  <- SourceType = 1 => Managemane
-- 27005  595844                  A31E14B9-8612-44F6-86F3-01F767F68C18  391E342A-A575-4C53-A233-9008F47DB115  100.00          100.00             283.40      5.14  2021-09-13 16:16:26.307  5.14              100.00             100.00       289675      283.40       5.14          1       NULL          NULL      NULL           NULL  <- SourceType = 1 => Managemane
-- 26537  595844                  A31E14B9-8612-44F6-86F3-01F767F68C18  6DEDABCF-DA56-42A5-88F5-5F710ABD164E  100.00          100.00               0.00    283.40  2021-09-07 21:04:24.623  283.40            100.00             100.00       289508        0.00     283.40          1       NULL          NULL      NULL           NULL  <- SourceType = 1 => Managemane
-- 25780  595844                  A31E14B9-8612-44F6-86F3-01F767F68C18  08EB4A4F-153E-4A81-8BB4-08FB8FC0FA30  100.00          100.00               0.00      0.00  2021-09-07 19:29:21.847  NULL                0.00             100.00         NULL        0.00       0.00          1       NULL          NULL      NULL           NULL  <- SourceType = 1 => Managemane
-- 25680  595844                  A31E14B9-8612-44F6-86F3-01F767F68C18  08EB4A4F-153E-4A81-8BB4-08FB8FC0FA30    0.00          100.00               0.00      0.00  2021-09-07 19:28:37.070  NULL                0.00               0.00         NULL        0.00       0.00          1       NULL          NULL      NULL           NULL  <- SourceType = 1 => Managemane

SELECT * FROM dbMarekEF_production..CoopBudgetUserPayments WHERE UserId = 'A31E14B9-8612-44F6-86F3-01F767F68C18' ORDER BY Id DESC;
-- Id    UserId                                IsUndoPayment Payment  LatestUpdate             OrderAllocation ShippingAllocation CoopBudgetId
-- 3974  A31E14B9-8612-44F6-86F3-01F767F68C18  0                2.60  2021-10-27 17:58:07.723           100.00               0.00  289675
-- 3973  A31E14B9-8612-44F6-86F3-01F767F68C18  0              283.40  2021-10-27 17:58:07.723           100.00               0.00  289508
