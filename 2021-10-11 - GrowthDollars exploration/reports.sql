-- Goal to create dashboard similar to DWHWRIGHT for DWH_HoChunkRewards

-- Reports:
-- 1	Coop Usage by Month
-- 2	Top Coop Users
-- 3	Top Growth Dollar Users
-- 4	Order Level Reports on Payments
-- 5	User Level Reports on Paymments
-- 6	Coop Balance
-- 7	Growth Dollar Balance

-------------------------------------
-- Ho-Chunk Rewards data
-- SiteId: 136945, CatalogId: 124
-------------------------------------
--...

---------------------------------
-- 1. Explore schema relations and cardinalities
---------------------------------

-- CoopBudgets->CoopBudgetUserPayment N*:M
SELECT
  COUNT(*) total,
	COUNT(cb.Id) cbId,
	COUNT(cbup.Id) cbupId,
	COUNT(cb.Id) cbId,
	COUNT(DISTINCT cb.Id) dCbId,
	COUNT(cbup.Id) cbupId,
	COUNT(DISTINCT cbup.Id) dCbupId
FROM dbMarekEF_production..CoopBudgets cb
FULL OUTER JOIN dbMarekEF_production..CoopBudgetUserPayments cbup ON cbup.CoopBudgetId = cb.Id
;
-- 260835	
-- 260835
-- 4172
-- 260835 | 258027
-- 4 172 | 4 172

-- there's always CoopBudgets record for every CoopBudgetUserPayments records
-- some CoopBudgets records do't have matching CoopBudgetUserPayments records
-- there  may be many CoopBudgets records for CoopBudgetUserPayments record
-- there  may be many CoopBudgetUserPayments records for CoopBudgets record

-- CoopBudgets->CoopBudgetUserPercentLogs 1*:1*

-- CoopBudgets records by matches in CoopBudgetUserPercentLogs
SELECT COUNT(*) FROM dbMarekEF_production..CoopBudgets -- 258 027
SELECT
  COUNT(cbId) AS cbRecords,
	cbuplRecords
FROM (
	SELECT
		cb.Id AS cbId,
		CASE
			WHEN COUNT(DISTINCT cbupl.CoopBudgetId) = 0 THEN '0'
			WHEN COUNT(DISTINCT cbupl.CoopBudgetId) = 1 THEN '1'
			WHEN COUNT(DISTINCT cbupl.CoopBudgetId) > 1 THEN 'Many'
		END AS cbuplRecords
	FROM dbMarekEF_production..CoopBudgets cb
	FULL OUTER JOIN dbMarekEF_production..CoopBudgetUserPercentLogs cbupl ON cbupl.CoopBudgetId = cb.Id
	WHERE cb.Id IS NOT NULL
	GROUP BY cb.Id
) r 
GROUP BY cbuplRecords
ORDER BY cbuplRecords
;
-- 252 639	0
--   5 388	1


-- CoopBudgetUserPercentLogs records by matches in CoopBudgets
SELECT COUNT(*) FROM dbMarekEF_production..CoopBudgetUserPercentLogs cbupl-- 30 132
SELECT
  COUNT(cbuplId) AS cbuplRecords,
	cbRecords
FROM (
	SELECT
		cbupl.Id AS cbuplId,
		CASE
			WHEN COUNT(DISTINCT cb.Id) = 0 THEN '0'
			WHEN COUNT(DISTINCT cb.Id) = 1 THEN '1'
			WHEN COUNT(DISTINCT cb.Id) > 1 THEN 'Many'
		END AS cbRecords
	FROM dbMarekEF_production..CoopBudgets cb
	FULL OUTER JOIN dbMarekEF_production..CoopBudgetUserPercentLogs cbupl ON cbupl.CoopBudgetId = cb.Id
	WHERE cbupl.Id IS NOT NULL
	GROUP BY cbupl.Id
) r 
GROUP BY cbRecords
ORDER BY cbRecords
;
-- 14 483	0
-- 15 649	1


-- CoopBudgetUserPercents->CoopBudgets
SELECT
  COUNT(*) total,
	COUNT(cbup.Id) cbupId,
	COUNT(cb.CoopBudgetUserPercentId) cbCoopBudgetUserPercentId,
	COUNT(DISTINCT cbup.Id) cbup,
	COUNT(DISTINCT cb.CoopBudgetUserPercentId) cbCoopBudgetUserPercentId
FROM dbMarekEF_production..CoopBudgetUserPercents cbup
FULL OUTER JOIN dbMarekEF_production..CoopBudgets cb ON cb.CoopBudgetUserPercentId = cbup.Id
;
-- 580718
-- 580718
-- 258027
-- 579796
-- 257105

-- CoopBudgetUserPercents->CoopBudgetUserPercentLogs N*:M
SELECT
  COUNT(*) total,
	COUNT(cbup.Id) cbupId,
	COUNT(cbupl.CoopBudgetUserPercentId) cbuplCoopBudgetUserPercentId,
	COUNT(DISTINCT cbup.Id) cbup,
	COUNT(DISTINCT cbupl.CoopBudgetUserPercentId) cbuplCoopBudgetId
FROM dbMarekEF_production..CoopBudgetUserPercents cbup
FULL OUTER JOIN dbMarekEF_production..CoopBudgetUserPercentLogs cbupl ON cbupl.CoopBudgetUserPercentId = cbup.Id
;
--604077
--604077
--30127
--579796
--5846

-- there's always CoopBudgetUserPercents record for every CoopBudgetUserPercentLogs records
-- some CoopBudgetUserPercentLogs records do't have matching CoopBudgetUserPercents records
-- there  may be many CoopBudgetUserPercents records for CoopBudgetUserPercentLogs record
-- there  may be many CoopBudgetUserPercentLogs records for CoopBudgetUserPercents record
