import xml.etree.ElementTree as ET


def get_tables_profiles_cinfig(fielname):
    tree = ET.parse(fielname)
    root = tree.getroot()

    tables = {}

    for table in root:

        table_name = table.attrib['name']

        accumulate_by_date = False
        if 'accumulate_by' in table.attrib:
            if table.attrib['accumulate_by'] == 'ga:date':
                accumulate_by_date = True

        webproperties = []
        for webpropertiy in table[0]:
            webproperties.append(webpropertiy.attrib['id'])

        dimensions = []
        for dimension in table[1]:
            dimensions.append(dimension.attrib['id'])

        metrics = []
        for metric in table[2]:
            metrics.append(metric.attrib['id'])

        tables[table_name] = {
            'webproperties': webproperties,
            'dimensions': dimensions,
            'metrics': metrics,
            'accumulate_by_date': accumulate_by_date
        }

    return tables


def generate_tables_creation_code(config):
    table_creation_code = []
    for table in config:
        drop_table_sql = 'DROP TABLE ' + table + ';\n'

        dimensions = config[table]['dimensions']
        metrics = config[table]['metrics']

        custom_column_code = []

        for dimension in dimensions:
            column_name = dimension.replace('ga:', 'ga_')
            column_type = 'VARCHAR (MAX)'
            # todo - change insertion rules
            if dimension in ['ga:date']:
                column_type = 'DATE'
            custom_column_code.append(column_name + ' ' + column_type)

        for metric in metrics:
            column_name = metric.replace('ga:', 'ga_')
            column_type = 'VARCHAR (256)'
            if metric in [
                'ga:timeOnPage',
                'ga:sessionDuration'
            ]:
                column_type = 'NUMERIC(18, 2)'
            if metric in [
                'ga:entrances',
                'ga:bounces',
                'ga:medium',
                'ga:exits',
                'ga:newUsers',
                'ga:pageviews',
                'ga:users',
                'ga:sessions',
                'ga:uniquePageviews',
                'ga:uniqueEvents',
                'ga:totalEvents',
                'ga:pageLoadTime',
                'ga:pageLoadSample',
                'ga:eventValue'
            ]:
                column_type = 'INT'
            custom_column_code.append(column_name + ' ' + column_type)

        custom_columns_sql = ',\n'.join(custom_column_code)

        create_table_sql = """
        CREATE TABLE """ + table + """
        (
          Account VARCHAR (256),
          WebProperty VARCHAR (256),
          [View] VARCHAR (256),
        """ + custom_columns_sql + """
        );\n"""

        table_creation_code.append(drop_table_sql + create_table_sql)

    tables_creation_sql = '\n\n'.join(table_creation_code)

    return tables_creation_sql
