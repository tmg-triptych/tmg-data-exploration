import pyodbc

db_connection = pyodbc.connect("Driver={SQL Server Native Client 11.0};"
                      "Server=TRIPTYCHSQLREP;"
                      "Database=DWH_Stage_CurrentStateLayer;"
                      "Trusted_Connection=yes;")
cursor = db_connection.cursor()

def get_last_date_from_db(table, view):
    start_date = '2005-01-01'
    cursor.execute("SELECT MAX(ga_date) FROM " + table + " WHERE [View] = '" + view + "'")
    for row in cursor:
        max_date = row[0]
        if max_date:
            start_date = str(max_date)
        return start_date

def delete_profile_records_by_date(table, view, date):
    cursor.execute("DELETE FROM " + table + " WHERE ga_date >= '" + date + "' AND [View] = '" + view + "'")
    db_connection.commit()

def delete_all_profile_records(table, view):
    cursor.execute("DELETE FROM " + table + " WHERE [View] = '" + view + "'")
    db_connection.commit()


def insert_rows_to_db(ga_response, table, prefix):
    for row in ga_response:
        insert_columns = prefix.copy()
        insert_rows = []
        for column in row:
            insert_columns.append(column.replace("'", ""))
        insert_rows.append("', '".join(insert_columns))

    insert_rows_str = "'),\n('".join(insert_rows)

    insert_request = """
          INSERT INTO """ + table + """ 
          VALUES
          ('""" + insert_rows_str + """')
          """

    cursor.execute(insert_request)
    db_connection.commit()