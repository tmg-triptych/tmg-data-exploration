from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials


def get_service(api_name, api_version, scopes, key_file_location):
    """Get a service that communicates to a Google API.

    Args:
        api_name: The name of the api to connect to.
        api_version: The api version to connect to.
        scopes: A list auth scopes to authorize for the application.
        key_file_location: The path to a valid service account JSON key file.

    Returns:
        A service that is connected to the specified API.
    """

    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        key_file_location, scopes=scopes)

    # Build the service object.
    service = build(api_name, api_version, credentials=credentials)

    return service


def get_webproperties_settings(exclude_word):
    """Get a distionary on available eb profiles.

    Args:
        service: The GA conenction service to use.
        exclude_word: Exclude profiles containing this word.

    Returns:
        a dictionary.
        key: Web Property Id, e.g.'UA-63898923-1'
        value: a dictionary with following keys
            'account_name' (value example - 'Triptych'),
            'webproperty_name' (value example - 'Triptych Dev'),
            'profiles' (value - list of ditcionaries with following keys:
                'id' (value example - '103566132', key property to request data),
                'name' (value example - 'Dev User ID View')
            )
    """

    result = {}

    # Get a list of all Google Analytics accounts for this user
    accounts = service.management().accounts().list().execute()

    if accounts.get('items'):
        for acc_num in range(len(accounts['items'])):

            account_id = accounts.get('items')[acc_num].get('id')
            account_name = accounts.get('items')[acc_num].get('name')

            properties = service.management().webproperties().list(accountId=account_id).execute()

            if properties.get('items'):

                items = properties.get('items')
                wp_num = 0

                for item in items:
                    web_property_id = item['id']
                    property = item.get('id')

                    webproperty_name = item.get('name')

                    profiles = service.management().profiles().list(
                        accountId=account_id,
                        webPropertyId=property).execute()

                    profiles_ = []
                    for profile in profiles.get('items'):
                        if exclude_word and exclude_word.lower() in profile.get('name').lower():
                            pass
                        else:
                            profiles_.append({
                                'id': profile.get('id'),
                                'name': profile.get('name')
                            })

                    result[web_property_id] = {
                        'account_name': account_name,
                        'webproperty_name': webproperty_name,
                        'profiles': profiles_
                    }

                    wp_num += 1

    return result


def get_data_from_ga(profile_id, dimensions, metrics, start_date, end_date, start_index):
    return service.data().ga().get(
            ids='ga:' + profile_id,
            start_date=start_date,
            end_date=end_date,
            metrics=metrics,
            dimensions=dimensions,
            start_index=start_index
    ).execute()


service = get_service(
        api_name='analytics',
        api_version='v3',
        scopes=['https://www.googleapis.com/auth/analytics.readonly'],
        key_file_location='api-project-750249306608-ebd76ffdd0ce.json')