import ga_integration as gai
import db_integration as dbi
import config as cnf
import datetime
import time

config = cnf.get_tables_profiles_cinfig('GAnalytics.xml')

webproperties_ditc = gai.get_webproperties_settings(exclude_word='raw')

for table in config:
    print('Download ' + table)
    table_webproperties = config[table]['webproperties']
    table_dimensions = ','.join(config[table]['dimensions'])
    table_metrics = ','.join(config[table]['metrics'])
    table_accumulate = config[table]['accumulate_by_date']

    for webproperty_id in table_webproperties:
        print('...download for "' + webproperty_id + '"')

        if webproperty_id not in webproperties_ditc:
            print('......[ERROR] no profile found for web property id ' + webproperty_id)
        else:
            webproperty_account_name = webproperties_ditc[webproperty_id]['account_name']
            webproperty_name = webproperties_ditc[webproperty_id]['webproperty_name']
            webproperty_profiles = webproperties_ditc[webproperty_id]['profiles']
            for profile in webproperty_profiles:
                profile_name = profile['name']
                profile_id = profile['id']
                end_date = str(datetime.date.today().strftime("%Y-%m-%d"))

                ## DB: DELETE LAST DATE IF NECESSARY
                start_date = '2005-01-01'
                max_date = None
                if table_accumulate:
                    start_date = dbi.get_last_date_from_db(table, profile_name)
                    dbi.delete_profile_records_by_date(table, profile_name, start_date)
                    print('...deleted records for: ' + str(start_date))
                else:
                    dbi.delete_all_profile_records(table, profile_name)

                start_index = 1
                while True:

                    # GA: GET NEXT PORTION OF TABLE DATA PER PROFILE
                    ga_response = gai.get_data_from_ga(
                        profile_id,
                        table_dimensions,
                        table_metrics,
                        start_date,
                        end_date,
                        start_index
                    ).get('rows')
                    time.sleep(0.1)  # only 10 RPS permitted

                    if ga_response:
                        new_rows_counter = len(ga_response)
                        print('......got ' + str(new_rows_counter) + ' rows from profile ' + profile_name)
                        
                        if new_rows_counter < 1000:
                            break
                        else:
                            start_index += 1000

                        # DB: SAVE NEXT PORTION
                        dbi.insert_rows_to_db(ga_response, table, [webproperty_account_name, webproperty_name, profile_name])
                        
                    else:
                        print('......got 0 rows from profile ' + profile_name)
                        break
