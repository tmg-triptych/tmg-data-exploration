SELECT COUNT(*) FROM DWH_Stage_CurrentStateLayer..CS_User_UHC; -- 97183
SELECT COUNT(*) FROM DWH_CurrentStateLayer..CS_User_UHC;       -- 97602

SELECT COUNT(*) FROM DWH_Stage_CurrentStateLayer..CS_User_Masonite; -- 1951
SELECT COUNT(*) FROM DWH_CurrentStateLayer..CS_User_Masonite;       -- 1938

SELECT COUNT(DISTINCT UserId), COUNT(*) FROM DWH_Stage_CurrentStateLayer..CS_User; -- 198336	198350
SELECT COUNT(DISTINCT UserId), COUNT(*) FROM DWH_CurrentStateLayer..CS_User;       -- 198336	233157 -- looks like error

SELECT COUNT(*) FROM DWH_Stage_CurrentStateLayer..CS_User_CustomMetaData_MASONITEDATA; -- 987
SELECT COUNT(*) FROM DWH_CurrentStateLayer..CS_User_CustomMetaData_MASONITEDATA;       -- 987

SELECT COUNT(*) FROM DWH_Stage_CurrentStateLayer..CS_AGG_UserActivity; -- 272159
SELECT COUNT(*) FROM DWH_CurrentStateLayer..CS_AGG_UserActivity;       -- 273319

--------------------- compare User x Client decomposed table rows

DROP TABLE IF EXISTS #tmp;

SELECT
	COALESCE(old.AccessMask, new.AccessMask) AS AccessMask,
	COALESCE(old.ContactId, new.ContactId) AS ContactId,
	old.UsersCount AS oldUsersCount,
	new.UsersCount AS newUsersCount,
	COALESCE(old.UsersCount, 0) - COALESCE(new.UsersCount, 0) AS diff
INTO #tmp
FROM (
  SELECT SiteId, AccessMask, ContactId, COUNT(*) AS UsersCount FROM DWH_Stage_CurrentStateLayer..CS_AGG_UserActivity GROUP BY SiteId, AccessMask, ContactId
) new
FULL OUTER JOIN (
  SELECT SiteId, AccessMask, ContactId, COUNT(*) AS UsersCount FROM DWH_CurrentStateLayer..CS_AGG_UserActivity GROUP BY SiteId, AccessMask, ContactId
) old ON
  new.AccessMask = old.AccessMask AND
  new.ContactId = old.ContactId
;

SELECT COUNT(*) FROM #tmp WHERE diff <> 0 ORDER BY SiteId, AccessMask, ContactId;

SELECT 
 c.Name,
 res.*
FROM (
	SELECT
		AccessMask,
		SUM(oldUsersCount) oldUsersTotal,
		SUM(newUsersCount) newUsersTotal,
		COALESCE(ABS(SUM(CASE WHEN diff <> 0 THEN oldUsersCount END)), 0) + COALESCE(ABS(SUM(CASE WHEN diff <> 0 THEN newUsersCount END)), 0) diff
	FROM #tmp
	GROUP BY AccessMask
) res
LEFT JOIN CS_Client c ON c.AccessMask = res.AccessMask
ORDER BY res.diff DESC
;


SELECT COUNT(*) FROM DWH_Client_Ipso..[User];
-- 1147
SELECT COUNT(*) FROM DWH_Stage_Client_Ipso..[User];
-- 27

SELECT * FROM DWH_Stage_CurrentStateLayer..CS_User WHERE AccessMask & 16777216 > 0

SELECT TOP 1 * FROM DWH_Client_Ipso..[User];

SELECT TOP 1 * FROM DWH_Stage_Client_Ipso..[User];

SELECT * FROM CS_Client


SELECT WebProperty, COUNT(*) FROM  DWH_CurrentStateLayer..GA_Traffic_By_Content_Daily  GROUP BY WebProperty

SELECT
  *
FROM DWH_CurrentStateLayer..GA_Traffic_By_Content_Daily
WHERE ga_pagePath LIKE '%editor%'
;



http://triptych.test.themarekgroup.net/editor/product?referenceId=1671