SELECT 
cls.email,
mb.Email AS ImpersonatorEmail,
[Id]
      ,[EpiLineItemId]
      ,dh.[UserId]
      ,[CmbrPath]
      ,[ToolId]
      ,[Name]
      ,[Sku]
      ,[Price]
      ,[CreationDate]
      ,[SkuId]
      ,[SiteId]
      ,[Filename]
      ,[VersionName]
      ,[IsRetired]
  FROM [dbMarekEF_production].[dbo].[DownloadHistories] dh with(nolock)
  LEFT JOIN dbMarekCommerceManager_production.[dbo].[cls_Contact] cls with(nolock) on dh.userid = cls.ContactId
  	left join dbMarekCommerceManager_production.[dbo].aspnet_Membership mb with(nolock) on dh.UserId = mb.UserId
  where 
  dh.siteid = 123399
  and 
  cls.email IS NULL
