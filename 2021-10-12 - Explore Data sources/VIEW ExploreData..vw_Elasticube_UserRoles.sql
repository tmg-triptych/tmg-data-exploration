select
  am.UserId
	,c.ContactId
	,ar.RoleName
	,am.Email
from dbMarekCommerceManager_production.[dbo].[aspnet_Membership] am with(nolock)
	join dbMarekCommerceManager_production.[dbo].[aspnet_UsersInRoles] uir with(nolock) on am.UserId = uir.UserId
	join dbMarekCommerceManager_production.dbo.aspnet_Roles ar with(nolock) ON uir.RoleId = ar.RoleId
	join dbMarekCommerceManager_production.[dbo].[cls_Contact] c with(nolock) on am.Email = c.Email


