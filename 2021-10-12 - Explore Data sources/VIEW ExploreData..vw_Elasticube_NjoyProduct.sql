SELECT 
c.objectid AS CatEntryID
,c.MetaFieldId
,c.LongString AS SKU
,CatalogEntryId
,cer.ParentEntryId AS ParentEntryID --added 8.18.20 to accomdate POD and MV products
,c.MetaClassId
,ce.ClassTypeId
,bb.longstring AS Metadata


					,(case 
					when cc.longstring not like '%Size%' then ''
					when cc.longstring like '%Size":"",%' then ''
					else left(right(cc.longstring,len(cc.longstring) - (CHARINDEX('"Size":"',cc.longstring,0) + len('"Size":"') - 1)),CHARINDEX('"',right(cc.longstring,len(cc.longstring) - (CHARINDEX('"Size":"',cc.longstring,0) + len('"Size":"')))) ) 
					end) as Size
,(case 
					when dd.longstring not like '%PosType%' then ''
					when dd.longstring like '%PosType":"",%' then ''
					else left(right(dd.longstring,len(dd.longstring) - (CHARINDEX('"PosType":"',dd.longstring,0) + len('"PosType":"') - 1)),CHARINDEX('"',right(dd.longstring,len(dd.longstring) - (CHARINDEX('"PosType":"',dd.longstring,0) + len('"PosType":"')))) ) 
					end) as POSType
,(case 
					when ee.longstring not like '%POSCostCategory%' then ''
					when ee.longstring like '%POSCostCategory":"",%' then ''
					else left(right(ee.longstring,len(ee.longstring) - (CHARINDEX('"POSCostCategory":"',ee.longstring,0) + len('"POSCostCategory":"') - 1)),CHARINDEX('"',right(ee.longstring,len(ee.longstring) - (CHARINDEX('"POSCostCategory":"',ee.longstring,0) + len('"POSCostCategory":"')))) ) 
					end) as POSCostCategory
,(case 
					when ff.longstring not like '%ComplianceApprovalDate%' then ''
					when ff.longstring like '%ComplianceApprovalDate":"",%' then ''
					else left(right(ff.longstring,len(ff.longstring) - (CHARINDEX('"ComplianceApprovalDate":"',ff.longstring,0) + len('"ComplianceApprovalDate":"') - 1)),CHARINDEX('"',right(ff.longstring,len(ff.longstring) - (CHARINDEX('"ComplianceApprovalDate":"',ff.longstring,0) + len('"ComplianceApprovalDate":"')))) ) 
					end) as ComplianceApprovalDate
,pd.UnitPrice
from [dbMarekCommerceManager_production].[dbo].[CatalogContentProperty] c with(nolock)
LEFT join [dbMarekCommerceManager_production].dbo.catalogentry ce with(nolock) on c.ObjectId = ce.CatalogEntryId 
left join [dbMarekCommerceManager_production].[dbo].[CatalogContentProperty] bb with(nolock) on c.ObjectId= bb.objectId and bb.MetaFieldName LIKE 'MetaInformation'
left join [dbMarekCommerceManager_production].[dbo].[CatalogContentProperty] cc with(nolock) on c.ObjectId= cc.objectId and cc.LongString LIKE '%Size%' and cc.MetaFieldName LIKE 'MetaInformation'
left join [dbMarekCommerceManager_production].[dbo].[CatalogContentProperty] dd with(nolock) on c.ObjectId= dd.objectId and dd.LongString LIKE '%PosType%' and dd.MetaFieldName LIKE 'MetaInformation'
left join [dbMarekCommerceManager_production].[dbo].[CatalogContentProperty] ee with(nolock) on c.ObjectId= ee.objectId and ee.LongString LIKE '%POSCostCategory%' and ee.MetaFieldName LIKE 'MetaInformation'
left join [dbMarekCommerceManager_production].[dbo].[CatalogContentProperty] ff with(nolock) on c.ObjectId= ff.objectId and ff.LongString LIKE '%ComplianceApprovalDate%' and ff.MetaFieldName LIKE 'MetaInformation'
left join [dbMarekCommerceManager_production].[dbo].[CatalogEntryRelation] cer with (nolock) on cer.ChildEntryId = ce.CatalogEntryId

	  LEFT join [dbMarekCommerceManager_production].[dbo].[PriceDetail] pd with(nolock) on ce.code = pd.CatalogEntryCode and ce.ClassTypeId like 'Variation'

WHERE ce.CatalogId = 123 and c.MetaFieldName = 'sku' and c.MetaClassId IN (1029,1035,1043,30) --and c.objectid  LIKE '19363'


