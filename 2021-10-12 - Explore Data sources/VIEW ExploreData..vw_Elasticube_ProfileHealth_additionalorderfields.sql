   
  SELECT  oaf.[Id]
      ,oaf.[EpiOrderId]
      ,oaf.[Name]
      ,oaf.[Value]
      ,oaf.[DisplayName]
      ,oaf.[Type]
	  ,oaf2.name AS Field2Name
	  ,oaf2.value AS Field2Value
	  ,oaf2.displayname AS Field2DisplayName
  FROM [dbMarekEF_production].[dbo].[OrderHistories] oh  with(nolock) 
  LEFT JOIN [dbMarekEF_production].[dbo].[OrderAdditionalFields] oaf with(nolock) on oh.EpiOrderId = oaf.EpiOrderId and oaf.name = 'CheckoutField1'
  LEFT JOIN [dbMarekEF_production].[dbo].[OrderAdditionalFields] oaf2 with(nolock)  on oaf.EpiOrderId = oaf2.EpiOrderId and oaf2.name = 'CheckoutField2'
    where oh.SiteId = 79119 

