 SELECT  oaf.[Id]
      ,oaf.[EpiOrderId]
      ,oaf.[Name]
      ,oaf.[Value] as 'ProjectType'
      ,oaf.[DisplayName]
      ,oaf.[Type]
	  ,oaf2.name AS DestinationTypeName
	  ,oaf2.value AS 'DestinationType'
	  ,oaf2.displayname AS DestinationTypeDisplayName
	   ,oaf3.name AS RetailersTypeName
	  ,oaf3.value AS 'Retailers'
	  ,oaf3.displayname AS RetailersDisplayName

  FROM [dbMarekEF_production].[dbo].[OrderHistories] oh  with(nolock) 
  LEFT JOIN [dbMarekEF_production].[dbo].[OrderAdditionalFields] oaf with(nolock) on oh.EpiOrderId = oaf.EpiOrderId and oaf.name = 'Project Type'
  LEFT JOIN [dbMarekEF_production].[dbo].[OrderAdditionalFields] oaf2 with(nolock)  on oaf.EpiOrderId = oaf2.EpiOrderId and oaf2.name = 'Destination Type'
  LEFT JOIN [dbMarekEF_production].[dbo].[OrderAdditionalFields] oaf3 with(nolock)  on oaf.EpiOrderId = oaf3.EpiOrderId and oaf3.name = 'Retailers'
    where oh.SiteId = 134350

