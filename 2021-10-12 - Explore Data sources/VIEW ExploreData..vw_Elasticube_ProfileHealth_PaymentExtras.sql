-- VIEW CODE
  SELECT 
    ofp.*
    ,oh.OrderTrackId
    ,oh.CreationDate
    ,oh.UserName
    ,ofcp.*
  FROM [dbMarekCommerceManager_production].[dbo].[OrderFormPayment] ofp  with(nolock)
  LEFT JOIN [dbMarekEF_production].[dbo].[OrderHistories] oh  with(nolock) on oh.EpiOrderId = ofp.OrderGroupId
  LEFT JOIN [dbMarekCommerceManager_production].[dbo].[OrderFormPayment_CustomPayment] ofcp with(nolock) on ofcp.ObjectId = ofp.PaymentId
  WHERE oh.SiteId = 79119


-- FIELDS BY SOURCES
SELECT TOP 1000 
			-- [OrderFormPayment]
			[PaymentId]
      ,[OrderFormId]
      ,[OrderGroupId]
      ,[BillingAddressId]
      ,[PaymentMethodId]
      ,[PaymentMethodName]
      ,[CustomerName]
      ,[Amount]
      ,[PaymentType]
      ,[ValidationCode]
      ,[AuthorizationCode]
      ,[TransactionType]
      ,[TransactionID]
      ,[Status]
      ,[ImplementationClass]
      ,[ProviderTransactionID]
			-- [OrderHistories]
      ,[OrderTrackId]
      ,[CreationDate]
      ,[UserName]
			-- [OrderFormPayment_CustomPayment]
      ,[ObjectId]
      ,[CreatorId]
      ,[Created]
      ,[ModifierId]
      ,[Modified]
      ,[Field1]
      ,[Field2]
      ,[Field3]
      ,[Field4]
      ,[Field5]
      ,[PaymentConfigurationId]
      ,[Field1Name]
      ,[Field2Name]
      ,[Field3Name]
      ,[Field4Name]
      ,[Field5Name]
      ,[Field1Text]
      ,[Field2Text]
      ,[Field3Text]
      ,[Field4Text]
      ,[Field5Text]
  FROM [ExploreData].[dbo].[vw_Elasticube_ProfileHealth_PaymentExtras]