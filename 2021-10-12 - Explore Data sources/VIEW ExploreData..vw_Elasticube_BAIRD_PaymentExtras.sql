     
  SELECT 
ofp.*
,oh.OrderTrackId
,oh.CreationDate
,oh.UserName
,ofcp.*
		--[PaymentId]
  --    ,[OrderFormId]
  --    ,[OrderGroupId]
  --    ,[BillingAddressId]
  --    ,[PaymentMethodId]
  --    ,[PaymentMethodName]
  --    ,[CustomerName]
  --    ,[Amount]
  --    ,[PaymentType]
  --    ,[ValidationCode]
  --    ,[AuthorizationCode]
  --    ,[TransactionType]
  --    ,[TransactionID]
  --    ,[Status]
  --    ,[ImplementationClass]
  --    ,[ProviderTransactionID]
  FROM [dbMarekCommerceManager_production].[dbo].[OrderFormPayment] ofp  with(nolock)
  LEFT JOIN [dbMarekEF_production].[dbo].[OrderHistories] oh  with(nolock) on oh.EpiOrderId = ofp.OrderGroupId
  LEFT JOIN [dbMarekCommerceManager_production].[dbo].[OrderFormPayment_CustomPayment] ofcp with(nolock) on ofcp.ObjectId = ofp.PaymentId
  WHERE oh.SiteId = 123399
