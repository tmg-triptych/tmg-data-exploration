USE [ExploreData]
GO
/****** Object:  StoredProcedure [dbo].[ADD_NJINV]    Script Date: 12.10.2021 21:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ADD_NJINV] 
AS

BEGIN
  SET NOCOUNT ON;


 INSERT INTO [ExploreData].[dbo].[NJOY_DailyInventory] 


SELECT 
  ce.CatalogEntryId
  ,cer.ParentEntryId as ParentEntryID
  ,ce.CatalogId
  ,ce.Name
  ,ce.code
  ,ce.ClassTypeId
  ,ce.MetaClassId
  ,ce.ContentGuid
  ,iss.[PurchaseAvailableQuantity]
  ,iss.[PreorderAvailableQuantity]
  ,iss.[BackorderAvailableQuantity]
  ,iss.[PurchaseRequestedQuantity]
  ,iss.[PreorderRequestedQuantity]
  ,iss.[BackorderRequestedQuantity]
  ,getdate()
FROM [dbMarekCommerceManager_production].dbo.catalogentry ce with(nolock) 
LEFT JOIN  [dbMarekCommerceManager_production].[dbo].[InventoryService] iss with(nolock) on iss.CatalogEntryCode = ce.Code
left join [dbMarekCommerceManager_production].[dbo].[CatalogEntryRelation] cer with (nolock) on cer.ChildEntryId = ce.CatalogEntryId
WHERE ce.CatalogId = 185 and ce.ClassTypeId = 'Variation'

END
