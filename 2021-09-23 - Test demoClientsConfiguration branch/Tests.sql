-- Testing changes on example clients

-- There's "Release Date" column in clients configaration table
-- - filled for MGIC (256) and Baird MOD (1)
-- - empty for others, for example, Masonite (128)
SELECT 'Prod', * FROM DWH_CurrentStateLayer..CS_Client WHERE AccessMask & (1 + 128 + 256) > 0;
SELECT 'Stage', * FROM DWH_Stage_CurrentStateLayer..CS_Client WHERE AccessMask & (1 + 128 + 256) > 0;

----------------------------------------------------------
-- New property ExcludeTestEvent added to several tables
----------------------------------------------------------
-- CS_Login
SELECT 'Prod', AccessMask, COUNT(*) Cnt, MIN(LoginDate) MinDate FROM DWH_CurrentStateLayer..CS_Login WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, ExcludeTestEvent, COUNT(*) Cnt, MIN(LoginDate) MinDate FROM DWH_Stage_CurrentStateLayer..CS_Login WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask, ExcludeTestEvent;

-- CS_Order
SELECT 'Prod', AccessMask, COUNT(*) Cnt, MIN(OrderDate) MinDate FROM DWH_CurrentStateLayer..CS_Order WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, ExcludeTestEvent, COUNT(*) Cnt, MIN(OrderDate) MinDate FROM DWH_Stage_CurrentStateLayer..CS_Order WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask, ExcludeTestEvent;

-- CS_OrderLineItem
SELECT 'Prod', AccessMask, COUNT(*) Cnt FROM DWH_CurrentStateLayer..CS_OrderLineItem WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, ExcludeTestEvent, COUNT(*) Cnt FROM DWH_Stage_CurrentStateLayer..CS_OrderLineItem WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask, ExcludeTestEvent;

-- CS_ProductOrder
SELECT 'Prod', AccessMask, COUNT(*) Cnt FROM DWH_CurrentStateLayer..CS_ProductOrder WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, ExcludeTestEvent, COUNT(*) Cnt FROM DWH_Stage_CurrentStateLayer..CS_ProductOrder WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask, ExcludeTestEvent;

-- CS_Payment
SELECT 'Prod', AccessMask, COUNT(*) Cnt FROM DWH_CurrentStateLayer..CS_Payment WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, ExcludeTestEvent, COUNT(*) Cnt FROM DWH_Stage_CurrentStateLayer..CS_Payment WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask, ExcludeTestEvent;


-- CS_AGG_UserActivity
SELECT 'Prod', AccessMask, SUM(LoginsCount) LoginsCount, MIN(FirstLoginDate) FirstLoginDate, MIN(FirstOrderDate) FirstOrderDate FROM DWH_CurrentStateLayer..CS_AGG_UserActivity WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, ExcludeTestEvent, SUM(LoginsCount) LoginsCount, MIN(FirstLoginDate) FirstLoginDate, MIN(FirstOrderDate) FirstOrderDate FROM DWH_Stage_CurrentStateLayer..CS_AGG_UserActivity WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask, ExcludeTestEvent;

-- CS_AGG_VariationOrder
SELECT 'Prod', AccessMask, SUM(OrdersCount) OrdersCount, MIN(FirstOrderDate) FirstOrderDate FROM DWH_CurrentStateLayer..CS_AGG_VariationOrder WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, ExcludeTestEvent, SUM(OrdersCount) OrdersCount, MIN(FirstOrderDate) FirstOrderDate FROM DWH_Stage_CurrentStateLayer..CS_AGG_VariationOrder WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask, ExcludeTestEvent;


---------------------------------------------------------
-- Used by DWH_Stage_Client_MGIC/DWH_Client_MGIC views
---------------------------------------------------------
-- _MGIC..[Login] - no changes (there was filter by date already)
SELECT 'Prod', AccessMask, COUNT(*) Cnt, MIN(LoginDate) MinDate FROM DWH_Client_MGIC..[Login] WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, COUNT(*) Cnt, MIN(LoginDate) MinDate FROM DWH_Stage_Client_MGIC..[Login] WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;

-- _MGIC..[Order] - no changes (there was filter by date already)
SELECT 'Prod', AccessMask, COUNT(*) Cnt, MIN(OrderDate) MinDate FROM DWH_Client_MGIC..[Order] WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, COUNT(*) Cnt, MIN(OrderDate) MinDate FROM DWH_Stage_Client_MGIC..[Order] WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask, ExcludeTestEvent;

-- _MGIC..[OrderLineItem] - filter wasn't applied before, fixed
SELECT 'Prod', AccessMask, COUNT(*) Cnt FROM DWH_Client_MGIC..OrderLineItem WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, COUNT(*) Cnt FROM DWH_Stage_Client_MGIC..OrderLineItem WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask;

-- _MGIC..[ProductOrder] - filter wasn't applied before, fixed
SELECT 'Prod', AccessMask, COUNT(*) Cnt FROM DWH_Client_MGIC..ProductOrder WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, COUNT(*) Cnt FROM DWH_Stage_Client_MGIC..ProductOrder WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask;

-- _MGIC..[Payment] - filter wasn't applied before, fixed
SELECT 'Prod', AccessMask, COUNT(*) Cnt FROM DWH_Client_MGIC..Payment WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, COUNT(*) Cnt FROM DWH_Stage_Client_MGIC..Payment WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;

-- _MGIC..AGG_UserActivity - filter wasn't applied before, fixed
SELECT 'Prod', AccessMask, SUM(LoginsCount) LoginsCount, MIN(FirstLoginDate) FirstLoginDate, MIN(FirstOrderDate) FirstOrderDate FROM DWH_Client_MGIC..AGG_UserActivity WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, SUM(LoginsCount) LoginsCount, MIN(FirstLoginDate) FirstLoginDate, MIN(FirstOrderDate) FirstOrderDate FROM DWH_Stage_Client_MGIC..AGG_UserActivity WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask, ExcludeTestEvent;

-- _MGIC..AGG_VariationOrder - filter wasn't applied before, fixed
SELECT 'Prod', AccessMask, SUM(OrdersCount) OrdersCount, MIN(FirstOrderDate) FirstOrderDate FROM DWH_Client_MGIC..AGG_VariationOrder WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, SUM(OrdersCount) OrdersCount, MIN(FirstOrderDate) FirstOrderDate FROM DWH_Stage_Client_MGIC..AGG_VariationOrder WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask, ExcludeTestEvent;

--------------------------------------------------------------
-- Used by DWH_Stage_Client_BairdMOD/DWH_Client_BairdMOD views
---------------------------------------------------------------
-- _BairdMOD..[Login] - filter wasn't applied before, fixed
SELECT 'Prod', AccessMask, COUNT(*) Cnt, MIN(LoginDate) MinDate FROM DWH_Client_BairdMOD..[Login] WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, COUNT(*) Cnt, MIN(LoginDate) MinDate FROM DWH_Stage_Client_BairdMOD..[Login] WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;

-- _BairdMOD..[Order] - filter wasn't applied before, fixed
SELECT 'Prod', AccessMask, COUNT(*) Cnt, MIN(OrderDate) MinDate FROM DWH_Client_BairdMOD..[Order] WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, COUNT(*) Cnt, MIN(OrderDate) MinDate FROM DWH_Stage_Client_BairdMOD..[Order] WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask, ExcludeTestEvent;

-- _BairdMOD..[OrderLineItem] - filter wasn't applied before, fixed
SELECT 'Prod', AccessMask, COUNT(*) Cnt FROM DWH_Client_BairdMOD..OrderLineItem WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, COUNT(*) Cnt FROM DWH_Stage_Client_BairdMOD..OrderLineItem WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;

-- _BairdMOD..[ProductOrder] - filter wasn't applied before, fixed
SELECT 'Prod', AccessMask, COUNT(*) Cnt FROM DWH_Client_BairdMOD..ProductOrder WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, COUNT(*) Cnt FROM DWH_Stage_Client_BairdMOD..ProductOrder WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;

-- _BairdMOD..[Payment] - filter wasn't applied before, fixed
SELECT 'Prod', AccessMask, COUNT(*) Cnt FROM DWH_Client_BairdMOD..Payment WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, COUNT(*) Cnt FROM DWH_Stage_Client_BairdMOD..Payment WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;

-- _BairdMOD..AGG_UserActivity - filter wasn't applied before, fixed
SELECT 'Prod', AccessMask, SUM(LoginsCount) LoginsCount, MIN(FirstLoginDate) FirstLoginDate, MIN(FirstOrderDate) FirstOrderDate FROM DWH_Client_BairdMOD..AGG_UserActivity WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, SUM(LoginsCount) LoginsCount, MIN(FirstLoginDate) FirstLoginDate, MIN(FirstOrderDate) FirstOrderDate FROM DWH_Stage_Client_BairdMOD..AGG_UserActivity WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask, ExcludeTestEvent;

-- _BairdMOD..AGG_VariationOrder - filter wasn't applied before, fixed
SELECT 'Prod', AccessMask, SUM(OrdersCount) OrdersCount, MIN(FirstOrderDate) FirstOrderDate FROM DWH_Client_BairdMOD..AGG_VariationOrder WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask ORDER BY AccessMask;
SELECT 'Stage', AccessMask, SUM(OrdersCount) OrdersCount, MIN(FirstOrderDate) FirstOrderDate FROM DWH_Stage_Client_BairdMOD..AGG_VariationOrder WHERE AccessMask & (1 + 128 + 256) > 0 GROUP BY AccessMask, ExcludeTestEvent ORDER BY AccessMask, ExcludeTestEvent;
