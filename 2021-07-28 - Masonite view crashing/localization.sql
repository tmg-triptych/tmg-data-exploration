USE [dbMarekCommerceManager_production]
select distinct --top 1000
		oih.EpiLineItemID as LineItemID
		,ofpc.Field2 
		,oh.CreationDate 
		,charindex('|',ofpc.Field2) - charindex('-',ofpc.Field2)
		,case
		  when ofpc.Field1 like '%PUSH%' then replace(substring(ofpc.Field1,charindex('-',ofpc.Field1), charindex('|',ofpc.Field1) - charindex('-',ofpc.Field1) ),'- ','')
			when ofpc.Field2 like '%PUSH%' AND charindex('|',ofpc.Field2) - charindex('-',ofpc.Field2) > 0 then replace(substring(ofpc.Field2,charindex('-',ofpc.Field2), charindex('|',ofpc.Field2) - charindex('-',ofpc.Field2) ),'- ','')
		else '' end as PushNumber
		--,(case when ofpc.Field2 like '%|%' and ofpc.Field2 like '%-%' then right(ofpc.Field2,len(ofpc.Field2) - charindex('|',ofpc.Field2)) else '' end) as GLAssignment
		--,(case when PaymentMethod like '%invoice%' then ofpc.Field2 else ofpc.Field1 end) as ApproverName
		from dbMarekEF_production..OrderHistories oh with(nolock)
		join dbMarekEF_production..OrderItemsHistories oih with(nolock) on oh.EpiOrderId = oih.EpiOrderId
		join dbMarekEF_production..OrderItemComponentsHistories oich with(nolock) on oih.EpiLineItemId = oich.EpiLineItemId
		join dbMarekCommerceManager_production..[CatalogEntry] ce with(nolock) on oich.SkuID = ce.ContentGuid
		left join dbMarekCommerceManager_production.[dbo].[OrderFormPayment] ofp with(nolock) on ofp.OrderGroupId = oh.EpiOrderId and ofp.PaymentMethodName NOT LIKE 'Coop Payment'
		left join dbMarekCommerceManager_production..OrderFormPayment_CustomPayment ofpc with(nolock) on ofp.PaymentId = ofpc.ObjectId
		left join dbMarekCommerceManager_production..Cls_Contact clc with(nolock) on oh.UserId = clc.ContactId
	where ce.catalogid = 83 
	AND ofpc.Field2 like '%PUSH%' and charindex('|',ofpc.Field2) - charindex('-',ofpc.Field2) < 0
	and clc.Email not in (select distinct email from [Triptych_Utility].dbo.exclude_from_reports with(nolock))
	order by oih.EpiLineItemID


		
