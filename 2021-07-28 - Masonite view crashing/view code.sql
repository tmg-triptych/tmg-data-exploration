USE [dbMarekCommerceManager_production]
--GO

/***** Object:  View [dbo].[vw_Elasticube_Masonite]    Script Date: 7/28/2021 9:34:57 AM *****/
--SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER view [dbo].[vw_Elasticube_Masonite]

as


select distinct --top 1000
		'Masonite' as Catalog
		,ce.CatalogId
		,oih.EpiLineItemID as LineItemID
		,oh.EpiOrderId as OrderGroupId
		,oih.Quantity
		,convert(date ,DATEADD(week, DATEDIFF(day, 0, ogp.Created)/7, 0)) AS WeekDate
		,coalesce(oih.TrackingNumber,'') as  TrackingNumber
		,(case when oh.orderstate in (0) then 'Received'
			when oh.orderstate in (10) then 'In Review'
			when oh.orderstate in (20) then 'Rejected'
			when oh.orderstate in (30) then 'Received'
			when oh.orderstate in (40) then 'In Progress'
			when oh.orderstate in (50) and oich.ShipState in (0,20) then 'Completed'
			when oh.orderstate in (60) then 'Failed'
			when oh.orderstate in (70) then 'Canceled'
			when oh.orderstate in (80) then 'Returned'
			else 'In Progress' end) as OrderStatus
		,oih.name as Name
		,oih.SKU
		,oih.Price
		,dateadd(hh,-6,oich.ShipDate) as ShipDate
		,(case when oh.orderstate in (50) then Datediff(dd,ogp.Created , oich.ShipDate) else null end) as OrderToShip
		,oga2.Organization as BusinessPartnerName
		,oh.OrderTrackId as OrderNumber
		,dateadd(hh,-6,ogp.Created) as OrderDate 
		,coalesce(cp3.LongString,cp4.LongString) as Media
		,ad.CompanyName
		,ad.FirstName
		,ad.LastName
		,ad.Line1 as Line1
		,ad.Line2 as Line2
		,ad.City as City
		,ad.State as [State]
		,ad.Zip as Zip
		,oh.Name as CustomerName
		,oh.userId as CustomerID
		,ss.ShippingMethodName as ShippingMethod
		,oh.ShippingTotal
		,oh.Total
		,oh.PaymentMethod
		,ad.Country as CountryCode
		,coalesce(ad.CountryName,oga2.countryname) as CountryName
		,replace(clc.Email,char(63),'') as UserName
		,(case when clc.Email like '%++%' then replace(clc.Email,SUBSTRING(clc.Email, CHARINDEX('++', clc.Email), (CHARINDEX('@',clc.Email) - 1) - ((CHARINDEX('++', clc.Email)) + Len('@') - 2)),'')
				else clc.Email end) as Email
		,convert(varchar(256),(case when oh.ApproveState = 2 then 'Pending'
			   when oh.ApproveState = 3 then 'Approved'
			   when oh.ApproveState = 4 then 'Rejected'
			   else '' end)) as ApproveState
		,ceparent.CatalogEntryId as ParentEntryId
		,cp5.LongString as VendorName
		,oich.Id as ItemCompID
		,oh.Tax
		,oh.Coop
		,oh.HandlingCharge
		--,appr.ApproverName
		,oh.SubTotal
		,oh.DiscountSum
		,coalesce( od.Code, pin.Name) as DiscountCode
		,coalesce(ad.FirstName,oga2.FirstName,bill.firstname) as BillingFirstName
		,coalesce(ad.LastName,oga2.LastName,bill.lastname) as BillingLastName
		,coalesce(ad.Line1,oga2.Line1,bill.Line1) as BillingLine1
		,coalesce(ad.Line2,oga2.Line2,bill.Line2) as BillingLine2
		,coalesce(ad.City,oga2.City,bill.City) as BillingCity
		,coalesce(ad.State,oga2.State,bill.State) as [BillingState]
		,coalesce(ad.Zip, oga2.PostalCode,bill.PostalCode) as BillingZip
		,bill.Reference
		,coalesce(bill.CountryName,oga2.CountryName) as BillingCountryName
		,coalesce(bill.Organization,oga2.Organization) as BillingCompanyName
		,coalesce(bill.DaytimePhoneNumber,oga2.DaytimePhoneNumber) as BillingPhone
		,(case when PaymentMethod like '%invoice%' then ofpc.Field1 else ofpc.Field2 end) as Customer
		--,ofpc.Field1 as ApproverName
		--,ofpc.Field2 as Customer
		--HUTTIG BLDG PROD COLUMBUS - PUSH008175|
		,(case when ofpc.Field1 like '%PUSH%' then replace(substring(ofpc.Field1,charindex('-',ofpc.Field1), charindex('|',ofpc.Field1) - charindex('-',ofpc.Field1) ),'- ','')
			   when ofpc.Field2 like '%PUSH%' then replace(substring(ofpc.Field2,charindex('-',ofpc.Field2), charindex('|',ofpc.Field2) - charindex('-',ofpc.Field2) ),'- ','')
			   else '' end) as PushNumber
		,(case when ofpc.Field4Name like '%GL%' then ofpc.Field4 else '' end) as GlCode 
		,(case when ofpc.Field2 like '%|%' and ofpc.Field2 like '%-%' then right(ofpc.Field2,len(ofpc.Field2) - charindex('|',ofpc.Field2)) else '' end) as GLAssignment
		,(case when PaymentMethod like '%invoice%' then ofpc.Field2 else ofpc.Field1 end) as ApproverName
		,  CONVERT(VARCHAR(1000),(CONVERT(VARBINARY(8), cc.Id)), 2) AS DOCID
			from dbMarekEF_production..OrderHistories oh with(nolock)
		join dbMarekEF_production..OrderItemsHistories oih with(nolock) on oh.EpiOrderId = oih.EpiOrderId
		join dbMarekEF_production..OrderItemComponentsHistories oich with(nolock) on oih.EpiLineItemId = oich.EpiLineItemId
		join dbMarekCommerceManager_production..[CatalogEntry] ce with(nolock) on oich.SkuID = ce.ContentGuid
		--join dbMarekCommerceManager_production..LineItem l with(nolock) on oih.EpiLineItemID = l.LineItemId
		left join dbMarekCommerceManager_production..[CatalogContentProperty] cp with(nolock) ON ce.CatalogEntryId = cp.ObjectId AND cp.MetaFieldName = 'SKU'
		left join dbMarekCommerceManager_production..[CatalogContentProperty] cp2 with(nolock) ON ce.CatalogEntryId = cp2.ObjectId AND cp2.MetaFieldName = 'DisplayName'
		left join dbMarekCommerceManager_production..[CatalogEntry] ceparent with(nolock) on oich.ToolId = ce.ContentGuid
		
		--left join dbMarekCommerceManager_production.[dbo].[CatalogEntryRelation] cer with(nolock) on cer.ChildEntryId = ce.CatalogEntryId

		left join dbMarekCommerceManager_production..[CatalogContentProperty] cp6 with(nolock) ON ceparent.CatalogEntryId = cp6.ObjectId AND cp6.MetaFieldName = 'SKU'
		left join dbMarekCommerceManager_production..[CatalogContentProperty] cp3 with(nolock) ON ceparent.CatalogEntryId = cp3.ObjectId AND cp3.MetaFieldName = 'ToolTypeName'
		left join dbMarekCommerceManager_production..[CatalogContentProperty] cp4 with(nolock) ON ce.CatalogEntryId = cp4.ObjectId AND cp4.MetaFieldName = 'ToolTypeName'
		left join dbMarekCommerceManager_production..[CatalogContentProperty] cp5 with(nolock) ON ceparent.CatalogEntryId  = cp5.ObjectId AND cp5.MetaFieldName = 'Vendor'
		left join dbMarekCommerceManager_production.dbo.[OrderGroup_PurchaseOrder] ogp with(nolock) on oh.EpiOrderId = ogp.ObjectId -- OrderDate
		Left join [dbMarekEF_production].[dbo].[Customizations] cc on cc.ComponentId = oich.Id
		left join dbMarekCommerceManager_production..shipment sh with(nolock) on oh.EpiOrderId = sh.OrderGroupId 
		left join dbMarekEF_production..Addresses ad with(nolock) on convert(varchar(100),ad.id) = convert(varchar(100),sh.ShippingAddressId)
		left join dbMarekEF_production.[dbo].[OrderShipments] oss with(nolock) on oih.EpiLineItemID = oss.EpiLineItemId
		left join dbMarekEF_production.[dbo].[Shipments] ss with(nolock) on oss.Shipmentid = ss.id
		left join dbMarekCommerceManager_production..OrderGroupAddress oga2 with(nolock) on oga2.Name = ss.ShippingAddressId and oh.EpiOrderId = oga2.OrderGroupID
		left join dbMarekCommerceManager_production..Cls_Contact clc with(nolock) on oh.UserId = clc.ContactId
		--left join dbMarekCommerceManager_production.[dbo].[PromotionUsage] pu with(nolock) on oh.EpiOrderId = pu.OrderGroupId
		left join dbMarekCommerceManager_production.[dbo].[OrderFormPayment] ofp with(nolock) on ofp.OrderGroupId = oh.EpiOrderId and ofp.PaymentMethodName NOT LIKE 'Coop Payment'
		left join dbMarekCommerceManager_production..OrderFormPayment_CustomPayment ofpc with(nolock) on ofp.PaymentId = ofpc.ObjectId --and ofpc.payment
		--left join (
		--	select EpiOrderId,cl.FullName as ApproverName,max(oah.CreationDate) as LastApprovedActivityDate
		--	from dbMarekEF_production.[dbo].[OrderApprovalHistories] oah with(nolock)
		--		join dbMarekCommerceManager_production.dbo.aspnet_Membership am with(nolock) on oah.ApproverId = am.UserId
		--		join dbMarekCommerceManager_production..cls_contact cl with(nolock) on am.Email = cl.Email
		--	group by EpiOrderId,cl.FullName
		--)as appr on appr.EpiOrderId = oh.EpiOrderId
		--left join (
		--	select CatalogEntryId,CatalogId,Name ,ccp.LongString as SKU
		--	from [dbo].[CatalogEntry] ce with(nolock)
		--	join [dbo].[CatalogContentProperty] ccp with(nolock) on ce.CatalogEntryId = ccp.ObjectId and ccp.MetaFieldName = 'SKU'
		--	where catalogid = 83 and classtypeid not in ('Variation','Package') and ce.EndDate >= getdate() and ce.Name not like '%-test%' and ce.Name not like '%test_%'
		--) parent on oih.Sku = parent.Sku
		left join(
			select distinct 
				oh.EpiOrderId
				,oga.[FirstName]
				,oga.[LastName]
				,oga.[Line1]
				,oga.[Line2]
				,oga.[City]
				,oga.[State]
				,oga.[PostalCode]
				,oga.Email
				,oga.Organization
				,oga.CountryName
				,oga.DaytimePhoneNumber
				,ofx.DataCollectionFieldViewData as Reference
			from dbMarekEF_production..OrderHistories oh with(nolock)
			join [dbMarekCommerceManager_production]..OrderForm orf with(nolock) on oh.EpiOrderId = orf.OrderGroupId
			join [dbMarekCommerceManager_production].[dbo].[OrderFormEx] ofx with(nolock) on orf.OrderFormId = ofx.ObjectId
			join [dbMarekCommerceManager_production].[dbo].[OrderGroupAddress] oga with(nolock) on orf.BillingAddressId = oga.Name
			where PaymentMethod = 'credit card' and oh.siteid = 31820
		) as bill on oh.EpiOrderId = bill.EpiOrderId
		left join [dbMarekEF_production].[dbo].[OrderDiscounts] od with(nolock) on od.EpiOrderId = oh.EpiOrderId
		left join dbMarekCommerceManager_production..OrderForm ofd with(nolock) on oh.EpiOrderId = ofd.OrderGroupId
		left join dbMarekCommerceManager_production..PromotionInformation pin with(nolock) on ofd.OrderFormId = pin.OrderFormId
	where ce.catalogid = 83
		and clc.Email not in (select distinct email from [Triptych_Utility].dbo.exclude_from_reports with(nolock))

	



GO