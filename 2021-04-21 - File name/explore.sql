-- Commerce data

SELECT * FROM  dbMarekCommerceManager_production..CatalogContentProperty WHERE MetaFieldName = 'FileName' AND ObjectId IN (36091) ORDER BY MetaFieldName;
-- 36091 ~/link/4cff1ae43f964624bfe577ae1722d429.aspx

SELECT * FROM [dbMarekCMS_production].[dbo].[tblWorkContent] WHERE Name = 'UHG_Access.eps'

SELECT * FROM dbMarekCommerceManager_production..CatalogEntry Where CatalogEntryId In (36091) 
-- 341B37DF-1C70-4725-B83C-F9582135665C

SELECT * FROM [dbMarekCMS_production].[dbo].[tblWorkContent] WHERE pkID = 31077
--------------------------------------------
-- CMS data

-- Property we need - here
SELECT * FROM [dbMarekCMS_production].[dbo].[tblWorkContent] WHERE Name = 'UHG_Access.eps'
SELECT * FROM [dbMarekCMS_production].[dbo].[tblWorkContent] WHERE fkContentId = 31077
SELECT * FROM [dbMarekCMS_production].[dbo].[tblWorkContent] WHERE pkID = 31077
-- pkID 47016  fkContentID	31077 Name = 'UHG_Access.eps'


SELECT * FROM [dbMarekCMS_production].[dbo].[tblContent] WHERE pkID = 47016 
-- ContentGUID = '9CC67E3F-963D-4CE0-9FFC-2CAB5DA56E01'
-- ContentAssetsID = '6DF9358B-36B2-4469-88BA-5C20C80A99EB'
SELECT * FROM [dbMarekCMS_production].[dbo].[tblContent] WHERE pkID = 31077 
-- 4CFF1AE4-3F96-4624-BFE5-77AE1722D429

SELECT * FROM [dbMarekCMS_production].[dbo].tblContentProperty  WHERE pkID = 47016 

-- dbmarekcommercemanager_production.dbo.vw_elasticube_productconfiguration_pivot



SELECT
	SUBSTRING(fn.LongString, 9, LEN(fn.LongString)-9-4) AS FileName,
	ce.CatalogEntryId
FROM (
  SELECT * FROM dbMarekCommerceManager_production..CatalogContentProperty WHERE MetaFieldName = 'FileName' AND LongString IS NOT NULL AND LEN(LongString) > 13
) fn
LEFT JOIN dbMarekCommerceManager_production..CatalogEntry ce ON ce.CatalogEntryId = fn.ObjectId
--left join [dbMarekCMS_production].[dbo].[tblContent] tc on convert(varchar(50),tc.ContentGUID) = app.LongString
--left join dbMarekCMS_production..tblWorkContent twc on twc.fkContentID = tc.pkID

-----------------------------
-- Data connection from Kate
-----------------------------
-- for internal links (like ~/link/4cff1ae43f964624bfe577ae1722d429.aspx)
SELECT 
  'dbMarekCommerceManager_production..CatalogContentProperty',ObjectId, LongString
FROM dbMarekCommerceManager_production..CatalogContentProperty fn
WHERE fn.ObjectId = 36091 AND fn.MetaFieldName = 'FileName'
;

SELECT
  'dbMarekCMS_production..tblContent', pkID, ContentGUID
FROM dbMarekCMS_production..tblContent tc
WHERE ContentGUID = '4CFF1AE4-3F96-4624-BFE5-77AE1722D429'
;

SELECT
  'dbMarekCMS_production..tblWorkContent', fkContentID, Name
FROM dbMarekCMS_production..tblWorkContent twc
WHERE fkContentID = 31077
;

-- for external links (like )
-- 40529	https://triptychcdnuhctalent.themarekgroup.net/442584_uhg_diversity_onepager_0218_ma_native.zip	/triptychcdnuhctalent.themarekgroup.net/442584_uhg_diversity_onepager_0218_ma_nativ	NULL	NULL	NULL
SELECT 
  'dbMarekCommerceManager_production..CatalogContentProperty', ObjectId, LongString
FROM dbMarekCommerceManager_production..CatalogContentProperty fn
WHERE
  fn.MetaFieldName = 'FileName' AND 
	LongString NOT LIKE '~/link/%'
;


-- Common case
SELECT
  ce_data.CatalogEntryId,
	ce_data.FileLink,
	ce_data.FileNameFromLink,
	tc.ContentGUID,
	tc.pkID,
	CASE
	  WHEN ce_data.FileLink LIKE '~/link/%' THEN twc.Name
		ELSE ce_data.FileLink
	END AS Name
FROM (
	SELECT
		fn.LongString AS FileLink,
		CASE
		  WHEN fn.LongString LIKE '~/link/%' AND LEN(fn.LongString) > 8 AND CHARINDEX('.', fn.LongString) > 8 THEN 
			  SUBSTRING(fn.LongString, 8, CHARINDEX('.', fn.LongString)-8)
		END AS FileNameFromLink,
		ce.CatalogEntryId
	FROM dbMarekCommerceManager_production..CatalogEntry ce
	LEFT JOIN dbMarekCommerceManager_production..CatalogContentProperty fn ON ce.CatalogEntryId = fn.ObjectId AND fn.MetaFieldName = 'FileName' 
	WHERE fn.LongString IS NOT NULL
	--  AND CatalogEntryId IN (1914, 6496, 40564)
) ce_data
LEFT JOIN dbMarekCMS_production..tblContent tc ON REPLACE(CAST(tc.ContentGUID AS VARCHAR(50)), '-', '') = ce_data.FileNameFromLink
LEFT JOIN dbMarekCMS_production..tblWorkContent twc ON twc.fkContentID = tc.pkID
;