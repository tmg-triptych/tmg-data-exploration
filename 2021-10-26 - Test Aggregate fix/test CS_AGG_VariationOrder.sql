------------------------
-- Merge & Compare
------------------------ 
SELECT * FROM DWH_Stage_CurrentStateLayer..CS_AGG_VariationOrder
-- Merge old & new tables
DROP TABLE IF EXISTS #Test_CS_AGG_VariationOrder
SELECT
   COALESCE(old.VariationId, new.VariationId) AS ContactId,
	 COALESCE(old.SiteId, new.SiteId) AS SiteId,
	 COALESCE(old.AccessMask, new.AccessMask) AS AccessMask,
	 COALESCE(old.ExcludeTestEvent, new.ExcludeTestEvent) AS ExcludeTestEvent,
	 CASE
	   WHEN old.VariationId IS NOT NULL AND new.VariationId IS NOT NULL THEN 'match'
		 WHEN old.VariationId IS NULL AND new.VariationId IS NOT NULL THEN 'new only'
		 WHEN old.VariationId IS NOT NULL AND new.VariationId IS NULL THEN 'old only'
	 END AS Classified,
	 c.Name,
	 CASE
	   WHEN c.Name IN (
		   'AllianceFranchise',
			 'Baird MOD',
			 'Caleffi',
			 'EmergeStore',
			 'Ho-Chunk Rewards',
			 'Ipso',
			 'MarekInventory',
			 'Masonite',
			 'MGIC',
			 'NJOY',
			 'Primus',
			 'ProfileHealth',
			 'UHCTEXAS',
			 'Wright'
		 ) THEN 'active'
		 ELSE 'test mode'
	END AS ClientType
INTO #Test_CS_AGG_VariationOrder
FROM (SELECT DISTINCT VariationId, SiteId, AccessMask, ExcludeTestEvent FROM DWH_CurrentStateLayer..CS_AGG_VariationOrder) old
FULL OUTER JOIN DWH_Stage_CurrentStateLayer..CS_AGG_VariationOrder new ON
  old.VariationId = new.VariationId AND
	old.AccessMask = new.AccessMask AND
	old.ExcludeTestEvent = new.ExcludeTestEvent
LEFT JOIN DWH_Stage_CurrentStateLayer..CS_Client c ON c.AccessMask = COALESCE(old.AccessMask, new.AccessMask)

-- Aggregate results
SELECT
	Name,
	COUNT(CASE WHEN  Classified = 'match' THEN 1 END) AS match,
	COUNT(CASE WHEN  Classified = 'new only' THEN 1 END) AS new_only,
	COUNT(CASE WHEN  Classified = 'old only' THEN 1 END) AS old_only,
	(100*COUNT(CASE WHEN Classified = 'match' THEN 1 END)/ COUNT(*)) AS match_rate
FROM #Test_CS_AGG_VariationOrder
WHERE ClientType = 'active'
GROUP BY Name
ORDER BY match_rate

-- Name            match  new_only  old_only  match_rate
-- Ipso                6      2878         0           0
-- MarekInventory      0        27         0           0
-- Primus             25      2862         0           0
-- Ho-Chunk Rewards   10     17225         0           0
-- ProfileHealth     104       106         0          49
-- AllianceFranchise  94        71         0          56
-- EmergeStore        36        23         0          61
-- MGIC              892       518         0          63
-- NJOY               38        19         0          66
-- Baird MOD         287       107         0          72
-- Masonite         1207       394         0          75
-- Caleffi           105        31         0          77
-- Wright            228        34         0          87
-- UHCTEXAS          126        14         0          90

-- All is ok, just lots of products added