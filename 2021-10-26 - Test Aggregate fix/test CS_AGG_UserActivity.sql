------------------------
-- Change overview
------------------------ 
-- https://bitbucket.org/tmg-triptych/tmg-data/pull-requests/37

-- prod: error
SELECT
  *
FROM DWH_CurrentStateLayer..CS_AGG_UserActivity
WHERE ContactId IN ('772817F9-3E77-412D-89B2-B9F7BAA1CB39', '243EAFDB-690D-48A6-A024-C55FB27093F5')
-- 243EAFDB-690D-48A6-A024-C55FB27093F5	123399	1	0	1
-- 772817F9-3E77-412D-89B2-B9F7BAA1CB39	NULL	1	0	0

-- stage: fixed
SELECT
  *
FROM DWH_Stage_CurrentStateLayer..CS_AGG_UserActivity
WHERE ContactId IN ('772817F9-3E77-412D-89B2-B9F7BAA1CB39', '243EAFDB-690D-48A6-A024-C55FB27093F5')
-- 243EAFDB-690D-48A6-A024-C55FB27093F5	123399	1	0	1
-- 772817F9-3E77-412D-89B2-B9F7BAA1CB39	123399	1	0	0
-- 243EAFDB-690D-48A6-A024-C55FB27093F5	123399	1	0	0


------------------------
-- Unstructured mining
------------------------ 

-- But records counter decreased
SELECT COUNT(*) FROM DWH_CurrentStateLayer..CS_AGG_UserActivity
-- 278 702
SELECT COUNT(*) FROM DWH_Stage_CurrentStateLayer..CS_AGG_UserActivity
-- 195 093

-- Test composite primary key: check there's no duplicates
SELECT COUNT(*) FROM (SELECT ContactId, SiteId, AccessMask, ExcludeTestEvent FROM DWH_CurrentStateLayer..CS_AGG_UserActivity GROUP BY ContactId, SiteId, AccessMask, ExcludeTestEvent HAVING COUNT(*) > 1) r
-- 3 529 - there are duplicates
SELECT COUNT(*) FROM (SELECT ContactId, SiteId, AccessMask, ExcludeTestEvent FROM DWH_Stage_CurrentStateLayer..CS_AGG_UserActivity GROUP BY ContactId, SiteId, AccessMask, ExcludeTestEvent HAVING COUNT(*) > 1) r
-- 0 - no duplicates

-- duplicates examples
SELECT TOP 5 * FROM (SELECT ContactId, SiteId, AccessMask, ExcludeTestEvent FROM DWH_CurrentStateLayer..CS_AGG_UserActivity GROUP BY ContactId, SiteId, AccessMask, ExcludeTestEvent HAVING COUNT(*) > 1) r
-- 229F098A-B726-4D0A-BEAB-72BB7A47323A	11682	0	0
-- 08EB4A4F-153E-4A81-8BB4-08FB8FC0FA30	1125	0	0
-- 6DEDABCF-DA56-42A5-88F5-5F710ABD164E	89115	0	0
-- BFC75D4E-FFB7-4FAB-8AE6-21E0D2C0FE15	3483	0	0
-- 538BCCDE-2934-4C68-B810-762885CCFDE1	3908	0	0

-- Without duplicates - still decreased
SELECT COUNT(*) FROM (SELECT DISTINCT ContactId, SiteId, AccessMask, ExcludeTestEvent FROM DWH_CurrentStateLayer..CS_AGG_UserActivity) r
-- 243 328
SELECT COUNT(*) FROM (SELECT DISTINCT ContactId, SiteId, AccessMask, ExcludeTestEvent FROM DWH_Stage_CurrentStateLayer..CS_AGG_UserActivity) r
-- 195 093

-- Exclude AccessMask 0
SELECT COUNT(*) FROM (SELECT DISTINCT ContactId, SiteId, AccessMask, ExcludeTestEvent FROM DWH_CurrentStateLayer..CS_AGG_UserActivity WHERE AccessMask > 0) r
-- 165 753
SELECT COUNT(*) FROM (SELECT DISTINCT ContactId, SiteId, AccessMask, ExcludeTestEvent FROM DWH_Stage_CurrentStateLayer..CS_AGG_UserActivity WHERE AccessMask > 0) r
-- 167 301
-- ...better now


------------------------
-- Merge & Compare
------------------------ 

-- Merge old & new tables
DROP TABLE IF EXISTS #Test_CS_AGG_UserActivity
SELECT
   COALESCE(old.ContactId, new.ContactId) AS ContactId,
	 COALESCE(old.SiteId, new.SiteId) AS SiteId,
	 COALESCE(old.AccessMask, new.AccessMask) AS AccessMask,
	 COALESCE(old.ExcludeTestEvent, new.ExcludeTestEvent) AS ExcludeTestEvent,
	 CASE
	   WHEN old.ContactId IS NOT NULL AND new.ContactId IS NOT NULL THEN 'match'
		 WHEN old.ContactId IS NULL AND new.ContactId IS NOT NULL THEN 'new only'
		 WHEN old.ContactId IS NOT NULL AND new.ContactId IS NULL THEN 'old only'
	 END AS Classified,
	 c.Name,
	 CASE
	   WHEN c.Name IN (
		   'AllianceFranchise',
			 'Baird MOD',
			 'Caleffi',
			 'EmergeStore',
			 'Ho-Chunk Rewards',
			 'Ipso',
			 'MarekInventory',
			 'Masonite',
			 'MGIC',
			 'NJOY',
			 'Primus',
			 'ProfileHealth',
			 'UHCTEXAS',
			 'Wright'
		 ) THEN 'active'
		 ELSE 'test mode'
	END AS ClientType
INTO #Test_CS_AGG_UserActivity
FROM (SELECT DISTINCT  ContactId, SiteId, AccessMask, ExcludeTestEvent FROM DWH_CurrentStateLayer..CS_AGG_UserActivity) old
FULL OUTER JOIN DWH_Stage_CurrentStateLayer..CS_AGG_UserActivity new ON
  old.ContactId = new.ContactId AND
	old.AccessMask = new.AccessMask AND
	old.ExcludeTestEvent = new.ExcludeTestEvent
LEFT JOIN DWH_Stage_CurrentStateLayer..CS_Client c ON c.AccessMask = COALESCE(old.AccessMask, new.AccessMask)

-- Aggregate results
SELECT
	--ExcludeTestEvent,
	Name,
	COUNT(CASE WHEN  Classified = 'match' THEN 1 END) AS match,
	COUNT(CASE WHEN  Classified = 'new only' THEN 1 END) AS new_only,
	COUNT(CASE WHEN  Classified = 'old only' THEN 1 END) AS old_only,
	(100*COUNT(CASE WHEN Classified = 'match' THEN 1 END)/ COUNT(*)) AS match_rate
FROM #Test_CS_AGG_UserActivity
WHERE ClientType = 'active'
GROUP BY  Name --ExcludeTestEvent,
ORDER BY  match_rate --ExcludeTestEvent,

-- Name           match  new_only  old_only  match_rate
-- Ipso               0	       50	        0	          0
-- Primus             0	       75	        0	          0
-- NJOY              13	        5	        0	         72
-- MarekInventory     4	        1	        0	         80
-- AllianceFranchise 75	       17	        0	         81
-- Caleffi           62	       11	        0	         84
-- MGIC             216	       27	        1	         88
-- ProfileHealth	   51	        5	        0	         91
-- UHCTEXAS          77	        7	        0	         91
-- EmergeStore       79	        7	        0	         91
-- Baird MOD        699	       29	       20	         93
-- Wright           378	       24	        0	         94
-- Masonite        2013	       38	        6	         97
-- Ho-Chunk Rewards 404	        5	        1	         98

-- Baird MOD mismatch examples
SELECT CoreRoleName FROM DWH_Stage_CurrentStateLayer..CS_Client WHERE Name = 'Baird MOD' -- CoreRoleName: Baird MOD
SELECT TOP 10 a.*, u.RoleNames FROM #Test_CS_AGG_UserActivity a LEFT JOIN DWH_Stage_CurrentStateLayer..CS_User u ON u.ContactId = a.ContactId WHERE a.Name = 'Baird MOD' AND a.Classified = 'new only'
-- all is ok, 'Baird MOD' role is here so user should be in aggregate
SELECT TOP 10 a.*, u.RoleNames FROM #Test_CS_AGG_UserActivity a LEFT JOIN DWH_Stage_CurrentStateLayer..CS_User u ON u.ContactId = a.ContactId WHERE a.Name = 'Baird MOD' AND a.Classified = 'old only'
-- all is ok, 'Baird MOD' role is NOT here so user should NOT be in aggregate