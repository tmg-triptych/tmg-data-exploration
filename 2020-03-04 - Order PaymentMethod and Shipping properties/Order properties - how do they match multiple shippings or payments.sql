-- Orders with different shippings
SELECT TOP 20 * FROM (
SELECT 
  oh.EpiOrderId, oh.ShippingMethod, oss.EpiLineItemId, oss.Shipmentid, ss.ShippingMethodName, COUNT(ss.ShippingMethodName) OVER (PARTITION BY oh.EpiOrderId) cnt
FROM dbMarekEF_production..OrderHistories oh
LEFT JOIN dbMarekEF_production..OrderShipments oss ON oh.EpiOrderId = oss.EpiOrderId 
LEFT JOIN dbMarekEF_production..Shipments ss ON oss.Shipmentid = ss.id
) r WHERE cnt > 1
;

SELECT * FROM dbMarekEF_production..OrderShipments ;

-- Orders with different payment methods
SELECT EpiOrderId, PaymentMethod FROM dbMarekEF_production..OrderHistories oh;
SELECT TOP 10 * FROM dbMarekCommerceManager_production..OrderFormPayment;

SELECT TOP 20 * FROM (
SELECT 
  oh.EpiOrderId, oh.PaymentMethod, ofp.PaymentMethodName, COUNT(ofp.PaymentMethodName) OVER (PARTITION BY oh.EpiOrderId) cnt
FROM dbMarekEF_production..OrderHistories oh
LEFT JOIN dbMarekCommerceManager_production..OrderFormPayment ofp ON ofp.OrderGroupId = oh.EpiOrderId
) r WHERE cnt > 1
;


