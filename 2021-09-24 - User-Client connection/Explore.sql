USE DWH_Stage_CurrentStateLayer;
-- USE DWH_CurrentStateLayer;
DROP TABLE IF EXISTS #UserClient_By_RoleNames
SELECT DISTINCT
  u.UserId,
	u.ContactId,
	u.Email,
	r.RoleName,
	c.Name AS ClientName
INTO #UserClient_By_RoleNames
FROM CS_User u
JOIN CS_UserRoles ur ON ur.UserId = u.UserId
JOIN CS_Roles r ON ur.RoleId = r.RoleId
JOIN CS_Client c ON r.RoleName = c.RoleName
-- 162 532 row(s) affected



DROP TABLE IF EXISTS #UserClient_By_RoleSiteID
SELECT DISTINCT
  u.UserId,
	u.ContactId,
	u.Email,
	c.SiteId,
	c.Name AS ClientName
INTO #UserClient_By_RoleSiteID
FROM CS_User u
JOIN CS_UserRoles ur ON ur.UserId = u.UserId
JOIN CS_Roles r ON ur.RoleId = r.RoleId 
JOIN CS_SiteRoles sr ON ur.RoleId = sr.RoleId
JOIN CS_Client c ON sr.SiteId = c.SiteId
-- 169 046 row(s) affected

DROP TABLE IF EXISTS #UserClient_Merged
SELECT
  COALESCE(uc_siteid.UserId, uc_rolename.UserId) AS UserId,
	COALESCE(uc_siteid.ContactId, uc_rolename.ContactId) AS ContactId,
	COALESCE(uc_siteid.Email, uc_rolename.Email) AS Email,
	COALESCE(uc_siteid.ClientName, uc_rolename.ClientName) AS ClientName,
	uc_rolename.RoleName,
	uc_siteid.SiteId,
	CASE
	  WHEN uc_siteid.ClientName IS NOT NULL AND uc_rolename.ClientName IS NOT NULL THEN 'by both'
		WHEN uc_siteid.ClientName IS NOT NULL AND uc_rolename.ClientName IS NULL THEN 'by siteid only'
		WHEN uc_siteid.ClientName IS NULL AND uc_rolename.ClientName IS NOT NULL THEN 'by rolename only'
	END AS Class
INTO #UserClient_Merged
FROM #UserClient_By_RoleSiteID uc_siteid
FULL OUTER JOIN #UserClient_By_RoleNames uc_rolename ON
  ((uc_siteid.UserId IS NULL AND uc_rolename.UserId IS NULL) OR uc_siteid.UserId = uc_rolename.UserId) AND
	((uc_siteid.ContactId IS NULL AND uc_rolename.ContactId IS NULL) OR uc_siteid.ContactId = uc_rolename.ContactId) AND
	((uc_siteid.Email IS NULL AND uc_rolename.Email IS NULL) OR uc_siteid.Email = uc_rolename.Email) AND
	uc_siteid.ClientName = uc_rolename.ClientName

SELECT Class, COUNT(*) UserClientPairs FROM #UserClient_Merged GROUP BY Class;
-- by siteid only  6 514
-- by both	     162 532

-- By Clients
SELECT
  ClientName,
	COUNT(CASE WHEN Class = 'by both' THEN 1 END) ByBoth,
	COUNT(CASE WHEN Class = 'by siteid only' THEN 1 END) BySiteidOnly
FROM #UserClient_Merged
GROUP BY ClientName ORDER BY ClientName;

SELECT * FROM #UserClient_Merged WHERE Class = 'by siteid only' AND ClientName = 'MGIC';


-- Localize problems
SELECT
  ur.UserId, r.RoleName
FROM CS_UserRoles ur 
JOIN CS_Roles r ON ur.RoleId = r.RoleId
JOIN CS_SiteRoles sr ON ur.RoleId = sr.RoleId
WHERE
  ur.UserId IN (
    SELECT UserId FROM #UserClient_Merged WHERE Class = 'by siteid only' AND ClientName = 'Caleffi'
	) AND sr.SiteId = 73154
ORDER BY ur.UserId


