-- GA data in DWH
USE DWH_CurrentStateLayer
SELECT TrackingId, Account, WebProperty, COUNT(*), MIN(ga_date), MAX(ga_date) FROM GA_Traffic_By_Content_Daily WHERE TrackingId IN ('UA-122470360-25') GROUP BY TrackingId, Account, WebProperty ORDER BY TrackingId DESC
SELECT TrackingId, Account, WebProperty, COUNT(*), MIN(ga_date), MAX(ga_date) FROM GA_Products WHERE TrackingId IN ('UA-122470360-25') GROUP BY TrackingId, Account, WebProperty ORDER BY TrackingId DESC
SELECT TrackingId, Account, WebProperty, COUNT(*), MIN(ga_date), MAX(ga_date) FROM GA_Events WHERE TrackingId IN ('UA-122470360-25') GROUP BY TrackingId, Account, WebProperty ORDER BY TrackingId DESC
--
SELECT TrackingId, Account, WebProperty, COUNT(*), MIN(ga_date), MAX(ga_date) FROM DWH_CurrentStateLayer..GA_Products WHERE ga_date >= '2020-12-07' AND TrackingId IN ('UA-122470360-28') GROUP BY TrackingId, Account, WebProperty ORDER BY TrackingId DESC
SELECT TrackingId, Account, WebProperty, COUNT(*), MIN(ga_date), MAX(ga_date) FROM DWH_CurrentStateLayer..GA_Events WHERE ga_date >= '2020-12-07' AND TrackingId IN ('UA-122470360-28') GROUP BY TrackingId, Account, WebProperty ORDER BY TrackingId DESC
-- GA data in client views
SELECT TrackingId, Account, WebProperty, COUNT(*), MIN(ga_date), MAX(ga_date) FROM DWH_Client_ProfileHealth..GA_Traffic_By_Content_Daily GROUP BY TrackingId, Account, WebProperty ORDER BY TrackingId DESC
SELECT TrackingId, Account, WebProperty, COUNT(*), MIN(ga_date), MAX(ga_date) FROM DWH_Client_ProfileHealth..GA_Events GROUP BY TrackingId, Account, WebProperty ORDER BY TrackingId DESC
SELECT TrackingId, Account, WebProperty, COUNT(*), MIN(ga_date), MAX(ga_date) FROM DWH_Client_ProfileHealth..GA_Products GROUP BY TrackingId, Account, WebProperty ORDER BY TrackingId DESC
--
SELECT TrackingId, Account, WebProperty, COUNT(*), MIN(ga_date), MAX(ga_date) FROM DWH_Client_MGIC..GA_Events GROUP BY TrackingId, Account, WebProperty ORDER BY TrackingId DESC
SELECT TrackingId, Account, WebProperty, COUNT(*), MIN(ga_date), MAX(ga_date) FROM DWH_Client_MGIC..GA_Products GROUP BY TrackingId, Account, WebProperty ORDER BY TrackingId DESC