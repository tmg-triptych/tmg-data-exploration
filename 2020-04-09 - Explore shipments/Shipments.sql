EXECUTE [_Test_CurrentStateLayer]

USE DWH_Stage_CurrentStateLayer;

---------------------------------------
-- CASE #1: NULLs in CS_User.ContactId
---------------------------------------
-- Count them:
SELECT COUNT(*) FROM CS_User WHERE ContactId IS NULL;
-- 26 

-- Look at them:
SELECT * FROM CS_User WHERE ContactId IS NULL;
-- All are excluded: ExcludeFromReports = 1 and Exclude_NoContact = 1

-- Discover CS_User source code:
SELECT
  am.Email,
  am.CreateDate,
  am.LastLoginDate,
  am.UserId,
  cc.ContactId
FROM dbMarekCommerceManager_production..aspnet_Membership am
LEFT JOIN dbMarekCommerceManager_production..cls_Contact cc WITH(nolock) ON am.Email = cc.Email
WHERE cc.ContactId IS NULL
ORDER BY am.LastLoginDate DESC

SELECT * FROM dbMarekCommerceManager_production..cls_Contact WHERE Email LIKE '%@masonite.com' AND Email LIKE '%jglover%'

---------------------------------------
-- CASE #2: NULLs in CS_User.UserId
---------------------------------------
-- Count them:
SELECT COUNT(*) FROM CS_User WHERE UserId IS NULL;
-- 11

-- Look at them:
SELECT * FROM CS_User WHERE UserId IS NULL;
-- All are excluded: ExcludeFromReports = 1 and Exclude_NoContact = 1

-- Discover CS_User source code:
SELECT
  am.Email,
  am.CreateDate,
  am.LastLoginDate,
  up.UserId
FROM dbMarekCommerceManager_production..aspnet_Membership am
LEFT JOIN dbMarekEF_production..UserProfile up ON up.UserId = am.UserId
WHERE up.UserId IS NULL
ORDER BY am.LastLoginDate DESC

---------------------------------------
-- CASE #3: Duplicate UserId's
---------------------------------------
-- Look at them:
SELECT UserId FROM dbo.CS_User WHERE UserId IS NOT NULL GROUP BY UserId HAVING COUNT(*) > 1;
-- 23

SELECT DISTINCT UserId FROM dbo.CS_User WHERE UserId IN (SELECT UserId FROM dbo.CS_User WHERE UserId IS NOT NULL GROUP BY UserId HAVING COUNT(*) > 1);

SELECT
  am.Email, cc.*
FROM dbMarekCommerceManager_production..aspnet_Membership am
LEFT JOIN dbMarekCommerceManager_production..cls_Contact cc WITH(nolock) 
	ON am.Email = cc.Email
WHERE am.Email IN (
  'admin@yourcompany.com',
  'agency++658557@fba-mi.com',
  'agi++567294@arcadegroup.com',
  'alena.litavor@marekgroup.com',
  'alexander.firsov@firstlinesoftware.com',
  'bryan@web2printexperts.com',
  'chris++704380@akers.insure',
  'gutowskiagency++706910@gmail.com',
  'ilya.lifman@firstlinesoftware.com',
  'isadore.baseman++50096@emersonreid.com',
  'janet++51134@mjmarketing.org',
  'jay.thomas@marekgroup.com',
  'jdraper++522347@qcinow.com',
  'katrina++638920@asgbroker.com',
  'manager@unknowdomain.domain',
  'mstarr++51891@thestarragency.com',
  'receiving@unknowdomain.domain',
  'shipping@unknowdomain.domain',
  'sonya++666315@thesunatlantic.com',
  'supervisor@unknowdomain.domain',
  'tami.marek@marekgroup.com',
  'tara.beyer@marekgroup.com',
  'terrysnowabc++617193@gmail.com'
)
ORDER BY am.Email DESC

---------------------------------------
-- CASE #5: NULL PaymentId's
---------------------------------------
-- Look at them:
SELECT COUNT(*) from dbo.CS_Payment WHERE PaymentId IS NULL