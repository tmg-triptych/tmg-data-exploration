USE ExploreData -- FOR TESTING

-----------------------------------------------------------------------------------
-- PART 1                                                                        --
-- Build table with new & old AllowAccess property values. Check what's changed. --
-----------------------------------------------------------------------------------
-- Step 1. Build actual site roles table
DROP TABLE IF EXISTS #UHC_SiteRoles;
SELECT
  name AS SiteName, 
	SiteID,
	value AS RoleName
INTO #UHC_SiteRoles
FROM (
  SELECT
		sd.name,
		sd.startpage AS SiteID,
		cp.LongString AS AllUsers
	from dbMarekCMS_production..tblSiteDefinition sd with(nolock)
	LEFT JOIN dbMarekCMS_production..tblContentProperty cp with(nolock) on sd.StartPage = cp.fkContentId and cp.fkPropertyDefinitionID = 924  --- all roles that have access to a site\
	where sd.StartPage = 8094 -- UHC Site id hardcoded here
) r
CROSS APPLY STRING_SPLIT(AllUsers, ','); 
;

-- TEST
-- SELECT TOP 10 * FROM #UHC_SiteRoles;
-- SELECT * FROM #UHC_SiteRoles ORDER BY RoleName;

-- Step 2. Build table with Product Version x Role in AllowAccess . Mark roles activeness
DROP TABLE IF EXISTS #UHC_VersionAllowAccessRole;
SELECT 
  t.pkId,
	t.ObjectId,
	t.AllowAccessRole,
	CASE WHEN r.RoleName IS NOT NULL THEN 1 ELSE 0 END AS IsActive
INTO #UHC_VersionAllowAccessRole
FROM (
	SELECT
		pkId,
		ObjectId,
		value AS AllowAccessRole
	FROM (
		SELECT
			ccp.pkId,
			ccp.ObjectId,
			ccp.LongString
		FROM dbMarekCommerceManager_production..CatalogContentProperty ccp
		LEFT JOIN dbMarekCommerceManager_production..CatalogEntry ce WITH(NOLOCK) ON ce.CatalogEntryId = ccp.ObjectId
		WHERE
			MetaFieldName = 'Allowaccess'
			AND CatalogId = 56 -- UHC Catalog id hardcoded here
	) t
	CROSS APPLY STRING_SPLIT(LongString, ',')
) t
LEFT JOIN #UHC_SiteRoles r ON r.RoleName = t.AllowAccessRole
;

-- TEST
-- SELECT TOP 10 * FROM #UHC_VersionAllowAccessRole;

-- Step 3. Build informational table for AllowAccess property:
--  - original value
--  - value cleaned from inactive roles
--  - change marker
DROP TABLE IF EXISTS #UHC_CCPAllowAccessOldAndNewValues;
SELECT
	tbefore.pkId,
	tbefore.ObjectId,
	tbefore.LongString AS LongString_before,
	tafter.LongString AS LongString_after,
	CASE WHEN LEN(tbefore.LongString) = LEN(tafter.LongString) THEN 0 ELSE 1 END AS IsChanged
INTO #UHC_CCPAllowAccessOldAndNewValues
FROM (
	SELECT
		ccp.pkId,
		ccp.ObjectId,
		ccp.LongString AS LongString
	FROM dbMarekCommerceManager_production..CatalogContentProperty ccp
	LEFT JOIN dbMarekCommerceManager_production..CatalogEntry ce WITH(NOLOCK) ON ce.CatalogEntryId = ccp.ObjectId
	WHERE
		ccp.MetaFieldName = 'Allowaccess'
		AND ce.CatalogId = 56 -- UHC Catalog id hardcoded here
) tbefore
LEFT JOIN (
  SELECT
	  pkId,
		ObjectId,
		STRING_AGG(AllowAccessRole, ',') AS LongString
	FROM #UHC_VersionAllowAccessRole
	WHERE IsActive = 1
	GROUP BY pkId, ObjectId
) tafter ON 
  tbefore.pkId = tafter.pkId
	AND tbefore.ObjectId = tafter.ObjectId
;

-- TEST
-- SELECT TOP 10 * FROM #UHC_CCPAllowAccessOldAndNewValues;
 SELECT * FROM #UHC_CCPAllowAccessOldAndNewValues WHERE objectid = '79833';
-- SELECT IsChanged, COUNT(*) FROM #UHC_CCPAllowAccessOldAndNewValues GROUP BY IsChanged;
-- 0	2600
-- 1	1937

-----------------------------------------
-- PART 2                              --
-- Update production data with values. --
-----------------------------------------
-- Step 1. Make a backup
DROP TABLE IF EXISTS CatalogContentProperty_backup;
SELECT
  *
INTO CatalogContentProperty_backup
FROM dbMarekCommerceManager_production..CatalogContentProperty
;

-- Step 2. Update with new values



UPDATE t1
SET t1.LongString = t2.LongString_after
-- FROM CatalogContentProperty_backup t1                       -- FOR TESTING
FROM dbMarekCommerceManager_production..CatalogContentProperty -- FRO ACTUAL RUN
INNER JOIN #UHC_CCPAllowAccessOldAndNewValues t2 ON
  t1.pkId = t2.pkId AND
	t1.ObjectId = t2.ObjectId 
WHERE 
  t1.MetaFieldName = 'Allowaccess'
;
-- (4537 row(s) affected)

-- TEST
-- SELECT TOP 5 * FROM CatalogContentProperty_backup
-- SELECT COUNT(*) FROM CatalogContentProperty_backup                          -- 1 279 098
-- SELECT * FROM CatalogContentProperty_backup WHERE objectid = '79833' AND MetaFieldName = 'Allowaccess'

-- TEST
-- SELECT TOP 5 * FROM dbMarekCommerceManager_production..CatalogContentProperty
-- SELECT COUNT(*) FROM dbMarekCommerceManager_production..CatalogContentProperty                           -- 1 279 098
-- SELECT * FROM dbMarekCommerceManager_production..CatalogContentProperty WHERE objectid = '79833' AND MetaFieldName = 'Allowaccess'