-- aspnet_Membership and cls_Contact
SELECT
    -- for examples
	am_Email, CreateDate, LastLoginDate, UserId, cc_Email, ContactId, problem
	-- for stats
	--problem, COUNT(*)
FROM (
	SELECT
		CASE
			WHEN am_cardinality > 1 THEN 'Multiple cls_Contact recs match same aspnet_Membership record'
			WHEN cc_cardinality > 1 THEN 'Multiple aspnet_Membership recs match same cls_Contact record'
			WHEN am_Email IS NULL THEN 'cls_Contact record with no matching aspnet_Membership record'
			WHEN cc_Email IS NULL THEN 'aspnet_Membership record with no matching cls_Contact record'
			WHEN am_cardinality = 1 AND cc_cardinality = 1 AND am_Email IS NOT NULL AND cc_Email IS NOT NULL THEN 'all ok'
		END AS problem,
	  *
	FROM (
		SELECT
			am.Email AS am_Email,
			am.CreateDate,
			am.LastLoginDate,
			am.UserId,
			CASE WHEN am.Email IS NOT NULL THEN COUNT(*) OVER (PARTITION BY am.Email) END AS am_cardinality,
			cc.Email AS cc_Email,  
			cc.ContactId,
			CASE WHEN cc.Email IS NOT NULL THEN COUNT(*) OVER (PARTITION BY cc.Email) END AS cc_cardinality
		FROM dbMarekCommerceManager_production..aspnet_Membership am
		LEFT JOIN dbMarekCommerceManager_production..cls_Contact cc WITH(nolock) ON am.Email = cc.Email
	) r
) r
-- for examples
WHERE problem != 'all ok'
ORDER BY problem
-- for stats
--GROUP BY problem
--ORDER BY problem
;


-- aspnet_Membership and UserProfile 
SELECT
	-- for examples
	Email, CreateDate, LastLoginDate, am_UserId, cc_UserId, problem
	-- for stats
	--problem, COUNT(*)
FROM (
	SELECT
		CASE
			WHEN am_cardinality > 1 THEN 'Multiple UserProfile recs match same aspnet_Membership record'
			WHEN up_cardinality > 1 THEN 'Multiple aspnet_Membership recs match same UserProfile record'
			WHEN am_UserId IS NULL THEN 'UserProfile record with no matching aspnet_Membership record'
			WHEN cc_UserId IS NULL THEN 'aspnet_Membership record with no matching UserProfile record'
			WHEN am_cardinality = 1 AND up_cardinality = 1 AND am_UserId IS NOT NULL AND cc_UserId IS NOT NULL THEN 'all ok'
		END AS problem,
	  *
	FROM (
		SELECT
			am.Email,
			am.CreateDate,
			am.LastLoginDate,
			am.UserId AS am_UserId,
			CASE WHEN am.UserId IS NOT NULL THEN COUNT(*) OVER (PARTITION BY am.Email) END AS am_cardinality,
			up.UserId AS cc_UserId,
			CASE WHEN up.UserId IS NOT NULL THEN COUNT(*) OVER (PARTITION BY up.UserId) END AS up_cardinality
		FROM dbMarekCommerceManager_production..aspnet_Membership am
		LEFT JOIN dbMarekEF_production..UserProfile up WITH(nolock) ON up.UserId = am.UserId
	) r
) r
-- for examples
WHERE problem != 'all ok'
ORDER BY problem
-- for stats
--GROUP BY problem
--ORDER BY problem
;