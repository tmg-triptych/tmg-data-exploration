--User without cls_Contact record
SELECT CreateDate, * FROM dbMarekCommerceManager_production..aspnet_Membership am WHERE am.UserId = 'F077012A-493B-4F1D-AB11-34C14D4C86CE'
SELECT * FROM dbMarekEF_production..UserProfile up WHERE up.UserId = 'F077012A-493B-4F1D-AB11-34C14D4C86CE'
SELECT * FROM dbMarekCommerceManager_production..cls_Contact cc WHERE cc.Email = 'ted.ruch@nm.com' -- no record here

--User without UserProfile and cls_Contact records
SELECT CreateDate, * FROM dbMarekCommerceManager_production..aspnet_Membership am WHERE am.UserId = '2C351C31-117B-415D-B36A-522A82BA1AD2'
SELECT * FROM dbMarekEF_production..UserProfile up WHERE up.UserId = '2C351C31-117B-415D-B36A-522A82BA1AD2' -- no record here
SELECT * FROM dbMarekCommerceManager_production..cls_Contact cc WHERE cc.Email IN ('amy.jones@miginsgroup.com', 'amy.jones++734823@miginsgroup.com') -- no record here

--User without UserProfile and cls_Contact records
SELECT CreateDate, * FROM dbMarekCommerceManager_production..aspnet_Membership am WHERE am.UserId = '6E5C29A2-E1D8-4FCE-BAC9-DEF6B0A212EE'
SELECT * FROM dbMarekEF_production..UserProfile up WHERE up.UserId = '6E5C29A2-E1D8-4FCE-BAC9-DEF6B0A212EE' -- no record here
SELECT * FROM dbMarekCommerceManager_production..cls_Contact cc WHERE cc.Email IN ('ginacruzjamison@gmail.com', 'ginacruzjamison++69720@gmail.com') -- no record here

