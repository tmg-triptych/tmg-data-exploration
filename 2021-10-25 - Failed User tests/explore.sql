-- Users wo ContactId
SELECT
  *
FROM DWH_CurrentStateLayer..CS_User
WHERE
  ContactId IS NULL AND
  -- Mute known problems
  UserId NOT IN (      
  '659AF2AF-0EC2-4A6D-82A0-6A7AA6C4CAF5',
  '169B4748-ED75-4102-996E-3768488DF6F7',
  '85448548-5335-46DD-B279-9D29B223673C',
  '81151B9B-B54C-44A6-86A3-D77C98892F7D',
  '8B3FEC69-4A50-4DE0-BF7F-0A759A6315A2',
  'A99B0B0F-76F7-4C28-B5A4-BA77D859355A',
  '7255B982-BA93-40C1-BF1E-615873FC1278',
  '3DDF45F6-89C3-44A0-A005-6FC8BF3D21CC',
  '027A7C09-CC0A-4324-A644-994667C8528C',
  '39A0E133-F944-489D-9A17-1F365E29EFD9',
  '5165892E-1A63-4CBA-88A1-FD50D307D744',
  '11D1BEB4-87E0-48C5-AA02-0460F4D3E84E',
  '4E0988B2-D73E-4CE4-ACD1-7333235D381C',
  'A983B097-8B8F-4953-9F5F-2E2DECA30017',
  '5948D90C-1113-4665-9555-CEB1AA19084A',
  '85D31727-04F7-497C-A5AA-F2212217C2ED',
  '15758BB9-D442-4D38-ADE3-0468E8C5FB0F',
  'C6895F3D-CEC2-44EA-8F8E-20D4FC1D45F3',
  '85468F6C-DAF9-428F-808F-438EF84DE97B',
  '45B56473-BA56-4BF6-A142-0C925C0AA123',
  'CFA98192-1E40-4D56-BBE7-8062EF82925A',
  '99BD9854-ACBF-464D-A523-02F4454EC58D',
  '4EE124A0-7FF9-4929-AA55-34A923DF15B3',
  '34ADCD9B-B0AC-4BAB-A90D-740ECD91CEFD',
  'AC0D9EC1-5EF4-488D-8FD8-7DA3C7541FCB',
  '1E831660-2615-4000-BA0B-55198D9B2327',
  '958A42DC-0BA2-49BE-9083-D77A69C49614',
  '5AED915B-BC01-4595-A9C8-EC76DAEED0CF',
  'C3637C87-3E9E-48AA-8390-3A6F8304AA4F',
  '9C13937A-18DB-4791-A77F-A7578AD9C2F6',
  'F38E4020-FB28-4C24-8BF1-664E5BF2995E',
	'F077012A-493B-4F1D-AB11-34C14D4C86CE'
)

-- ERROR: among 237720 CS_User records, there are 1 records with NULL value in UserId
SELECT
  *
FROM DWH_CurrentStateLayer..CS_User
WHERE
  UserId IS NULL AND
	NOT (FullName = 'Anonymous') AND 
  UserName NOT IN (
    'dyanwindsor++518965@aol.com',
    'pealdevelopment@gmail.com',
    'lg++694037@goodwinfinancial.com',
    'ike++617057@imferguson.net',
    'carleyferris++693972@gmail.com',
    'curtis++545614@insuranceoffice.me',
    'cbell++610411@aris-secure.com',
    'kpruitt++681866@americanseniorbenefits.com',
    'benjamin.olsasky++624020@americare.com',
    'salusstrategies++565536@gmail.com',
    'richardthurz++575778@aol.com',
		'amy.jones++734823@miginsgroup.com'
  )
	-- ginacruzjamison@gmail.com

-- How CS_User table is made
SELECT
    am.Email AS am_Email,
		am.UserId AS am_UserId,
		up.UserId AS up_UserId,
		up.Email AS up_Email,
		cc.ContactId AS cc_ContactId
  FROM dbMarekCommerceManager_production..aspnet_Membership am WITH(NOLOCK)
  LEFT JOIN dbMarekEF_production..UserProfile up WITH(NOLOCK)
    ON up.UserId = am.UserId
  LEFT JOIN dbMarekCommerceManager_production..cls_Contact cc WITH(NOLOCK)
    ON am.Email = cc.Email 
	WHERE am.Email = 'ginacruzjamison++69720@gmail.com'

