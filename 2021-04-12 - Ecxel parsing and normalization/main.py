import openpyxl
import re

COLUMN_LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'


def read_tables_from_config(wb_, print_debug=False):
    if print_debug:
        print('Read tables')
    result = []
    last_table = None
    sheet_names = wb_.sheetnames
    if len(sheet_names):
        sheet = wb_[sheet_names[0]]  # hardcode: always use 1st sheet
        row = sheet['2']             # hardcode: tables described in this row
        for cell in row:
            #print(cell, cell.coordinate, cell.value)
            if cell.value:                           # first cell in merged range contains value
                result.append({
                    'name': cell.value,
                    'source_cell': cell.coordinate,
                    'start_column': cell.column_letter,
                    'end_column': None,
                    'versions': []
                })
                if last_table is None:
                    last_table = 0
                else:
                    last_table += 1
            else:                                     # other cells need to be processed to remember end of merged range
                if last_table is not None:
                    result[last_table]['end_column'] = COLUMN_LETTERS[cell.column-1]
    return result


def read_versions_and_files_from_config(config_, wb_, print_debug=False):
    if print_debug:
        print('Read versions and files')
    result = config_
    sheet_names = wb_.sheetnames
    if len(sheet_names):
        sheet = wb_[sheet_names[0]]  # hardcode: always use 1st sheet
        for table_index, table in enumerate(config_):
            if print_debug:
                print('..read for table: ' + table['name'])
            for column in COLUMN_LETTERS[
                          COLUMN_LETTERS.index(table['start_column']):COLUMN_LETTERS.index(table['end_column']) + 1
                          ]:
                if print_debug:
                    print('...read column: ' + column)
                result[table_index]['versions'] .append(
                    {
                        'name': sheet[column + '3'].value,       # hardcode: versions always in row 3
                        'source_cell': sheet[column + '3'].coordinate,
                        'column': column,
                        'file': sheet[column + '4'].value,       # hardcode: files always in row 4
                        'states': []
                    }
                )
    return result


def read_states_from_config(config_, wb_, print_debug=False):
    if print_debug:
        print('Read states and sheets')
    result = config_
    sheet_names = wb_.sheetnames
    if len(sheet_names):
        sheet = wb_[sheet_names[0]]  # hardcode: always use 1st sheet
        for table_index, table in enumerate(config_):
            if print_debug:
                print('..read for table: ' + table['name'])
            for version_index, version in enumerate(table['versions']):
                if print_debug:
                    print('..read for version: ' + version['name'] + ' ' + version['file'] + ' ' + version['column'])
                col = sheet['A']     # hardcode: states in column A
                states_list_started = False
                for state_cell in col:
                    if states_list_started:
                        source_cell = version['column'] + str(state_cell.row)
                        sheets = sheet[source_cell].value
                        if sheets:
                            sheets = sheets.split('\n')
                        else:
                            sheets = []
                        result[table_index]['versions'][version_index]['states'].append(
                            {
                                'name':  state_cell.value,
                                'sheets': sheets,
                                'source_cell': source_cell
                            }
                        )
                    if state_cell.value == 'State / File': # hatdcode: states col header
                        states_list_started = True

    return config_


def read_config(filename, print_debug=False):
    if print_debug:
        print("=====READ CONFIG====")
    wb = openpyxl.load_workbook(filename=filename, data_only=False)
    config_ = read_tables_from_config(wb, print_debug)
    config_ = read_versions_and_files_from_config(config_, wb, print_debug)
    config_ = read_states_from_config(config_, wb, print_debug)
    if print_debug:
        print()
        print("=====PRINT CONFIG====")
        debug_print_config(config_)
    return config_


def read_di_config(filename, print_debug=False):
    if print_debug:
        print("=====READ CONFIG====")
    wb_ = openpyxl.load_workbook(filename=filename, data_only=False)
    di_config_ = {}
    sheet_names = wb_.sheetnames
    if len(sheet_names):
        sheet = wb_[sheet_names[0]]  # hardcode: always use 1st sheet
        for row in sheet:
            if (row[0].value != 'State') and row[2].value is not None:  # skip header & empty rows
                state = row[0].value
                file = str(row[2].value) + '.xlsx'
                di_config_[state] = file

    if print_debug:
        print()
        print("=====PRINT CONFIG====")
        print(di_config_)
    return di_config_


def debug_print_config(config_):
    for table in config_:
        print('Table: "' + table['name'] + '" (' + table['start_column'] + '-' + table['end_column'] + ')')
        for version in table['versions']:
            print('..version "' + version['name'] + '" from file "' + version['file'] + '" ' + version['column'])
            for state in version['states']:
                print('....state "' + state['name'] + '" from sheets: ' + '; '.join(state['sheets']) )


def text_to_col_name(text):
    result_ = text
    result_ = re.sub('[\W]+', '', text)
    return result_


def nonable_to_str(text):
    if text:
        return str(text)
    else:
        return ''


def read_accident_table(sheet_, state_, version_, print_debug=False):
    result_ = []
    if sheet_['C11'].value == 'Plan':  # check assumption, because we will read rows after this one
        possible_prefix = ''
        for row in range(12, sheet_.max_row + 1):
            next_row = None
            c_value = sheet_['C' + str(row)].value  # hardcoded: column & logic for it
            d_value = sheet_['D' + str(row)].value  # hardcoded: column & logic for it
            e_value = sheet_['E' + str(row)].value  # hardcoded: column & logic for it
            f_value = sheet_['F' + str(row)].value  # hardcoded: column & logic for it
            g_value = sheet_['G' + str(row)].value  # hardcoded: column & logic for it

            debug_row_string = '"' + \
                               '", "'.join([str(c_value), str(d_value), str(e_value), str(f_value), str(g_value)]) + '"'

            # understand what's in row - hardcode for specific formatting rules
            is_empty = c_value is None and d_value is None and e_value is None and f_value is None and g_value is None
            is_repeated_header = (c_value is None and d_value is None and e_value == 'Combined Accident Insurance' and
                                  f_value is None and g_value is None) \
                                 or (c_value == 'Plan' and e_value == 'Gold'
                                     and f_value == 'Platinum' and g_value == 'Diamond')
            is_bottom_text = str(c_value).startswith('Applicable to: ')
            is_line1_of_multiline_row = c_value in ('Family Care', 'Outpatient Surgery Facility', 'Sports Package')
            is_line2_of_multiline_row = possible_prefix in (
                'Family Care', 'Outpatient Surgery Facility', 'Sports Package')
            is_usual_row = c_value and e_value and f_value and g_value
            is_group_header = c_value and d_value is None and e_value is None and f_value is None and g_value is None
            is_intended_row = c_value is None and d_value and e_value and f_value and g_value

            # print('row: ' + str(row))
            if is_empty or is_repeated_header or is_bottom_text:
                pass # possible_prefix = ''

            elif is_line1_of_multiline_row:
                if c_value == 'Sports Package':
                    next_row = {'State': state_, 'Version': version_,
                                'Column_Name': text_to_col_name(c_value), 'Gold_value': e_value,
                                'Platinum_value': e_value, 'Diamond_value': e_value}
                else:
                    next_row = {'State': state_, 'Version': version_,
                                'Column_Name': text_to_col_name(c_value), 'Gold_value': e_value,
                                'Platinum_value': f_value, 'Diamond_value': g_value}
                possible_prefix = c_value

            elif is_line2_of_multiline_row:
                next_row = {'State': state_, 'Version': version_,
                            'Column_Name': text_to_col_name(possible_prefix) + '_alt', 'Gold_value': e_value,
                            'Platinum_value': e_value, 'Diamond_value': e_value}
                possible_prefix = ''

            elif is_usual_row:
                if (possible_prefix in ('MonthlyPremiums_', 'SemiMonthlyPremiums_',
                                        'BiWeeklyPremiums_', 'WeeklyPremiums_')):
                    next_row = {'State': state_, 'Version': version_,
                                'Column_Name': possible_prefix + text_to_col_name(c_value), 'Gold_value': e_value,
                                'Platinum_value': f_value,
                                'Diamond_value': g_value}
                else:
                    next_row = {'State': state_, 'Version': version_,
                                'Column_Name': text_to_col_name(c_value), 'Gold_value': e_value, 'Platinum_value': f_value,
                                'Diamond_value': g_value}
                    possible_prefix = text_to_col_name(c_value) + '_'

            elif is_group_header:
                possible_prefix = text_to_col_name(c_value) + '_'

            elif is_intended_row:
                next_row = {'State': state_, 'Version': version_,
                            'Column_Name': possible_prefix + text_to_col_name(d_value), 'Gold_value': e_value,
                            'Platinum_value': f_value, 'Diamond_value': g_value}

            else:
                print('[unknown situation] row: ' + str(row) + ', data: ' + debug_row_string)

            if next_row:
                # print('...save col: "' + str(next_row['Column_Name']) + '"')
                result_.append(next_row)
    return result_


def read_disability_insurance_table(sheet_, state_, sheet_name_, print_debug=False):
    result_ = []
    for column_shift in range(1, sheet_.max_column, 6):
        header = sheet_[5][column_shift + 1].value
        header_color = sheet_[5][column_shift + 1].font.color.rgb
        if header_color == 'FFFF0000':
            print('....shift ' + str(column_shift) + ', header "' + header + '", skip due to red color')
        else:
            print('....shift ' + str(column_shift) + ', header "' + header + '", read')

            # read solumn names
            row_headers_dict = {
                'Coverage Type': 'CoverageType',
                'Benefit Period': 'BenefitPeriod',
                'Injury Elimination Period': 'InjuryEliminationPeriod',
                'Sickness Elimination Period': 'SicknessEliminationPeriod',
                'Guaranteed Issue Amount (GI)': 'GuaranteedIssueAmount',
                'Total Disability Benefit Amounts': 'TotalDisabilityBenefitAmounts',
                'Partial Disability Benefit': 'PartialDisabilityBenefit',
                'Mental/Nervous Disorders and Substance Abuse': 'MentalNervousDisordersandSubstanceAbuse',
                'Waiver of Premium': 'WaiverofPremium',
                'Portability': 'Portability',
                'Integration with Other Sources of Income': 'IntegrationwithOtherSourcesofIncome',
                'Contribution': 'Contribution',
                'Pre-Existing Condition Limitation': 'Pre-ExistingConditionLimitation',
                'Organ Donation': 'OrganDonation',
                'First Day Coverage - Hospitalization': 'FirstDayCoverage-Hospitalization',
                'First Day Coverage - Outpatient Surgery': 'FirstDayCoverage-OutpatientSurgery',
                '18-49': 'Rate_18-49',
                '50-59': 'Rate_50-59',
                '60-69': 'Rate_60-69'
            }
            rows_to_skip = [
                '',
                'Class 1 Rates',
                'Class 2 Rates',
                'Class 3 Rates',
                'Class 4 Rates',
                'Applicable to:',
                'Employee Disability Income Benefits',
                'Additional Benefits',
                'Issue Age',
                'Rates are monthly per $100 of monthly benefit',
                'Issue Age Rates',
                'Waives the elimination period if an employee has an outpatient surgical procedure due to a covered Sickness or Injury.',
                'Waives the elimination period if an employees is hospitalized due to the disability.',
                'Disabilities due to an organ donation are covered as a sickness and the elimination period is waived.'
            ]
            for row in range(8, sheet_.max_row + 1):
                row_header = nonable_to_str(sheet_[row][column_shift].value).strip()
                if row_header in rows_to_skip:
                    pass
                elif row_header in row_headers_dict:
                    col_name = row_headers_dict[row_header]
                    g_value = nonable_to_str(sheet_[row][column_shift + 1].value).strip()
                    p_value = nonable_to_str(sheet_[row][column_shift + 2].value).strip()
                    d_value = nonable_to_str(sheet_[row][column_shift + 3].value).strip()
                    if col_name == 'GuaranteedIssueAmount':  # hardcode: value splitted to next 2 rows
                        g_value += nonable_to_str(sheet_[row + 1][column_shift + 1].value).strip() \
                                  + nonable_to_str(sheet_[row + 2][column_shift + 1].value).strip()
                        p_value += nonable_to_str(sheet_[row + 1][column_shift + 2].value).strip() \
                                   + nonable_to_str(sheet_[row + 2][column_shift + 2].value).strip()
                        d_value += nonable_to_str(sheet_[row + 1][column_shift + 3].value).strip() \
                                   + nonable_to_str(sheet_[row + 2][column_shift + 3].value).strip()
                    elif col_name in (['Rate_18-49', 'Rate_50-59', 'Rate_60-69']):
                        g_value = g_value
                        p_value = p_value
                        d_value = d_value

                    class_name = sheet_name_[1]
                    benefit_duration = str(sheet_[11][column_shift + 3].value).split(' ')[0] + '/' \
                                        + str(sheet_[12][column_shift + 3].value).split(' ')[0] + ' ' \
                                        + str(sheet_[10][column_shift + 3].value).split(' ')[0] + 'MO'

                    next_row = {'State': state_, 'BenefitDuration': benefit_duration, 'Class': class_name,
                                'Column_Name': col_name, 'Gold_value': g_value,
                                'Platinum_value': p_value, 'Diamond_value': d_value}

                    result_.append(next_row)
                else:
                    print('......UNKNOWN ROW: "' + row_header + '"')
    return result_


def read_critical_illness_rates_table(sheet_, state_, version_, print_debug=False):
    result_ = []
    #start_cell = 12 # 39 65  # first row hardcoded
    #prefix = '10KF' # 20KF  30KF
    groups = {
        '10KF': 12,
        '20KF': 38,
        '30KF': 64
    }
    gold_columns = ['B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
    platinum_columns = ['M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U']
    diamond_columns = ['X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF']

    for group in groups:
        start_cell = groups[group]
        prefix = group
        #print(prefix, start_cell)
        for row_shift in [3, 4, 5, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]:
            # get row prefix
            row_prefix = sheet_[gold_columns[0] + str(start_cell + row_shift)].value

            for column_index, gold_column_letter in enumerate(gold_columns[1:]):
                # get column prefixes
                col_prefix = prefix + '_' + str(sheet_[gold_column_letter + str(start_cell + 7)].value) +\
                             str(sheet_[gold_column_letter + str(start_cell + 8)].value)
                #print(col_prefix, row_prefix)
                column_name = col_prefix + '_' + row_prefix
                gold_value = nonable_to_str(sheet_[gold_column_letter + str(start_cell + row_shift)].value)
                plat_value = nonable_to_str(sheet_[platinum_columns[column_index + 1] + str(start_cell + row_shift)].value)
                diam_value = nonable_to_str(sheet_[diamond_columns[column_index + 1] + str(start_cell + row_shift)].value)

                next_row = {'State': state_, 'Version': version_, 'Column_Name': column_name,
                            'Gold_value': gold_value,
                            'Platinum_value': plat_value,
                            'Diamond_value': diam_value
                            }

                result_.append(next_row)
    return result_


def read_critical_illness_offering_table(sheet_, state_, version_, print_debug=False):
    result_ = []

    header = ''
    for row in range(9, sheet_.max_row + 1):  # first row to parse hardcoded

        # get column name
        column_name = None
        e_cell = sheet_['E' + str(row)]
        is_header = (e_cell.fill.start_color.index == 'FF366092')
        if is_header:
            header = text_to_col_name(e_cell.value) + '_'
        elif e_cell.value:
            column_name = ci_column_name_to_short(header + text_to_col_name(e_cell.value))

        if column_name:
            next_row = {'State': state_, 'Version': version_, 'Column_Name': column_name,
                        'Gold_value': nonable_to_str(sheet_['F' + str(row)].value),
                        'Platinum_value': nonable_to_str(sheet_['G' + str(row)].value),
                        'Diamond_value': nonable_to_str(sheet_['H' + str(row)].value)
                        }
            result_.append(next_row)

    return result_


def read_data_from_files(config_, print_debug=False):
    accident_table_data_ = []
    critical_illness_table_data_ = []
    disability_income_table_data_ = []

    if print_debug:
        print("=====READ DATA====")
    for table in config_:
        if table['name'] == 'Critical Illness':  # DEBUG ONLY
            if print_debug:
                print('Table: "' + table['name'] + '"')
            for version in table['versions']:
                if print_debug:
                    print('..version "' + version['name'] + '" from file "' + version['file'])
                for state in version['states']:
                    if state['sheets']:
                        if print_debug:
                            print('....state "' + state['name'] + '"')
                        for sheetname in state['sheets']:
                            if print_debug:
                                print('......sheet "' + sheetname + '"')
                                wb = openpyxl.load_workbook(filename='data/' + version['file'], data_only=True)
                                if sheetname in wb.sheetnames:
                                    sheet = wb[sheetname]

                                    if table['name'] == 'Accident':
                                        accident_table_data_.append(
                                            read_accident_table(sheet, state['name'], version['name'], print_debug))
                                        pass

                                    elif table['name'] == 'Critical Illness':
                                        new_statename = state['name']
                                        if new_statename == 'FL' and version['file'] == 'Individual Critical Illness - FL with Waive.xlsx':
                                            new_statename = 'FL-UW'
                                        if sheetname.endswith('Rates') or sheetname.endswith('Rates - Waive UW'):
                                            critical_illness_table_data_.append(read_critical_illness_rates_table(sheet, new_statename, version['name'], print_debug))
                                        if sheetname.endswith('Offering') or sheetname.endswith('Offering-Waive UW'):
                                            critical_illness_table_data_.append(read_critical_illness_offering_table(sheet, new_statename, version['name'], print_debug))

                                elif sheetname in ('If No Underwriting Waiver is selected:',
                                                   'If Underwriting Waiver is selected:'):
                                    pass
                                else:
                                    print('[ERROR] Sheet "' + sheetname + '" missing in file + "' + version['file'] + '"')

                    #break  # DEBUG: stop on first sheet
                    #break
                    #if state['name'] == 'AK':
                        #break  # DEBUG: stop on early state
            #break  # DEBUG: stop on first version & file
        #break  # DEBUG: stop on first table
    return accident_table_data_, critical_illness_table_data_, disability_income_table_data_


def read_data_from_di_files(di_config_, print_debug=False):
    disability_income_table_data_ = []

    if print_debug:
        print("=====READ DATA====")
        print('Table: "Disability Insurance"')

    for statename in di_config_:
        filename = di_config_[statename]
        if print_debug:
            print('..state "' + statename + '" from file "' + filename)
        wb = openpyxl.load_workbook(filename='data/Combined_DI Data Build/' + filename, data_only=True)
        for sheetname in wb.sheetnames:
            if sheetname in (['C1 0%', 'C2 0%', 'C3 0%', 'C4 0%']):
                print(sheetname)
                sheet = wb[sheetname]
                disability_income_table_data_.append(read_disability_insurance_table(sheet, statename, sheetname, print_debug))

        #break  # DEBUG: 1st state only

    return disability_income_table_data_


def get_columns_critical_illness_offering_table(sheet_, known_columns):
    header = ''
    result_ = known_columns.copy()
    for row in range(9, sheet_.max_row + 1):
        e_cell = sheet_['E' + str(row)]
        is_header = (e_cell.fill.start_color.index == 'FF366092')
        if is_header:
            header = text_to_col_name(e_cell.value) + '_'
        elif e_cell.value:
            column = header + text_to_col_name(e_cell.value)
            if column not in result_:
                result_.append(column)

    return result_


def get_ci_columns(config_, print_debug=False):
    col_list = []

    if print_debug:
        print("=====READ UNIQUE COLNAMES====")
    for table in config_:
        if print_debug:
            print('Table: "' + table['name'] + '"')
        if table['name'] == 'Critical Illness':
            for version in table['versions']:
                if print_debug:
                    print('..version "' + version['name'] + '" from file "' + version['file'])
                for state in version['states']:
                    if state['sheets']:
                        if print_debug:
                            print('....state "' + state['name'] + '"')
                        for sheetname in state['sheets']:
                            if print_debug:
                                print('......sheet "' + sheetname + '"')
                                wb = openpyxl.load_workbook(filename='data/'+ version['file'], data_only=True)
                                if sheetname in wb.sheetnames:
                                    sheet = wb[sheetname]
                                    if table['name'] == 'Critical Illness':
                                        if sheetname.endswith('Offering'):
                                            col_list = get_columns_critical_illness_offering_table(sheet, col_list)
                                elif sheetname.startswith('If Underwriting Waiver') or \
                                        sheetname.startswith('If No Underwriting Waiver is selected'):
                                    pass
                                else:
                                    print('[ERROR] Sheet "' + sheetname + '" missing in file + "' + version['file'] + '"')

    for col in col_list:
        print(get_ci_columns(col))
    return 0


def ci_column_name_to_short(name_):
    names_dict = {
        'AdditionalBenefits_ChildhoodConditions': 'AddBen_ChildhoodConditions',
        'AdditionalBenefits_Coveragewhenaconditionrequiressubsequenthospitaladmission':
            'AddBen_CoverageHospitalAdmission',
        'AdditionalBenefits_Disability': 'AddBen_Disability',
        'AdditionalBenefits_EmployeeonlyEEorEmployeeSpouseEESP': 'AddBen_EEorEESP',
        'AdditionalBenefits_HospitalAdmissionBenefit': 'AddBen_HospitalAdmissionBenefit',
        'AdditionalBenefits_MortgageandRentHelper': 'AddBen_MortgageandRentHelper',
        'AdditionalBenefits_Pays100ofthedependentchildfaceamount': 'AddBen_Pays100DependentChild',
        'AdditionalBenefits_Paysanextrabenefiteachmonththeinsuredmisses5ormoredaysofworkupto6months':
            'AddBen_PaysMonthInsuredMissesWork',
        'AdditionalBenefits_ProvidesbenefitsforchildhoodconditionsCerebralPalsyCongenitalBirthDefectsHeartLungCleftLipPalateetcCysticFibrosisDownSyndromeMuscularDystrophyType1Diabetes':
            'AddBen_ProvidesBenefitsChildhoodConditions',
        'AdditionalBenefits_WellnessBenefitPayableonceperinsuredperyear': 'AddBen_WellnessBenefit',
        'AdvocacyPackage_AsktheExpertHotlineprovides24houradvicefromexpertsaboutaparticularmedicalcondition':
            'AdvPack_AskExpertHotline',
        'AdvocacyPackage_BestDoctors': 'AdvPack_BestDoctors',
        'AdvocacyPackage_BestDoctorsCompsych': 'AdvPack_BestDoctorsCompsych',
        'AdvocacyPackage_InDepthMedicalReviewoffersafullreviewofdiagnosisandtreatmentplan':
            'AdvPack_InDepthMedicalReview',
        'AdvocacyPackage_PhysicianReferrals': 'AdvPack_PhysicianReferrals',
        'CriticalIllnessBenefits_AlzheimersDisease': 'CIBen_AlzheimersDisease',
        'CriticalIllnessBenefits_BenefitReductionDuetoAge': 'CIBen_BenefitReductionDuetoAge',
        'CriticalIllnessBenefits_BenignBrainTumor': 'CIBen_BenignBrainTumor',
        'CriticalIllnessBenefits_Cancerexceptskincancer': 'CIBen_Cancerexceptskincancer',
        'CriticalIllnessBenefits_CarcinomainSitu': 'CIBen_CarcinomainSitu',
        'CriticalIllnessBenefits_Coma': 'CIBen_Coma',
        'CriticalIllnessBenefits_ContinuityofCoverageTakeover': 'CIBen_ContinuityofCoverageTakeover',
        'CriticalIllnessBenefits_CoronaryArteryDisease': 'CIBen_CoronaryArteryDisease',
        'CriticalIllnessBenefits_CoveredConditionsPaysapercentageofFaceAmount': 'CIBen_CoveredConditions',
        'CriticalIllnessBenefits_EndStageRenalFailure': 'CIBen_EndStageRenalFailure',
        'CriticalIllnessBenefits_HeartAttack': 'CIBen_HeartAttack',
        'CriticalIllnessBenefits_MajorOrganFailure': 'CIBen_MajorOrganFailure',
        'CriticalIllnessBenefits_MaximumBenefitAmountxFaceAmount': 'CIBen_MaximumBenefitAmountxFaceAmount',
        'CriticalIllnessBenefits_MultipleSclerosis': 'CIBen_MultipleSclerosis',
        'CriticalIllnessBenefits_Paralysis': 'CIBen_Paralysis',
        'CriticalIllnessBenefits_ParalysisorDismemberment': 'CIBen_ParalysisorDismemberment',
        'CriticalIllnessBenefits_ParkinsonsDisease': 'CIBen_ParkinsonsDisease',
        'CriticalIllnessBenefits_PreExistingConditionsLimitation': 'CIBen_PreExistingConditionsLimitation',
        'CriticalIllnessBenefits_SkinCancerBenefitPayableonceperinsuredperyear': 'CIBen_SkinCancerBenefit',
        'CriticalIllnessBenefits_Stroke': 'CIBen_Stroke',
        'RecurrenceBenefit_BenefitsarepayableforasubsequentdiagnosisofBenignBrainTumorCancerComaHeartAttackorStroke':
            'RecBen_Benefits',
        'AdditionalBenefits_WaiveUnderwritingQuestions': 'AdditionalBenefits_WaiveUnderwritingQuestions'
    }

    if name_ not in names_dict:
        print('[ERROR] Unknown column name: ' + name_)
    else:
        return names_dict[name_]


def transform_to_dict(data_, is_di=False):
    dict_ = {}
    columns_ = []
    if is_di:
        for portion in data_:
            for input_row in portion:
                pk_state = input_row['State']
                pk_benefit = input_row['BenefitDuration']
                pk_class = input_row['Class']
                col_name = input_row['Column_Name']
                if pk_state not in dict_:
                    dict_[pk_state] = {pk_benefit: {pk_class: {'Gold': {}, 'Platinum': {}, 'Diamond': {}}}}
                if pk_benefit not in dict_[pk_state]:
                    dict_[pk_state][pk_benefit] = {pk_class: {'Gold': {}, 'Platinum': {}, 'Diamond': {}}}
                if pk_class not in dict_[pk_state][pk_benefit]:
                    dict_[pk_state][pk_benefit][pk_class] = {'Gold': {}, 'Platinum': {}, 'Diamond': {}}
                dict_[pk_state][pk_benefit][pk_class]['Gold'][col_name] = input_row['Gold_value']
                dict_[pk_state][pk_benefit][pk_class]['Platinum'][col_name] = input_row['Platinum_value']
                dict_[pk_state][pk_benefit][pk_class]['Diamond'][col_name] = input_row['Diamond_value']

                if col_name not in columns_:
                    columns_.append(col_name)
    else:
        for portion in data_:
            for input_row in portion:
                pk_state = input_row['State']
                pk_version = input_row['Version']
                col_name = input_row['Column_Name']
                if pk_state not in dict_:
                    dict_[pk_state] = {pk_version: {'Gold': {}, 'Platinum': {}, 'Diamond': {}}}
                if pk_version not in dict_[pk_state]:
                    dict_[pk_state][pk_version] = {'Gold': {}, 'Platinum': {}, 'Diamond': {}}
                dict_[pk_state][pk_version]['Gold'][col_name] = input_row['Gold_value']
                dict_[pk_state][pk_version]['Platinum'][col_name] = input_row['Platinum_value']
                dict_[pk_state][pk_version]['Diamond'][col_name] = input_row['Diamond_value']

                if col_name not in columns_:
                    columns_.append(col_name)

    return dict_, columns_


def transform_dict_to_table(dict_, columns_, is_di=False):
    if is_di:
        header_row = ['State', 'BenefitDuration', 'Class', 'Plan'] + columns_
        result_ = [header_row]
        for pk_state in dict_.keys():
            for pk_benefit in dict_[pk_state].keys():
                for pk_class in dict_[pk_state][pk_benefit].keys():
                    for pk_plan in dict_[pk_state][pk_benefit][pk_class].keys():
                        next_row = [pk_state, pk_benefit, pk_class, pk_plan]
                        for column in columns_:
                            if column in dict_[pk_state][pk_benefit][pk_class][pk_plan]:
                                next_row.append(str(dict_[pk_state][pk_benefit][pk_class][pk_plan][column]).strip())
                            else:
                                next_row.append('')
                        result_.append(next_row)

    else:
        header_row = ['State', 'Version', 'Plan'] + columns_
        result_ = [header_row]
        for pk_state in dict_.keys():
            for pk_version in dict_[pk_state].keys():
                for pk_plan in dict_[pk_state][pk_version].keys():
                    next_row = [pk_state, pk_version, pk_plan]
                    for column in columns_:
                        if column in dict_[pk_state][pk_version][pk_plan]:
                            next_row.append(str(dict_[pk_state][pk_version][pk_plan][column]).strip())
                        else:
                            next_row.append('')
                    result_.append(next_row)

    return result_


def save_to_file(data_to_save_, filename_, sheetname_):
    wb = openpyxl.Workbook()

    ws1 = wb.active
    ws1.title = sheetname_

    for row in data_to_save_:
        ws1.append(row)

    wb.save(filename=filename_)

    return 0


#config_file_name = 'data/INSTRUCTIONS - State Availability and Rate-Plans.xlsx'
#config = read_config(config_file_name, print_debug=False)

# read & transform Accident data
#accident_table_data, critical_illness_table_data, disability_income_table_data = \
#    read_data_from_files(config, print_debug=True)
#accident_dict, accident_columns = transform_to_dict(accident_table_data)
#accident_result = transform_dict_to_table(accident_dict, accident_columns)
#save_to_file(accident_result, 'output/result.xlsx', 'Combined_AccidentData')
#...done

# prepare to read CI data
#get_ci_columns(config, print_debug=True)
#...done

# read & transform CI data
#accident_table_data, critical_illness_table_data, disability_income_table_data = read_data_from_files(config,
#                                                                                                      print_debug=True)
#critical_illness_dict, critical_illness_columns = transform_to_dict(critical_illness_table_data)
#critical_illness_result = transform_dict_to_table(critical_illness_dict, critical_illness_columns)
#save_to_file(critical_illness_result, 'output/result.xlsx', 'Combined_CriticalIllnessData')

# Combined_DisabilityIncomeData
di_config_file_name = 'data/Combined_DI Data Build/Group DI - State Classifications_dh.xlsx'
di_config = read_di_config(di_config_file_name, print_debug=False)

disability_income_table_data = read_data_from_di_files(di_config, print_debug=True)
disability_income_dict, disability_income_columns = transform_to_dict(disability_income_table_data, is_di=True)
disability_income_result = transform_dict_to_table(disability_income_dict, disability_income_columns, is_di=True)
save_to_file(disability_income_result, 'output/result.xlsx', 'Combined_DisabilityInsurance')
