-- Id Name         SiteId CatalogId AccessMask
-- 11	Primus        94288        87       2048
-- DWH_Stage_Client_Primus
-- DWH_Client_Primus

-- User Theme
SELECT TOP 1
  'User' AS TableName,
  (SELECT COUNT(*) FROM DWH_Stage_Client_Primus..[User]) AS StageRecordsCount,
  (SELECT COUNT(*) FROM DWH_Client_Primus..[User]) AS ProdRecordsCount,
	NULL AS Stage,
	NULL AS Prod
FROM DWH_Stage_CurrentStateLayer..CS_User
-- Order Theme
UNION ALL
SELECT TOP 1
  'Order' AS TableName,
  (SELECT COUNT(*) FROM DWH_Stage_Client_Primus..[Order]) AS StageRecordsCount,
  (SELECT COUNT(*) FROM DWH_Client_Primus..[Order]) AS ProdRecordsCount,
	(SELECT STRING_AGG(SiteId, ', ') FROM (SELECT DISTINCT SiteId FROM DWH_Stage_Client_Primus..[Order]) res) AS Stage,
	(SELECT STRING_AGG(SiteId, ', ') FROM (SELECT DISTINCT SiteId FROM DWH_Client_Primus..[Order]) res) AS Prod
FROM DWH_Stage_CurrentStateLayer..CS_User
UNION ALL
SELECT TOP 1
  'OrderLineItem' AS TableName,
  (SELECT COUNT(*) FROM DWH_Stage_Client_Primus..OrderLineItem) AS StageRecordsCount,
  (SELECT COUNT(*) FROM DWH_Client_Primus..OrderLineItem) AS ProdRecordsCount,
	NULL AS Stage,
	NULL AS Prod
FROM DWH_Stage_CurrentStateLayer..CS_User
UNION ALL
SELECT TOP 1
  'ProductOrder' AS TableName,
  (SELECT COUNT(*) FROM DWH_Stage_Client_Primus..ProductOrder) AS StageRecordsCount,
  (SELECT COUNT(*) FROM DWH_Client_Primus..ProductOrder) AS ProdRecordsCount,
	NULL AS Stage,
	NULL AS Prod
FROM DWH_Stage_CurrentStateLayer..CS_User
UNION ALL
SELECT TOP 1
  'Payment' AS TableName,
  (SELECT COUNT(*) FROM DWH_Stage_Client_Primus..Payment) AS StageRecordsCount,
  (SELECT COUNT(*) FROM DWH_Client_Primus..Payment) AS ProdRecordsCount,
	NULL AS Stage,
	NULL AS Prod
FROM DWH_Stage_CurrentStateLayer..CS_User
UNION ALL
-- Activity Theme
SELECT TOP 1
  'Login' AS TableName,
  (SELECT COUNT(*) FROM DWH_Stage_Client_Primus..[Login]) AS StageRecordsCount,
  (SELECT COUNT(*) FROM DWH_Client_Primus..[Login]) AS ProdRecordsCount,
	(SELECT STRING_AGG(SiteId, ', ') FROM (SELECT DISTINCT SiteId FROM DWH_Stage_Client_Primus..[Login]) res) AS Stage,
	(SELECT STRING_AGG(SiteId, ', ') FROM (SELECT DISTINCT SiteId FROM DWH_Client_Primus..[Login]) res) AS Prod
FROM DWH_Stage_CurrentStateLayer..CS_User
UNION ALL
-- Product Theme
SELECT TOP 1
  'Product' AS TableName,
  (SELECT COUNT(*) FROM DWH_Stage_Client_Primus..Product) AS StageRecordsCount,
  (SELECT COUNT(*) FROM DWH_Client_Primus..Product) AS ProdRecordsCount,
	(SELECT STRING_AGG(CatalogId, ', ') FROM (SELECT DISTINCT CatalogId FROM DWH_Stage_Client_Primus..Product) res) AS Stage,
	NULL AS Prod
FROM DWH_Stage_CurrentStateLayer..CS_User
UNION ALL
SELECT TOP 1
  'Variation' AS TableName,
  (SELECT COUNT(*) FROM DWH_Stage_Client_Primus..Variation) AS StageRecordsCount,
  (SELECT COUNT(*) FROM DWH_Client_Primus..Variation) AS ProdRecordsCount,
	(SELECT STRING_AGG(CatalogId, ', ') FROM (SELECT DISTINCT CatalogId FROM DWH_Stage_Client_Primus..Variation) res) AS Stage,
	NULL AS Prod
FROM DWH_Stage_CurrentStateLayer..CS_User
UNION ALL
SELECT TOP 1
  'Kit' AS TableName,
  (SELECT COUNT(*) FROM DWH_Stage_Client_Primus..[Kit]) AS StageRecordsCount,
  (SELECT COUNT(*) FROM DWH_Client_Primus..[Kit]) AS ProdRecordsCount,
	(SELECT STRING_AGG(CatalogId, ', ') FROM (SELECT DISTINCT CatalogId FROM DWH_Stage_Client_Primus..[Kit]) res) AS Stage,
	NULL AS Prod
FROM DWH_Stage_CurrentStateLayer..CS_User
UNION ALL
-- AGGREGATES
SELECT TOP 1
  'VariationAvailability' AS TableName,
  (SELECT COUNT(*) FROM DWH_Stage_Client_Primus..AGG_UserActivity) AS StageRecordsCount,
  (SELECT COUNT(*) FROM DWH_Client_Primus..AGG_UserActivity) AS ProdRecordsCount,
	NULL AS Stage,
	NULL AS Prod
FROM DWH_Stage_CurrentStateLayer..CS_User
UNION ALL
SELECT TOP 1
  'VariationAvailability' AS TableName,
  (SELECT COUNT(*) FROM DWH_Stage_Client_Primus..AGG_VariationOrder) AS StageRecordsCount,
  (SELECT COUNT(*) FROM DWH_Client_Primus..AGG_VariationOrder) AS ProdRecordsCount,
	NULL AS Stage,
	NULL AS Prod
FROM DWH_Stage_CurrentStateLayer..CS_User
;