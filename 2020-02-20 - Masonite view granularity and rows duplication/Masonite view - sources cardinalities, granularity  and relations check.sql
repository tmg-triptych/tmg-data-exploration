-----------------------------------------------------------------------------------
-- 1) Check level's cardinality, denormalization of main level entity & joined info
-----------------------------------------------------------------------------------
--------------- ORDER [1] -------------
-- dbMarekEF_production..OrderHistories
-- dbMarekCommerceManager_production..Cls_Contact
-- dbMarekCommerceManager_production..OrderGroup_PurchaseOrder
SELECT 
  COUNT(DISTINCT oh.EpiOrderId) AS OrderCardinality, 
  COUNT(oh.EpiOrderId) - COUNT(DISTINCT oh.EpiOrderId) AS OrderDuplicates,
  COUNT(DISTINCT clc.ContactId) AS ContactCardinality, 
  COUNT(clc.ContactId) - COUNT(DISTINCT clc.ContactId) AS ContactDuplicates,
  COUNT(DISTINCT ogp.ObjectId) AS OrderGroupPOCardinality, 
  COUNT(ogp.ObjectId) - COUNT(DISTINCT ogp.ObjectId) AS OrderGroupPODuplicates
FROM dbMarekEF_production..OrderHistories oh
LEFT JOIN dbMarekCommerceManager_production..Cls_Contact clc ON oh.UserId = clc.ContactId
LEFT JOIN dbMarekCommerceManager_production..OrderGroup_PurchaseOrder ogp ON oh.EpiOrderId = ogp.ObjectId 
;
-- Cardinality  Duplicates
-- 270 320 	 0	              -- OrderHistories
--  41 423	 228 195	      -- Cls_Contact
-- 270 316	 0                -- OrderGroup_PurchaseOrder

------------- ORDERFORM [2] -------------
-- dbMarekCommerceManager_production..OrderForm
-- dbMarekCommerceManager_production..OrderFormEx
SELECT
  COUNT(DISTINCT orf.OrderFormId) AS OrderFormCardinality,
  COUNT(orf.OrderFormId) - COUNT(DISTINCT orf.OrderFormId) AS OrderFormDuplicates,
  COUNT(DISTINCT ofx.ObjectId) AS OrderFormExCardinality,
  COUNT(ofx.ObjectId) - COUNT(DISTINCT ofx.ObjectId) AS OrderFormExCardinality
FROM dbMarekCommerceManager_production..OrderForm orf
LEFT JOIN dbMarekCommerceManager_production..OrderFormEx ofx ON orf.OrderFormId = ofx.ObjectId
;
-- Cardinality  Duplicates  
-- 341 817      0              -- OrderForm
-- 341 817	    0              -- OrderFormEx

------------- LINEITEM [3] -------------
-- dbMarekEF_production..OrderItemsHistories
-- dbMarekEF_production..OrderDiscounts
-- dbMarekEF_production..OrderShipments
-- dbMarekEF_production..Shipments
-- dbMarekCommerceManager_production..OrderForm
-- dbMarekCommerceManager_production..OrderFormEx
-- dbMarekCommerceManager_production..Shipment -- with duplication filter - due to collision and asbcence fo perfect key
-- dbMarekEF_production..Addresses
-- dbMarekCommerceManager_production..OrderGroupAddress  -- with duplication filter - due to duplicate records in DB
SELECT 
  COUNT(DISTINCT oih.EpiLineItemId ) AS OIHCardinality,
  COUNT(oih.EpiLineItemId ) - COUNT(DISTINCT oih.EpiLineItemId ) AS OIHDuplicates,
  COUNT(DISTINCT od.Id ) AS OrderDiscountsCardinality,
  COUNT(od.Id ) - COUNT(DISTINCT od.Id ) AS OrderDiscountsDuplicates,
  COUNT(DISTINCT li.LineItemId ) AS LineItemCardinality,
  COUNT(li.LineItemId ) - COUNT(DISTINCT li.LineItemId ) AS LineItemDuplicates,
  COUNT(DISTINCT ss.id ) AS EfShipmentsCardinality,
  COUNT(ss.id ) - COUNT(DISTINCT ss.id ) AS EfShipmentsDuplicates,
  COUNT(DISTINCT odf.OrderFormId ) AS OrderFormCardinality,
  COUNT(odf.OrderFormId ) - COUNT(DISTINCT odf.OrderFormId ) AS OrderFormDuplicates,
  COUNT(DISTINCT ofx.ObjectId ) AS OrderFormExCardinality,
  COUNT(ofx.ObjectId ) - COUNT(DISTINCT ofx.ObjectId ) AS OrderFormExDuplicates,
  COUNT(DISTINCT sh.ShipmentId ) AS CommarceShipmentCardinality,
  COUNT(sh.ShipmentId ) - COUNT(DISTINCT sh.ShipmentId ) AS CommarceShipmentDuplicates,
  COUNT(DISTINCT ad.id ) AS ShippingAddressCardinality,
  COUNT(ad.id ) - COUNT(DISTINCT ad.id ) AS ShippingAddressDuplicates,
  COUNT(DISTINCT oga2.OrderGroupAddressId ) AS OrderGroupAddressCardinality,
  COUNT(oga2.OrderGroupAddressId ) - COUNT(DISTINCT oga2.OrderGroupAddressId ) AS OrderGroupAddressDuplicates
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekEF_production..OrderDiscounts od ON od.EpiLineItemId = oih.EpiLineItemId
LEFT JOIN dbMarekCommerceManager_production.dbo.LineItem li ON li.LineItemId = oih.EpiLineItemID
LEFT JOIN dbMarekEF_production..OrderShipments oss ON oih.EpiLineItemID = oss.EpiLineItemId 
LEFT JOIN dbMarekEF_production..Shipments ss ON oss.Shipmentid = ss.id
LEFT JOIN dbMarekCommerceManager_production..OrderForm odf ON odf.OrderFormId = li.OrderFormId
LEFT JOIN dbMarekCommerceManager_production..OrderFormEx ofx ON odf.OrderFormId = ofx.ObjectId
LEFT JOIN (
  SELECT 
    *, ROW_NUMBER() OVER (PARTITION BY OrderFormId, ShippingAddressId ORDER BY ShipmentId DESC) AS rn -- to remove 7 duplicates, possible wrong join for 7 cases from 2016 =(
  FROM dbMarekCommerceManager_production..Shipment
) sh ON li.OrderFormId = sh.OrderFormId AND li.ShippingAddressId = sh.ShippingAddressId AND sh.rn = 1
LEFT JOIN  dbMarekEF_production..Addresses ad ON CONVERT(VARCHAR(100), ad.id) = CONVERT(VARCHAR(100), sh.ShippingAddressId)
LEFT JOIN (
  SELECT
    *, ROW_NUMBER() OVER (PARTITION BY Name, OrderGroupID ORDER BY OrderGroupAddressId DESC) AS rn   -- to avoid 22% duplication, fine solution, no data loss
  FROM dbMarekCommerceManager_production..OrderGroupAddress 
) oga2 ON oga2.Name = ss.ShippingAddressId AND oih.EpiOrderId = oga2.OrderGroupID AND oga2.rn = 1
;

-- Cardinality  Duplicates  
-- 583 471    	0              -- OrderItemsHistories
-- 121        	0              -- OrderDiscounts
-- 583 331	    0	           -- LineItems
-- 273 751	    309 315	       -- EfShipments
-- 270 240	    313 091	       -- OrderForm
-- 270 240	    313 091        -- OrderFormEx
-- 214 078	    253 284        -- CommecreShipment
--  77 106	    363 999        -- Addresses
-- 322 638  	381 611        -- OrderGroupAddress

------------- PRODUCT [4] -------------
-- dbMarekEF_production..OrderItemComponentsHistories
-- dbMarekCommerceManager_production..CatalogEntry
-- dbMarekCommerceManager_production..CatalogContentProperty
SELECT
  COUNT(DISTINCT ofp.PaymentId) AS PaymentCardinality,
  COUNT(ofp.PaymentId) - COUNT(DISTINCT ofp.PaymentId) AS PaymentDuplicates,
  COUNT(DISTINCT ofpc.ObjectId) AS CustomPaymentCardinality,
  COUNT(ofpc.ObjectId) - COUNT(DISTINCT ofpc.ObjectId) AS CustomPaymentCardinality
FROM dbMarekEF_production..OrderItemComponentsHistories oich
LEFT JOIN dbMarekCommerceManager_production..CatalogEntry
LEFT JOIN dbMarekCommerceManager_production..CatalogContentProperty
;

------------- PAYMENT [5] -------------
-- dbMarekCommerceManager_production..OrderFormPayment
-- dbMarekCommerceManager_production..OrderFormPayment_CustomPayment
SELECT
  COUNT(DISTINCT ofp.PaymentId) AS PaymentCardinality,
  COUNT(ofp.PaymentId) - COUNT(DISTINCT ofp.PaymentId) AS PaymentDuplicates,
  COUNT(DISTINCT ofpc.ObjectId) AS CustomPaymentCardinality,
  COUNT(ofpc.ObjectId) - COUNT(DISTINCT ofpc.ObjectId) AS CustomPaymentCardinality
FROM dbMarekCommerceManager_production..OrderFormPayment ofp
LEFT JOIN dbMarekCommerceManager_production..OrderFormPayment_CustomPayment ofpc ON ofp.PaymentId = ofpc.ObjectId 
;
-- Cardinality  Duplicates
-- 271 821	 0	               -- OrderFormPayment
-- 224 098	 0	               -- OrderFormPayment_CustomPayment

------------- PROMOTION [6] -------------
-- dbMarekCommerceManager_production..PromotionInformation
SELECT
  COUNT(DISTINCT pin.PromotionInformationId) AS Cardinality, COUNT(pin.PromotionInformationId) - COUNT(DISTINCT pin.PromotionInformationId) AS Duplicates
FROM dbMarekCommerceManager_production..PromotionInformation pin 
;
-- Cardinality  Duplicates
-- 4 165	0

------------- CUSTOMIZATION [7] -------------
SELECT
  COUNT(DISTINCT oich.Id) AS Cardinality, COUNT(oich.Id) - COUNT(DISTINCT oich.Id) AS Duplicates
FROM dbMarekEF_production..OrderItemComponentsHistories oich
LEFT JOIN dbMarekEF_production..Customizations cc ON cc.ComponentId = oich.Id 
;

SELECT COUNT(DISTINCT cc.ComponentId), COUNT (cc.ComponentId) FROM dbMarekEF_production..Customizations cc;
-- 171 584	172 003
-- ComponentId is just not unique identifier here
-- is it ok or table filling script error?
-- does this have business meaning?

SELECT
  ComponentId, COUNT(*)
FROM dbMarekEF_production..Customizations
GROUP BY ComponentId
HAVING COUNT(*) > 1
;
-- 306691
-- 660845
-- 309124
SELECT * FROM dbMarekEF_production..Customizations WHERE ComponentId IN (306691, 660845,309124);

-------------------------------------
-- 2) Check relations between levels
-------------------------------------
--------------- ORDER-PAYMENT is 1:M -------------
SELECT 
  COUNT(oh.EpiOrderId) - COUNT(DISTINCT oh.EpiOrderId) AS OrdersDuplication,
  COUNT(ofp.PaymentId) - COUNT(DISTINCT ofp.PaymentId) AS PaymentDuplication
FROM dbMarekEF_production..OrderHistories oh
LEFT JOIN dbMarekCommerceManager_production..OrderFormPayment ofp ON ofp.OrderGroupId = oh.EpiOrderId
LEFT JOIN  dbMarekCommerceManager_production..OrderFormPayment_CustomPayment ofpc ON ofp.PaymentId = ofpc.ObjectId 
;
-- 2 067	0

--------------- ORDER-PROMOTION is 1:M -------------
SELECT 
  COUNT(oh.EpiOrderId) - COUNT(DISTINCT oh.EpiOrderId) AS LineItemsDuplication,
  COUNT(pin.PromotionInformationId) - COUNT(DISTINCT pin.PromotionInformationId) AS PromotionDuplication
FROM dbMarekEF_production..OrderHistories oh
LEFT JOIN dbMarekCommerceManager_production..OrderForm odf ON odf.OrderGroupId = oh.EpiOrderId
LEFT JOIN dbMarekCommerceManager_production..PromotionInformation pin ON odf.OrderFormId = pin.OrderFormId 
;
-- 649	0

--------------- ORDER-LINEITEM is 1:M -------------
SELECT 
  COUNT(oh.EpiOrderId) - COUNT(DISTINCT oh.EpiOrderId) AS OrdersDuplication,
  COUNT(oih.EpiLineItemId) - COUNT(DISTINCT oih.EpiLineItemId) AS LineItemsDuplication
FROM dbMarekEF_production..OrderHistories oh
LEFT JOIN dbMarekEF_production..OrderItemsHistories oih ON oh.EpiOrderId = oih.EpiOrderId
;
-- 312 559	0

--------------- LINEITEM-PRODUCT is 1:M -------------
SELECT 
  COUNT(oih.EpiLineItemId) - COUNT(DISTINCT oih.EpiLineItemId) AS LineItemsDuplication,
  COUNT(oich.Id) - COUNT(DISTINCT oich.Id) AS ProductDuplication
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekEF_production..OrderItemComponentsHistories oich ON oich.EpiLineItemID = oih.EpiLineItemId 
;
-- 77 277	0
