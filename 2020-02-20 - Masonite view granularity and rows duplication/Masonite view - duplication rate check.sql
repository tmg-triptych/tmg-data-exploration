---------------------------------------------------------------------
-- 1) Check joins duplication rates
---------------------------------------------------------------------

--------------- ORDER -------------
-- dbMarekEF_production..OrderHistories
-- dbMarekCommerceManager_production..Cls_Contact
-- dbMarekCommerceManager_production..OrderGroup_PurchaseOrder

-- dbMarekEF_production..OrderHistories alone
SELECT 
   COUNT(oh.EpiOrderId) - COUNT(DISTINCT oh.EpiOrderId)
FROM dbMarekEF_production..OrderHistories oh 
;
-- 0

-- JOIN dbMarekCommerceManager_production..Cls_Contact
SELECT 
   COUNT(oh.EpiOrderId) - COUNT(DISTINCT oh.EpiOrderId)
FROM dbMarekEF_production..OrderHistories oh 
LEFT JOIN dbMarekCommerceManager_production..Cls_Contact clc ON oh.UserId = clc.ContactId
;
-- 0

-- JOIN dbMarekCommerceManager_production..OrderGroup_PurchaseOrder
SELECT 
   COUNT(oh.EpiOrderId) - COUNT(DISTINCT oh.EpiOrderId)
FROM dbMarekEF_production..OrderHistories oh 
LEFT JOIN dbMarekCommerceManager_production..OrderGroup_PurchaseOrder ogp ON oh.EpiOrderId = ogp.ObjectId 
;
-- 0


------------- LINEITEM -------------
-- dbMarekEF_production..OrderItemsHistories
-- dbMarekEF_production..OrderDiscounts
-- dbMarekCommerceManager_production..OrderForm
-- dbMarekEF_production..OrderShipments
-- dbMarekEF_production..Shipments
-- dbMarekCommerceManager_production..OrderGroupAddress
-- Subquery: bill
-- dbMarekCommerceManager_production..Shipment
-- dbMarekEF_production..Addresses

-- dbMarekEF_production..OrderItemsHistories alone (+ extra data from parent Order) 
SELECT
  COUNT(oih.EpiLineItemID) - COUNT(DISTINCT oih.EpiLineItemID)
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekEF_production..OrderHistories oh ON oh.EpiOrderId = oih.EpiOrderId
;
-- 0

-- JOIN dbMarekEF_production..OrderDiscounts
SELECT
  COUNT(oih.EpiLineItemID) - COUNT(DISTINCT oih.EpiLineItemID)
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekEF_production..OrderHistories oh ON oh.EpiOrderId = oih.EpiOrderId
LEFT JOIN dbMarekEF_production..OrderDiscounts od ON od.EpiOrderId = oh.EpiOrderId -- from view
--LEFT JOIN dbMarekEF_production..OrderDiscounts od ON od.EpiLineItemId = oih.EpiLineItemId -- this will work without duplicates
;
-- 1000

-- JOIN dbMarekCommerceManager_production..OrderForm
SELECT
  COUNT(oih.EpiLineItemID) - COUNT(DISTINCT oih.EpiLineItemID)
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekEF_production..OrderHistories oh ON oh.EpiOrderId = oih.EpiOrderId
LEFT JOIN dbMarekCommerceManager_production..OrderForm ofd ON oh.EpiOrderId = ofd.OrderGroupId -- from view
-- LEFT JOIN dbMarekCommerceManager_production.dbo.LineItem li ON li.LineItemId = oih.EpiLineItemID -- this will work without duplicates
-- LEFT JOIN dbMarekCommerceManager_production..OrderForm ofd ON ofd.OrderFormId = li.OrderFormId   -- this will work without duplicates
;
-- 11


-- JOIN dbMarekEF_production..OrderShipments
SELECT
  COUNT(oih.EpiLineItemID) - COUNT(DISTINCT oih.EpiLineItemID)
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekEF_production..OrderHistories oh ON oh.EpiOrderId = oih.EpiOrderId
LEFT JOIN dbMarekEF_production..OrderShipments oss ON oih.EpiLineItemID = oss.EpiLineItemId 
;
-- 0

-- JOIN dbMarekEF_production..Shipments
SELECT
  COUNT(oih.EpiLineItemID) - COUNT(DISTINCT oih.EpiLineItemID)
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekEF_production..OrderHistories oh ON oh.EpiOrderId = oih.EpiOrderId
LEFT JOIN dbMarekEF_production..OrderShipments oss ON oih.EpiLineItemID = oss.EpiLineItemId 
LEFT JOIN dbMarekEF_production..Shipments ss ON oss.Shipmentid = ss.id
;
-- 0

-- JOIN dbMarekCommerceManager_production..OrderGroupAddress
SELECT
  COUNT(oih.EpiLineItemID) - COUNT(DISTINCT oih.EpiLineItemID)
LEFT JOIN dbMarekEF_production..OrderShipments oss ON oih.EpiLineItemID = oss.EpiLineItemId 
LEFT JOIN dbMarekEF_production..Shipments ss ON oss.Shipmentid = ss.id
LEFT JOIN dbMarekCommerceManager_production..OrderGroupAddress oga2 ON oga2.Name = ss.ShippingAddressId AND oih.EpiOrderId = oga2.OrderGroupID -- from view
-- don't know better solution yet
;
-- 129 833

-- JOIN Subquery: bill
SELECT
  COUNT(oih.EpiLineItemID) - COUNT(DISTINCT oih.EpiLineItemID)
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekEF_production..OrderHistories oh ON oh.EpiOrderId = oih.EpiOrderId
LEFT JOIN(
	SELECT distinct 
		oh.EpiOrderId
		,oga.[FirstName]
		,oga.[LastName]
		,oga.[Line1]
		,oga.[Line2]
		,oga.[City]
		,oga.[State]
		,oga.[PostalCode]
		,oga.Email
		,oga.Organization
		,oga.CountryName
		,oga.DaytimePhoneNumber
		,ofx.DataCollectionFieldViewData as Reference
	FROM dbMarekEF_production..OrderHistories oh
	join [dbMarekCommerceManager_production]..OrderForm orf on oh.EpiOrderId = orf.OrderGroupId
	join [dbMarekCommerceManager_production].[dbo].[OrderFormEx] ofx on orf.OrderFormId = ofx.ObjectId
	join [dbMarekCommerceManager_production].[dbo].[OrderGroupAddress] oga on orf.BillingAddressId = oga.Name
	where PaymentMethod = 'credit card'
) as bill on oh.EpiOrderId = bill.EpiOrderId
;
-- 33 679
-- better join single missing table OrderFormEx directly:
SELECT
  COUNT(oih.EpiLineItemID), COUNT(oih.EpiLineItemID) - COUNT(DISTINCT oih.EpiLineItemID)
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekEF_production..OrderHistories oh ON oh.EpiOrderId = oih.EpiOrderId
LEFT JOIN dbMarekCommerceManager_production.dbo.LineItem li ON li.LineItemId = oih.EpiLineItemID
LEFT JOIN dbMarekCommerceManager_production..OrderForm ofd ON ofd.OrderFormId = li.OrderFormId
LEFT JOIN dbMarekCommerceManager_production..OrderFormEx ofx ON ofd.OrderFormId = ofx.ObjectId
;
-- 582 547 0 

-- JOIN dbMarekCommerceManager_production..Shipment
SELECT
  COUNT(oih.EpiLineItemID), COUNT(oih.EpiLineItemID) - COUNT(DISTINCT oih.EpiLineItemID)
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekEF_production..OrderHistories oh ON oh.EpiOrderId = oih.EpiOrderId
LEFT JOIN dbMarekCommerceManager_production.dbo.LineItem li ON li.LineItemId = oih.EpiLineItemID
LEFT JOIN dbMarekCommerceManager_production..Shipment sh ON oh.EpiOrderId = sh.OrderGroupId                                               -- from view
--LEFT JOIN dbMarekCommerceManager_production..Shipment sh ON li.ShippingAddressId = sh.ShippingAddressId AND li.OrderFormId = sh.OrderFormId -- this reduces duplicates to 7
;
-- 604 219	21 672

-- JOIN dbMarekEF_production..Addresses
SELECT
  COUNT(oih.EpiLineItemID), COUNT(oih.EpiLineItemID) - COUNT(DISTINCT oih.EpiLineItemID)
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekEF_production..OrderHistories oh ON oh.EpiOrderId = oih.EpiOrderId
LEFT JOIN dbMarekCommerceManager_production.dbo.LineItem li ON li.LineItemId = oih.EpiLineItemID
--LEFT JOIN dbMarekCommerceManager_production..Shipment sh ON oh.EpiOrderId = sh.OrderGroupId                                               -- from view
LEFT JOIN dbMarekCommerceManager_production..Shipment sh ON li.ShippingAddressId = sh.ShippingAddressId AND li.OrderFormId = sh.OrderFormId -- this reduces duplicates to 7
LEFT JOIN dbMarekEF_production..Addresses ad ON CONVERT(VARCHAR(100), ad.id) = CONVERT(VARCHAR(100), sh.ShippingAddressId)
;
-- 582 554	7 (all duplicates from Shipment join, no new)

--------------- ORDER -------------
-- dbMarekEF_production..OrderItemComponentsHistories
-- dbMarekCommerceManager_production..CatalogEntry
-- dbMarekCommerceManager_production..CatalogContentProperty
-- dbMarekEF_production..Customizations
-- Subquery: parent

-- dbMarekEF_production..OrderItemComponentsHistories alone
SELECT
  COUNT(oich.Id), COUNT(oich.Id) - COUNT(DISTINCT oich.Id)
FROM dbMarekEF_production..OrderItemComponentsHistories oich
;
-- 659646	0

-- JOIN dbMarekCommerceManager_production..CatalogEntry
SELECT
  COUNT(oich.Id), COUNT(oich.Id) - COUNT(DISTINCT oich.Id)
FROM dbMarekEF_production..OrderItemComponentsHistories oich
LEFT JOIN dbMarekCommerceManager_production..CatalogEntry ce ON oich.SkuID = ce.ContentGuid
;
-- 659646	0

-- JOIN dbMarekCommerceManager_production..CatalogContentProperty
SELECT
  COUNT(oich.Id), COUNT(oich.Id) - COUNT(DISTINCT oich.Id)
FROM dbMarekEF_production..OrderItemComponentsHistories oich
LEFT JOIN dbMarekCommerceManager_production..CatalogEntry ce ON oich.SkuID = ce.ContentGuid
LEFT JOIN dbMarekCommerceManager_production..CatalogContentProperty cp ON ce.CatalogEntryId = cp.ObjectId  AND cp.MetaFieldName = 'DisplayName' -- from view
--LEFT JOIN dbMarekCommerceManager_production..CatalogContentProperty cp ON ce.CatalogEntryId = cp.ObjectId AND cp.MetaClassId = ce.MetaClassId AND cp.MetaFieldName = 'DisplayName' -- this works fine
;
-- 662427	2781

-- JOIN Subquery: parent
SELECT
  COUNT(DISTINCT oich.Id) AS Cardinality, COUNT(oich.Id) - COUNT(DISTINCT oich.Id) AS Duplicates
FROM dbMarekEF_production..OrderItemComponentsHistories oich
LEFT JOIN dbMarekEF_production..OrderItemsHistories oih ON oih.EpiLIneItemId = oich.EpiLIneItemId
left join (
	select CatalogEntryId,CatalogId,Name ,ccp.LongString as SKU
	from dbMarekCommerceManager_production..[CatalogEntry] ce with(nolock)
	join dbMarekCommerceManager_production..[CatalogContentProperty] ccp with(nolock) on ce.CatalogEntryId = ccp.ObjectId and ccp.MetaFieldName = 'SKU' AND ccp.MetaClassId = ce.MetaClassId
	where classtypeid not in ('Variation','Package') and ce.EndDate >= getdate() and ce.Name not like '%-test%' and ce.Name not like '%test_%'
) parent on oih.Sku = parent.Sku
;
-- 659 647	68 448

-- JOIN dbMarekEF_production..Customizations
SELECT
  COUNT(DISTINCT oich.Id) AS Cardinality, COUNT(oich.Id) - COUNT(DISTINCT oich.Id) AS Duplicates
FROM dbMarekEF_production..OrderItemComponentsHistories oich
LEFT JOIN dbMarekEF_production..Customizations cc ON cc.ComponentId = oich.Id 
;
-- 659 605	419

SELECT COUNT(DISTINCT cc.ComponentId), COUNT (cc.ComponentId) FROM dbMarekEF_production..Customizations cc;
-- 171 584	172 003

-- ComponentId is just not unique identifier here
-- is it ok or table filling script error?
-- does this have business meaning?

--------------- PAYMENT -------------
-- dbMarekCommerceManager_production..OrderFormPayment
-- dbMarekCommerceManager_production..OrderFormPayment_CustomPayment

-- dbMarekCommerceManager_production..OrderFormPayment alone
SELECT
  COUNT(DISTINCT ofp.PaymentId ) AS Cardinality, COUNT(ofp.PaymentId ) - COUNT(DISTINCT ofp.PaymentId ) AS Duplicates
FROM dbMarekCommerceManager_production..OrderFormPayment ofp
;
-- 271 458	0

-- JOIN dbMarekCommerceManager_production..OrderFormPayment_CustomPayment
SELECT
  COUNT(DISTINCT ofp.PaymentId ) AS Cardinality, COUNT(ofp.PaymentId ) - COUNT(DISTINCT ofp.PaymentId ) AS Duplicates
FROM dbMarekCommerceManager_production..OrderFormPayment ofp
LEFT JOIN dbMarekCommerceManager_production..OrderFormPayment_CustomPayment ofpc ON ofp.PaymentId = ofpc.ObjectId 
;
-- 271 458	0