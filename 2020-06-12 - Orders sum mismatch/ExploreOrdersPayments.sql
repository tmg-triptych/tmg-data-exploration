USE ExploreData;

DROP TABLE OrderPaymentsSumMismatch;
SELECT
	oh.EpiOrderId, 
	oh.OrderTrackId,
	oh.CreationDate,
	YEAR(oh.CreationDate) y,
	MONTH(oh.CreationDate) m,
	oh.Total,
	oh.Coop,
	p.PaymentAmount_Processed,
	p.PaymentAmount_Pending,
	p.PaymentAmount_Failed,
	p.PaymentAmount_NULL,
	oh.Total + oh.Coop - p.PaymentAmount_Processed AS diff,
	CASE WHEN ABS(oh.Total + oh.Coop - p.PaymentAmount_Processed) > 0 THEN 1 ELSE 0 END AS mismatch_flag,
	CASE WHEN ABS(oh.Total + oh.Coop - p.PaymentAmount_Processed) > 0.05 THEN 1 ELSE 0 END AS mismatch_more_5_c_flag
INTO OrderPaymentsSumMismatch
FROM dbMarekEF_production..OrderHistories oh
LEFT JOIN (
	SELECT
		ofp.OrderGroupId,
		COALESCE(SUM(CASE WHEN Status = 'Processed' THEN Amount END), 0) AS PaymentAmount_Processed,
		COALESCE(SUM(CASE WHEN Status = 'Pending' THEN Amount END), 0) AS PaymentAmount_Pending,
		COALESCE(SUM(CASE WHEN Status = 'Failed' THEN Amount END), 0) AS PaymentAmount_Failed,
		COALESCE(SUM(CASE WHEN Status IS NULL THEN Amount END), 0) AS PaymentAmount_NULL
	FROM dbMarekCommerceManager_production..OrderFormPayment ofp
	GROUP BY ofp.OrderGroupId
) p ON p.OrderGroupId = oh.EpiOrderId
WHERE OrderState = 50
;

DROP TABLE OrdersWithMismatch;
SELECT
  *
INTO OrdersWithMismatch
FROM OrderPaymentsSumMismatch
WHERE
  mismatch_more_5_c_flag = 1 AND (
    y = 2020 OR (
	  y = 2019 AND
	  m = 12
	)
  );
;


DROP TABLE OrdersWithMismatch_02;
SELECT
  res.*,
  a.PostalCode
INTO OrdersWithMismatch_02
FROM (
  SELECT 
    owm.*, ofp.BillingAddressId AS AddressId, 'Billing' AS AddressType
  FROM OrdersWithMismatch owm
  LEFT JOIN dbMarekCommerceManager_production..OrderFormPayment ofp ON ofp.OrderGroupId = owm.EpiOrderId
  UNION ALL
  SELECT
   owm.* ,  CAST(s.ShippingAddressId AS uniqueidentifier) AS AddressId, 'Shipping' AS AddressType
  FROM OrdersWithMismatch owm
  LEFT JOIN dbMarekEF_production..OrderItemsHistories oih ON oih.EpiOrderId = owm.EpiOrderId
  LEFT JOIN dbMarekEF_production..OrderShipments oss ON oih.EpiLineItemId = oss.EpiLineItemId
  LEFT JOIN dbMarekEF_production..Shipments s ON s.Id = oss.ShipmentId  
) res
LEFT JOIN (
	SELECT
		ROW_NUMBER() OVER (PARTITION BY Name, OrderGroupId ORDER BY OrderGroupAddressId) AS rn,
		*
	FROM dbMarekCommerceManager_production..OrderGroupAddress    
) a ON
a.Name = res.AddressId AND
a.OrderGroupId = res.EpiOrderId AND 
a.rn = 1
;

SELECT DISTINCT
  t.EpiOrderId,
  t.OrderTrackId,
  t.CreationDate,
  t.Total,
  t.Coop,
  t.PaymentAmount_Processed,
  t.diff,
  t.AddressId,
  t.AddressType,
  t.PostalCode,
  t1.LatestUpdateDate lud1,
  t2.LatestUpdateDate lud2,
  t3.LatestUpdateDate lud3,
  t4.LatestUpdateDate lud4,
  t5.LatestUpdateDate lud5,
  t6.LatestUpdateDate lud6
FROM OrdersWithMismatch_02 t
LEFT JOIN taxes t1 ON t1.Zip = t.PostalCode AND t1.TotalTaxExempt = 'SERVICES'
LEFT JOIN taxes t2 ON t2.Zip = t.PostalCode AND t2.TotalTaxExempt = 'LABOR/SERVICES'
LEFT JOIN taxes t3 ON t3.Zip = t.PostalCode AND t3.TotalTaxExempt = 'FREIGHT/SERVICES'
LEFT JOIN taxes t4 ON t4.Zip = t.PostalCode AND t4.TotalTaxExempt = 'LABOR/SERVICES/FREIGHT'
LEFT JOIN taxes t5 ON t5.Zip = t.PostalCode AND t5.TotalTaxExempt = ''
LEFT JOIN taxes t6 ON t6.Zip = t.PostalCode AND t6.TotalTaxExempt = 'ABOR/FREIGHT/SERVICES'
;



