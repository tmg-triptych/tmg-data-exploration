--USE Stage_ExploreData
USE ExploreData
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


DROP PROCEDURE IF EXISTS Rebuild_MGIC_CS_ORDER_EXPORT;
GO
CREATE PROCEDURE Rebuild_MGIC_CS_ORDER_EXPORT
AS
DECLARE
  @time_before DATETIME,
  @time_after DATETIME,
  @tdiff [int],
  @records_before [int],
  @records_after [int];
BEGIN
  SET NOCOUNT ON;

  SET @time_before = GETDATE();
    SET @records_before = 0
      
  IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MGIC_CS_ORDER_EXPORT'))
    BEGIN
       SET @records_before = (SELECT COUNT(*) FROM MGIC_CS_ORDER_EXPORT);
    END

  drop table if exists #orderstatus;
  select 
    statustable.OrderTrackId
    ,statustable.EpiOrderId
    ,statustable.CreationDate
    ,statustable.siteid
    ,statustable.OrderState
    ,statustable.Shipstate
    ,case 
			when statustable.orderstate in (0) then 'In Progress' --- changed FROM Received
			when statustable.orderstate in (10) then 'In Review'
			when statustable.orderstate in (20) then 'Rejected'
			when statustable.orderstate in (30) then 'In Progress'
			when statustable.orderstate in (40) then 'In Progress'
			when statustable.orderstate in (50) and statustable.ShipState LIKE '%10%' then 'In Progress'
			when statustable.orderstate in (50) and statustable.ShipState LIKE '%0%' then 'Completed'
			when statustable.orderstate in (50) and statustable.ShipState LIKE '%20%' then 'Completed'
			when statustable.orderstate in (60) then 'Failed'
			when statustable.orderstate in (70) then 'Canceled'
			when statustable.orderstate in (80) then 'Returned'
			else 'In Progress'
		end as calculatedOrderStatus
  into #orderstatus
  from (
    select distinct
      oh.ordertrackid
      ,oh.OrderState
      ,oh.EpiOrderId
      ,oh.CreationDate
      ,oh.SiteId
      ,stuff(
				(
					select 
						'|' + CAST(oich1.ShipState as varchar)
					from dbMarekEF_production..OrderItemComponentsHistories oich1 with(nolock) 	
					where oich1.EpiOrderId=oich.EpiOrderId
					FOR XML PATH('')
				), 1, 1, ''
			) as Shipstate
    from dbMarekEF_production..OrderItemComponentsHistories oich with(nolock)
    left join	dbMarekEF_production..OrderHistories oh with(nolock) on oich.EpiOrderId = oh.EpiOrderId
		where oh.SiteId = 93345 -- MGIC only
  ) statustable
  ;
  
  drop table if exists #shipdates;
  select  
    oich.ShipDate
    ,oh.EpiOrderId
    ,oich.EpiLineItemId 
    ,CONCAT(DATEPART(Year,oich.ShipDate),DATEPART(Month,oich.ShipDate),DATEPART(day,oich.ShipDate)) as Date
	into #shipdates
  from  dbMarekEF_production..OrderItemComponentsHistories oich with(nolock) 
	left join dbMarekEF_production..OrderHistories oh with(nolock) on oh.EpiOrderId = oich.EpiOrderId
	where oh.SiteId = 93345 -- MGIC only
  
    
  drop table if exists MGIC_CS_ORDER_EXPORT;
	select
		oh.OrderTrackId as OrderNumber
		,oh.Name AS ORDERNAME
		,coalesce(clc.FullName,oh.userName) as CustomerName
		,clc.Email AS Email
		,oh.UserName AS AccountId    
		,coalesce(oaf3.Value, '') AS SalesRegion
		,coalesce(aa.ExternalAddressId,'') AS ContactId
		,oh.Total   	
		,ogp.Created AS OrderDate 
		,os.calculatedOrderStatus as ORDERStatus 
		,coalesce(oaf.Value, '') AS RecipientMessage
		,coalesce(oga2.FirstName, ss.FirstName) as FirstName
		,coalesce(oga2.LastName, ss.LastName) as LastName
		,CONCAT_WS(
		  ' ',
			coalesce(oga2.FirstName, ss.FirstName),
			coalesce(oga2.LastName, ss.LastName)
		) as RecipientName
		,oga2.Organization as CompanyName	
		,oga2.Line1  as Line1
		,oga2.Line2 as Line2
		,oga2.city as City
		,oga2.state as State
		,oga2.PostalCode  as Zip
		,ss.ShippingMethodName as ShippingMethod
		,ordership.Latestshipdate AS Shipdate
    ,oh.EpiOrderId
  into MGIC_CS_ORDER_EXPORT 
	from dbMarekEF_production..OrderHistories oh with(nolock)
	left join dbMarekCommerceManager_production..Cls_Contact clc with(nolock) on oh.UserId = clc.ContactId
	left join dbMarekCommerceManager_production..aspnet_Membership am with(nolock) on am.Email = clc.Email
	left join dbMarekEF_production..UserProfile up with(nolock) on am.UserId = up.UserId
	left join dbMarekCommerceManager_production..OrderGroup_PurchaseOrder ogp with(nolock) on oh.EpiOrderId = ogp.ObjectId
	left join dbMarekEF_production..OrderAdditionalFields oaf with (nolock) on oaf.EpiOrderId = oh.EpiOrderId and oaf.Name = 'RecipientMessage'
	left join dbMarekEF_production..OrderAdditionalFields oaf2 with (nolock) on oaf2.EpiOrderId = oh.EpiOrderId and oaf2.Name = 'ContactID'
	left join dbMarekEF_production..OrderAdditionalFields oaf3 with (nolock) on oaf3.EpiOrderId = oh.EpiOrderId and oaf3.Name = 'SalesRegion'
	left join #orderstatus os on os.EpiOrderId = oh.EpiOrderId 
	left join (
		select 
			MAX(shipdate) as Latestshipdate
			,epiorderid
		from #shipdates
		where shipdate IS NOT NULL
		group by epiorderid
	) ordership on ordership.epiorderid = oh.EpiOrderId
	join (
		select
			EpiLineItemId
			,EpiOrderId
			,row_number() OVER (partition by EpiOrderId order by EpiLineItemId) rn  -- take first among several (used to join shipping data, which is same for all lineitems)
		from dbMarekEF_production..OrderItemsHistories with(nolock)
	) oih on oh.EpiOrderId = oih.EpiOrderId and oih.rn = 1 
	left join dbMarekEF_production..OrderShipments oss with(nolock) on oih.EpiLineItemID = oss.EpiLineItemId
	left join dbMarekEF_production..Shipments ss with(nolock) on oss.Shipmentid = ss.id
	left join (
		select
		  *
			,row_number() OVER (partition by OrderGroupId order by OrderGroupAddressId) rn -- take fist among several (it's expected there's single shipping per all lineitems)
		from dbMarekCommerceManager_production..OrderGroupAddress oga2a with(nolock)
	)	as oga2 on oga2.Name = ss.ShippingAddressId and oh.EpiOrderId = oga2.OrderGroupID and oga2.rn = 1
	left join dbMarekEF_production..Addresses aa with(nolock) on convert(varchar(100),aa.id) = convert(varchar(100),ss.shippingaddressid) 
	where
	  oh.SiteId = 93345 -- MGIC only
	  and (clc.Email not in (select distinct email from Triptych_Utility.dbo.exclude_from_reports with(nolock)) or clc.email is null) 
	  and oh.CreationDate > '2020-12-06 00:00:0.000'
  ;

  SET @time_after = GETDATE();
  SET @tdiff = DATEDIFF(second, @time_before, @time_after);
  SET @records_after = (SELECT COUNT(*) FROM MGIC_CS_ORDER_EXPORT);
  RAISERROR('..MGIC_CS_ORDER_EXPORT built, elapsed time: %d sec, %d to %d records', 10, 1, @tdiff, @records_before, @records_after) WITH NOWAIT
END
GO

-- =============================================

DROP PROCEDURE IF EXISTS Rebuild_MGIC_CS_LINEITEM_EXPORT;
GO
CREATE PROCEDURE Rebuild_MGIC_CS_LINEITEM_EXPORT
AS
DECLARE
  @time_before DATETIME,
  @time_after DATETIME,
  @tdiff [int],
  @records_before [int],
  @records_after [int];
BEGIN
  SET NOCOUNT ON;

  SET @time_before = GETDATE();
    SET @records_before = 0
      
  IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MGIC_CS_LINEITEM_EXPORT'))
    BEGIN
       SET @records_before = (SELECT COUNT(*) FROM MGIC_CS_LINEITEM_EXPORT);
    END

  drop table if exists MGIC_CS_LINEITEM_EXPORT;  
	select distinct
		oh.OrderTrackId as OrderNumber
		,oih.SKU
		,oih.BaseSku
		,CONCAT_WS(', ', oih.BaseSku, oih.SKU) as ItemNumberSKU
		,coalesce(oih.Name,cer.name) as Name
		,oih.Quantity
		,oih.Price as PricePerUnit
		,oih.Price*oih.Quantity as LineSubtotal
		,case
			when oih.State in (0) then 'In Progress'
			when oih.State in (20) and oich.ShipState in (0,20) then 'Completed'  
			when oih.State in (30) then 'Error'
			when oih.State in (40) then 'Canceled'
			when oih.State in (50)  then 'Returned'
			when oih.State in (60) then 'Rejected'
			else 'In Progress'
		end as LineStatus
		,CURRENT_TIMESTAMP AS EXPORTDATETIME
		,ss.ShippingMethodName as ShippingMethod
		,(case when ss.ShippingMethodName LIKE '%DIGITALDOWNLOAD%' then oh.CreationDate ELSE oich.ShipDate END ) as ShipDate
		,coalesce(oih.TrackingNumber,'') as  TrackingNumber
    ,oh.EpiOrderId
    ,oih.EpiLineItemId
  into MGIC_CS_LINEITEM_EXPORT 
	from dbMarekEF_production..OrderHistories oh with(nolock)
	join dbMarekEF_production..OrderItemsHistories oih with(nolock) on oh.EpiOrderId = oih.EpiOrderId
	join dbMarekEF_production..OrderItemComponentsHistories oich with(nolock) on oih.EpiLineItemId = oich.EpiLineItemId
	join dbMarekCommerceManager_production..CatalogEntry ce with(nolock) on oich.SkuID = ce.ContentGuid --child
	left join dbMarekCommerceManager_production..CatalogEntry cer with(nolock) on oih.ToolId = cer.ContentGuid -- parent
	left join dbMarekCommerceManager_production..CatalogContentProperty cp3 with(nolock) ON cer.CatalogEntryId = cp3.ObjectId AND cp3.MetaFieldName = 'ToolTypeName'
	left join dbMarekEF_production..OrderShipments oss with(nolock) on oih.EpiLineItemID = oss.EpiLineItemId
	left join dbMarekEF_production..Shipments ss with(nolock) on oss.Shipmentid = ss.id
	left join dbMarekCommerceManager_production..Cls_Contact clc with(nolock) on oh.UserId = clc.ContactId
	left join dbMarekCommerceManager_production..aspnet_Membership am with(nolock) on am.Email = clc.Email
	where
		ce.CatalogId = 115 -- MGIC only
		and (clc.Email not in (select distinct email from Triptych_Utility.dbo.exclude_from_reports with(nolock)) or clc.email is null) 
		and oh.CreationDate > '2020-12-06 00:00:0.000'
  ;
  
  SET @time_after = GETDATE();
  SET @tdiff = DATEDIFF(second, @time_before, @time_after);
  SET @records_after = (SELECT COUNT(*) FROM MGIC_CS_LINEITEM_EXPORT);
  RAISERROR('..MGIC_CS_LINEITEM_EXPORT built, elapsed time: %d sec, %d to %d records', 10, 1, @tdiff, @records_before, @records_after) WITH NOWAIT
END
GO

-- =============================================

DROP PROCEDURE IF EXISTS Rebuild_MGIC_CS;
GO
CREATE PROCEDURE Rebuild_MGIC_CS
AS
DECLARE
  @time_before DATETIME,
  @time_after DATETIME,
  @tdiff [int],
  @records_before [int],
  @records_after [int];
BEGIN

  EXECUTE Rebuild_MGIC_CS_LINEITEM_EXPORT
  EXECUTE Rebuild_MGIC_CS_ORDER_EXPORT
  
END
GO


-- =============================================

EXECUTE Rebuild_MGIC_CS
GO