USE ExploreData
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================

DROP PROCEDURE IF EXISTS Update_MGIC_LINEITEM_EXPORT;
GO
CREATE PROCEDURE Update_MGIC_LINEITEM_EXPORT
AS
DECLARE
  @time_before DATETIME,
  @time_after DATETIME,
  @tdiff [int],
  @records_before [int],
  @records_after [int];
BEGIN
  SET NOCOUNT ON;

  SET @time_before = GETDATE();
    SET @records_before = 0
      
  IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MGIC_LINEITEM_EXPORT'))
    BEGIN
      SET @records_before = (SELECT COUNT(*) FROM MGIC_LINEITEM_EXPORT);
			 
			drop table if exists MGIC_LINEITEM_EXPORT_OLD;
			exec sp_rename 'MGIC_LINEITEM_EXPORT', 'MGIC_LINEITEM_EXPORT_OLD';

			select
				coalesce(cs.OrderNumber, hist.OrderNumber) as OrderNumber
				,coalesce(cs.SKU, hist.SKU) as SKU
				,coalesce(cs.BaseSku, hist.BaseSku) as BaseSku
				,coalesce(cs.ItemNumberSKU, hist.ItemNumberSKU) as ItemNumberSKU
				,coalesce(cs.Name, hist.Name) as Name
				,coalesce(cs.Quantity, hist.Quantity) as Quantity
				,coalesce(cs.PricePerUnit, hist.PricePerUnit) as PricePerUnit
				,coalesce(cs.LineSubtotal, hist.LineSubtotal) as LineSubtotal
				,coalesce(cs.LineStatus, hist.LineStatus) as LineStatus
				,coalesce(cs.EXPORTDATETIME, hist.EXPORTDATETIME) as EXPORTDATETIME
				,coalesce(cs.ShippingMethod, hist.ShippingMethod) as ShippingMethod
				,coalesce(cs.ShipDate, hist.ShipDate) as ShipDate
				,coalesce(cs.TrackingNumber, hist.TrackingNumber) TrackingNumber
				,coalesce(cs.EpiOrderId, hist.EpiOrderId) EpiOrderId
				,coalesce(cs.EpiLineItemId, hist.EpiLineItemId) EpiLineItemId
		    ,case

				  -- refresh date if row first appears in current state table 
				  when hist.EpiLineItemId IS NULL then GETDATE()
				
					-- ...or if some key columns were updated
					when
					  hist.LineStatus != cs.LineStatus 
						OR (hist.LineStatus IS NULL AND cs.LineStatus IS NOT NULL)
						OR (hist.LineStatus IS NOT NULL AND cs.LineStatus IS NULL)
					then GETDATE()
					--WHEN hist.EXPORTDATETIME != cs.EXPORTDATETIME THEN GETDATE()  fix: this condition is always truem should be removed [Lyubov]
					when 
					  hist.ShipDate != cs.ShipDate
						OR (hist.ShipDate IS NULL AND cs.ShipDate IS NOT NULL)
						OR (hist.ShipDate IS NOT NULL AND cs.ShipDate IS NULL)
				  then GETDATE() 
					when
					  hist.TrackingNumber != cs.TrackingNumber 
						OR (hist.TrackingNumber IS NULL AND cs.TrackingNumber IS NOT NULL)
						OR (hist.TrackingNumber IS NOT NULL AND cs.TrackingNumber IS NULL)
					then GETDATE() 
				
					-- ...in rest cases leave it as is
					else hist.LastChangeDate
				end as LastChangeDate
		  into MGIC_LINEITEM_EXPORT
		  from MGIC_CS_LINEITEM_EXPORT cs
		  full outer join MGIC_LINEITEM_EXPORT_OLD hist ON cs.EpiLineItemId = hist.EpiLineItemId
			;

    END
	ELSE
	  BEGIN
			SELECT
			  *,
				GETDATE() AS LastChangeDate
			INTO MGIC_LINEITEM_EXPORT
			FROM MGIC_CS_LINEITEM_EXPORT
			;
		END


  
  SET @time_after = GETDATE();
  SET @tdiff = DATEDIFF(second, @time_before, @time_after);
  SET @records_after = (SELECT COUNT(*) FROM MGIC_LINEITEM_EXPORT);
  RAISERROR('..MGIC_LINEITEM_EXPORT updated, elapsed time: %d sec, %d to %d records', 10, 1, @tdiff, @records_before, @records_after) WITH NOWAIT
END
GO

-- =============================================

DROP PROCEDURE IF EXISTS Update_MGIC_ORDER_EXPORT;
GO
CREATE PROCEDURE Update_MGIC_ORDER_EXPORT
AS
DECLARE
  @time_before DATETIME,
  @time_after DATETIME,
  @tdiff [int],
  @records_before [int],
  @records_after [int];
BEGIN
  SET NOCOUNT ON;

  SET @time_before = GETDATE();
    SET @records_before = 0
      
  IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MGIC_ORDER_EXPORT'))
    BEGIN
       SET @records_before = (SELECT COUNT(*) FROM MGIC_ORDER_EXPORT);

			drop table if exists MGIC_ORDER_EXPORT_OLD;
			exec sp_rename 'MGIC_ORDER_EXPORT', 'MGIC_ORDER_EXPORT_OLD';

			select
				coalesce(cs.OrderNumber, hist.OrderNumber) as OrderNumber
				,coalesce(cs.ORDERNAME, hist.ORDERNAME) as ORDERNAME
				,coalesce(cs.CustomerName, hist.CustomerName) as CustomerName
				,coalesce(cs.Email, hist.Email) as Email
				,coalesce(cs.AccountId, hist.AccountId) as AccountId
				,coalesce(cs.SalesRegion, hist.SalesRegion) as SalesRegion
				,coalesce(cs.ContactId, hist.ContactId) as ContactId
				,coalesce(cs.Total, hist.Total) as Total
				,coalesce(cs.OrderDate, hist.OrderDate) as OrderDate
				,coalesce(cs.ORDERStatus, hist.ORDERStatus) as ORDERStatus
				,coalesce(cs.RecipientMessage, hist.RecipientMessage) as RecipientMessage
				,coalesce(cs.FirstName, hist.FirstName) as FirstName
				,coalesce(cs.LastName, hist.LastName) as LastName
				,coalesce(cs.RecipientName, hist.RecipientName) as RecipientName
				,coalesce(cs.CompanyName, hist.CompanyName) as CompanyName
				,coalesce(cs.Line1, hist.Line1) as Line1
				,coalesce(cs.Line2, hist.Line2) as Line2
				,coalesce(cs.City, hist.City) as City
				,coalesce(cs.State, hist.State) as State
				,coalesce(cs.Zip, hist.Zip) as Zip
				,coalesce(cs.ShippingMethod, hist.ShippingMethod) as ShippingMethod
				,coalesce(cs.Shipdate, hist.Shipdate) as Shipdate
				,coalesce(cs.EpiOrderId, hist.EpiOrderId) as EpiOrderId				
		    ,case

				  -- refresh date if row first appears in current state table 
				  when hist.EpiOrderId IS NULL then GETDATE()
				
					-- ...or if some key columns were updated
					when
					  hist.Orderstatus != cs.Orderstatus 
						OR (hist.Orderstatus IS NULL AND cs.Orderstatus IS NOT NULL)
						OR (hist.Orderstatus IS NOT NULL AND cs.Orderstatus IS NULL)
					then GETDATE()
					when 
					  hist.ShipDate != cs.ShipDate
						OR (hist.ShipDate IS NULL AND cs.ShipDate IS NOT NULL)
						OR (hist.ShipDate IS NOT NULL AND cs.ShipDate IS NULL)
				  then GETDATE()

					-- ...or if something has changed in related LineItems
					when lineitems.MaxLastChangeDate > hist.LastChangeDate
					then lineitems.MaxLastChangeDate
				
					-- ...in rest cases leave it as is
					ELSE hist.LastChangeDate
				END AS LastChangeDate
		  into MGIC_ORDER_EXPORT
		  from MGIC_CS_ORDER_EXPORT cs
		  full outer join MGIC_ORDER_EXPORT_OLD hist on cs.EpiOrderId = hist.EpiOrderId
			left join (
			  select
				  EpiOrderId,
					max(LastChangeDate) AS MaxLastChangeDate
				from MGIC_LINEITEM_EXPORT
				group by EpiOrderId
			) lineitems on lineitems.EpiOrderId = hist.EpiOrderId
			;

    END
	ELSE
	  BEGIN
			SELECT
			  *,
				GETDATE() AS LastChangeDate
			INTO MGIC_ORDER_EXPORT
			FROM MGIC_CS_ORDER_EXPORT			
			;		

		END
  
  SET @time_after = GETDATE();
  SET @tdiff = DATEDIFF(second, @time_before, @time_after);
  SET @records_after = (SELECT COUNT(*) FROM MGIC_ORDER_EXPORT);
  RAISERROR('..MGIC_ORDER_EXPORT updated, elapsed time: %d sec, %d to %d records', 10, 1, @tdiff, @records_before, @records_after) WITH NOWAIT
END
GO


-- =============================================

DROP PROCEDURE IF EXISTS Update_MGIC;
GO
CREATE PROCEDURE Update_MGIC
AS
DECLARE
  @time_before DATETIME,
  @time_after DATETIME,
  @tdiff [int],
  @records_before [int],
  @records_after [int];
BEGIN

  EXECUTE Update_MGIC_LINEITEM_EXPORT
  EXECUTE Update_MGIC_ORDER_EXPORT
  
END
GO


-- =============================================

EXECUTE Update_MGIC
GO