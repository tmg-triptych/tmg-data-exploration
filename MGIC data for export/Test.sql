----------------------
-- Check data state --
----------------------

--USE ExploreData
USE Stage_ExploreData

-- Check volumes
------------------

SELECT COUNT(*) FROM MGIC_CS_LINEITEM_EXPORT
SELECT COUNT(*) FROM MGIC_CS_ORDER_EXPORT
SELECT COUNT(*) FROM MGIC_LINEITEM_EXPORT
SELECT COUNT(*) FROM MGIC_ORDER_EXPORT
-- expected: history and current state counts match
-- if not, check duplicates in history


-- Currents State tables
--------------------------

-- Test all LineItem records are unique
SELECT * FROM (SELECT EpiLineItemId FROM MGIC_CS_LINEITEM_EXPORT GROUP BY EpiLineItemId HAVING COUNT(*) > 1) t;
-- 0 rows

-- Test all Order records are unique
SELECT * FROM (SELECT EpiOrderId FROM MGIC_CS_ORDER_EXPORT GROUP BY EpiOrderId HAVING COUNT(*) > 1) t;
-- 0 rows

-- History tables
-------------------

-- Test all LineItem records are unique
SELECT * FROM (SELECT EpiLineItemId FROM MGIC_LINEITEM_EXPORT GROUP BY EpiLineItemId HAVING COUNT(*) > 1) t;
-- 0 rows

-- Test all Order records are unique
SELECT * FROM (SELECT EpiOrderId FROM MGIC_ORDER_EXPORT GROUP BY EpiOrderId HAVING COUNT(*) > 1) t;
-- 0 rows
