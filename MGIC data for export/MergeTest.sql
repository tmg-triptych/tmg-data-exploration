-------------------------------
-- Compare stage & production 
-------------------------------
EXECUTE Stage_ExploreData..Rebuild_MGIC_CS;
EXECUTE ExploreData..Rebuild_MGIC_CS;

-- Order
USE Stage_ExploreData;
DROP TABLE IF EXISTS #Compare_MGIC_CS_ORDER_EXPORT;
SELECT
	COALESCE(new.EpiOrderId, old.EpiOrderId) AS EpiOrderId,
	CASE
	  WHEN new.EpiOrderId IS NULL THEN 'old only'
		WHEN old.EpiOrderId IS NULL THEN 'new only'
		ELSE 'both'
	END AS presented,
	CASE
	  WHEN COALESCE(new.ORDERNAME, '') != COALESCE(old.ORDERNAME, '') THEN 'ORDERNAME mismatch'
		WHEN COALESCE(new.CustomerName, '') != COALESCE(old.CustomerName, '') THEN 'CustomerName mismatch'
		WHEN COALESCE(new.Email, '') != COALESCE(old.Email, '') THEN 'Email mismatch'
		WHEN COALESCE(new.AccountId, '') != COALESCE(old.AccountId, '') THEN 'AccountId mismatch'
		WHEN COALESCE(new.SalesRegion, '') != COALESCE(old.SalesRegion, '') THEN 'SalesRegion mismatch'
		WHEN COALESCE(new.ContactId, '') != COALESCE(old.ContactId, '') THEN 'ContactId mismatch'
		WHEN COALESCE(new.Total, '') != COALESCE(old.Total, '') THEN 'Total mismatch'
		WHEN COALESCE(new.OrderDate, '') != COALESCE(old.OrderDate, '') THEN 'OrderDate mismatch'
		WHEN COALESCE(new.ORDERStatus, '') != COALESCE(old.ORDERStatus, '') THEN 'ORDERStatus mismatch'
		WHEN COALESCE(new.RecipientMessage, '') != COALESCE(old.RecipientMessage, '') THEN 'RecipientMessage mismatch'
		WHEN COALESCE(new.FirstName, '') != COALESCE(old.FirstName, '') THEN 'FirstName mismatch'
		WHEN COALESCE(new.LastName, '') != COALESCE(old.LastName, '') THEN 'LastName mismatch'
		WHEN COALESCE(new.RecipientName, '') != COALESCE(old.RecipientName, '') THEN 'RecipientName mismatch'
		WHEN COALESCE(new.CompanyName, '') != COALESCE(old.CompanyName, '') THEN 'CompanyName mismatch'
		WHEN COALESCE(new.Line1, '') != COALESCE(old.Line1, '') THEN 'Line1 mismatch'
		WHEN COALESCE(new.Line2, '') != COALESCE(old.Line2, '') THEN 'Line2 mismatch'
		WHEN COALESCE(new.City, '') != COALESCE(old.City, '') THEN 'City mismatch'
		WHEN COALESCE(new.State, '') != COALESCE(old.State, '') THEN 'State mismatch'
		WHEN COALESCE(new.Zip, '') != COALESCE(old.Zip, '') THEN 'Zip mismatch'
		WHEN COALESCE(new.ShippingMethod, '') != COALESCE(old.ShippingMethod, '') THEN 'ShippingMethod mismatch'
		WHEN COALESCE(new.Shipdate, '') != COALESCE(old.Shipdate, '') THEN 'Shipdate mismatch'
		ELSE 'all fields match'
	END AS fields_comparaison
INTO #Compare_MGIC_CS_ORDER_EXPORT
FROM Stage_ExploreData..MGIC_CS_ORDER_EXPORT new
LEFT JOIN ExploreData..MGIC_CS_ORDER_EXPORT old ON
	new.EpiOrderId = old.EpiOrderId
;

SELECT
  presented,
	COUNT(*)
FROM #Compare_MGIC_CS_ORDER_EXPORT
GROUP BY presented
ORDER BY presented
;

SELECT
	fields_comparaison,
	COUNT(*)
FROM #Compare_MGIC_CS_ORDER_EXPORT
WHERE presented = 'both'
GROUP BY fields_comparaison
ORDER BY fields_comparaison
;


--- LineItem

USE Stage_ExploreData;
DROP TABLE IF EXISTS #Compare_MGIC_CS_LINEITEM_EXPORT;
SELECT
	COALESCE(new.EpiLineItemId, old.EpiLineItemId) AS EpiLineItemId,
	CASE
	  WHEN new.EpiLineItemId IS NULL THEN 'old only'
		WHEN old.EpiLineItemId IS NULL THEN 'new only'
		ELSE 'both'
	END AS presented,
	CASE
	  WHEN COALESCE(new.OrderNumber, '') != COALESCE(old.OrderNumber, '') THEN 'OrderNumber mismatch'
		WHEN COALESCE(new.SKU, '') != COALESCE(old.SKU, '') THEN 'SKU mismatch'
		WHEN COALESCE(new.BaseSku, '') != COALESCE(old.BaseSku, '') THEN 'BaseSku mismatch'
		WHEN COALESCE(new.ItemNumberSKU, '') != COALESCE(old.ItemNumberSKU, '') THEN 'ItemNumberSKU mismatch'
		WHEN COALESCE(new.Name, '') != COALESCE(old.Name, '') THEN 'Name mismatch'
		WHEN COALESCE(new.Quantity, '') != COALESCE(old.Quantity, '') THEN 'Quantity mismatch'
		WHEN COALESCE(new.PricePerUnit, '') != COALESCE(old.PricePerUnit, '') THEN 'PricePerUnit mismatch'
		WHEN COALESCE(new.LineSubtotal, '') != COALESCE(old.LineSubtotal, '') THEN 'LineSubtotal mismatch'
		WHEN COALESCE(new.LineStatus, '') != COALESCE(old.LineStatus, '') THEN 'LineStatus mismatch'
		WHEN COALESCE(new.ShippingMethod, '') != COALESCE(old.ShippingMethod, '') THEN 'ShippingMethod mismatch'
		WHEN COALESCE(new.ShipDate, '') != COALESCE(old.ShipDate, '') THEN 'ShipDate mismatch'
		WHEN COALESCE(new.TrackingNumber, '') != COALESCE(old.TrackingNumber, '') THEN 'TrackingNumber mismatch'
		WHEN COALESCE(new.EpiOrderId, '') != COALESCE(old.EpiOrderId, '') THEN 'EpiOrderId mismatch'
		ELSE 'all fields match'
	END AS fields_comparaison
INTO #Compare_MGIC_CS_LINEITEM_EXPORT
FROM Stage_ExploreData..MGIC_CS_LINEITEM_EXPORT new
LEFT JOIN ExploreData..MGIC_CS_LINEITEM_EXPORT old ON
	new.EpiLineItemId = old.EpiLineItemId
;      

SELECT
  presented,
	COUNT(*)
FROM #Compare_MGIC_CS_LINEITEM_EXPORT
GROUP BY presented
ORDER BY presented
;

SELECT
	fields_comparaison,
	COUNT(*)
FROM #Compare_MGIC_CS_LINEITEM_EXPORT
WHERE presented = 'both'
GROUP BY fields_comparaison
ORDER BY fields_comparaison
;