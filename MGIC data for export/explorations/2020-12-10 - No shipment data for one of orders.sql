----------------------------
-- Problem: no order data --
----------------------------

-- good one
SELECT EpiOrderId, OrderNumber, FirstName, LastName FROM MGIC_CS_ORDER_EXPORT WHERE OrderNumber = '175-794529';
-- 794575	175-794529	David	Garcia

 -- bad one
SELECT EpiOrderId, OrderNumber, FirstName, LastName FROM MGIC_CS_ORDER_EXPORT WHERE OrderNumber = '175-795120';
-- 795133	175-795120	NULL	NULL

----------------------------
-- Problem: no order data --
----------------------------

-- calculation from SP
select
	oh.OrderTrackId as OrderNumber
	,oh.EpiOrderId
	,oih.EpiLineItemId
	,oss.Shipmentid
	,ss.ShippingAddressId
	,aa.FirstName as FirstName
	,aa.LastName as LastName
	,ss.FirstName as FirstName
	,ss.LastName as LastName
	,oga2.FirstName as FirstName
	,oga2.LastName as LastName
from dbMarekEF_production..OrderHistories oh with(nolock)
	join (
		select
			EpiLineItemId,
			EpiOrderId,
			row_number() OVER (PARTITION BY EpiOrderId ORDER BY EpiLineItemId) rn -- question: according this logic, we'll take FISRT lineitem for Shipments details, is it ok? [Lyubov]
		from dbMarekEF_production..OrderItemsHistories with(nolock)
	) oih on oh.EpiOrderId = oih.EpiOrderId --and oih.rn = 1
	left join dbMarekEF_production.[dbo].[OrderShipments] oss with(nolock) on oih.EpiLineItemID = oss.EpiLineItemId
	left join dbMarekEF_production.[dbo].[Shipments] ss with(nolock) on oss.Shipmentid = ss.id
	left join (
		select
			*
		from dbMarekCommerceManager_production..OrderGroupAddress oga2a with(nolock)
	)	as oga2 on oga2.Name = ss.ShippingAddressId and oh.EpiOrderId = oga2.OrderGroupID
	LEFT JOIN [dbMarekEF_production].[dbo].[Addresses] aa ON ss.ShippingAddressId  = aa.Id
where oh.OrderTrackId  = '175-795120'
;

-- still no first and last names for 175-795120


-- code from Jake: why shipping data is expected to be joined

select 'Firstitemline'AS firstitemlinetable,
		EpiLineItemId,
		EpiOrderId,
		row_number() OVER (PARTITION BY EpiOrderId ORDER BY EpiLineItemId) rn -- question: according this logic, we'll take FISRT lineitem for Shipments details, is it ok? [Lyubov]
	from dbMarekEF_production..OrderItemsHistories oih with(nolock) where oih.EpiOrderId = 795133

SELECT 'ordershipments'AS ordershipmentstable,* FROM [dbMarekEF_production].[dbo].[OrderShipments]oss  where oss.EpiLineItemId = 2005013
SELECT 'shipments'AS Shipmentstable, * FROM dbMarekEF_production.[dbo].[Shipments] ss with(nolock) where ss.id = 335829

SELECT 
	MIN(OrderGroupAddressId) as OrderGroupAddressId	,OrderGroupId
--,Name
FROM dbMarekCommerceManager_production..OrderGroupAddress oga2 WITH(NOLOCK)
where ordergroupid IN ('795133')
GROUP BY OrderGroupId


select 'ordergroupaddress'AS ordergroupaddrestable,* from dbMarekCommerceManager_production..OrderGroupAddress oga2a with(nolock) where oga2a.ordergroupaddressid = 1403300 --here is missing firstname, lastname

select 'Firstitemline'AS firstitemlinetable,epiOrderid, shipmentid, row_number() over (partition by epiOrderid order by shipmentid) as rn
FROM [dbMarekEF_production].[dbo].[OrderShipments] os1 WITH(NOLOCK) 
where os1.epiorderid = 795133-- and os1.rn = 1


SELECT 'ordershipments'AS ordershipmentstable, ShippingAddressId, FirstName, LastName FROM dbMarekEF_production.[dbo].[Shipments] ss1  WITH(NOLOCK)   where  ss1.id = 335829
SELECT * FROM dbMarekCommerceManager_production..OrderGroupAddress oga2 where oga2.OrderGroupID = 795133 and Name = 'd5a60f89-34b7-4566-b500-6fabcab7a999';
SELECT 'addresshipments'AS addresshipmentsstable,Id, FirstName, LastName FROM [dbMarekEF_production].[dbo].[Addresses] aa WITH(NOLOCK) where aa.Id='d5a60f89-34b7-4566-b500-6fabcab7a999'