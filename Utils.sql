USE DWH_CurrentStateLayer;
 
USE DWH_Stage_CurrentStateLayer;

-- GET ALL TABLES
SELECT TABLE_NAME 
FROM DWH_Stage_CurrentStateLayer.INFORMATION_SCHEMA.TABLES 
WHERE TABLE_TYPE = 'BASE TABLE' ORDER BY TABLE_NAME


DROP TABLE Campaign_Wright;
DROP TABLE CS_AGG_Order;
DROP TABLE CS_AGG_UserActivity;
DROP TABLE CS_AGG_VariationOrder;
DROP TABLE CS_Campaign_Wright;
DROP TABLE CS_Catalog;
DROP TABLE CS_CoopAdjustments_Wright;
DROP TABLE CS_CoopBalance;
DROP TABLE CS_CoopBalance_tmp01;
DROP TABLE CS_CoopLog;
DROP TABLE CS_ExcludeEmailList;
DROP TABLE CS_Kit;
DROP TABLE CS_Login;
DROP TABLE CS_Order;
DROP TABLE CS_OrderLineItem;
DROP TABLE CS_Payment;
DROP TABLE CS_Payment_Masonite;
DROP TABLE CS_Product;
DROP TABLE CS_ProductAvailability;
DROP TABLE CS_ProductAvailability_tmp01_AllEvents;
DROP TABLE CS_ProductAvailabilityForRole;
DROP TABLE CS_ProductLive_ActivityPeriods;
DROP TABLE CS_ProductOrder;
DROP TABLE CS_ProductOrder_Wright;
DROP TABLE CS_ProductVariantSelection;
DROP TABLE CS_ProductVersion;
DROP TABLE CS_Roles;
DROP TABLE CS_Site;
DROP TABLE CS_SiteCatalog;
DROP TABLE CS_SiteRoles;
DROP TABLE CS_User;
DROP TABLE CS_User_CustomMetaData_MASONITEDATA;
DROP TABLE CS_User_Masonite;
DROP TABLE CS_UserCatalog;
DROP TABLE CS_UserRoles;
DROP TABLE CS_Variation;
DROP TABLE CS_VariationAvailability;
DROP TABLE CS_VariationAvailability_tmp01_AllEvents;
DROP TABLE CS_VariationProduct;
DROP TABLE CS_VariationVersion;
DROP TABLE CS_Wright_OptOutUsers;
DROP TABLE ProductAvailability_tmp01_AllEvents;
DROP TABLE ProductLive_GeneralActivityPeriods;
DROP TABLE Slice_10mln;
DROP TABLE Slice_1mln;
DROP TABLE TABLE1;
DROP TABLE Test;
DROP TABLE TestPartitioning;


-- GET ALL SP 
SELECT *  FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' ORDEr BY SPECIFIC_NAME;


DROP PROCEDURE Rebuild_CS_AGG_Order
DROP PROCEDURE Rebuild_CS_AGG_UserActivity
DROP PROCEDURE Rebuild_CS_AGG_VariationOrder
DROP PROCEDURE Rebuild_CS_Campaign_Wright
DROP PROCEDURE Rebuild_CS_Catalog
DROP PROCEDURE Rebuild_CS_CoopAdjustments_Wright
DROP PROCEDURE Rebuild_CS_CoopBalance
DROP PROCEDURE Rebuild_CS_CoopLog
DROP PROCEDURE Rebuild_CS_ExcludeEmailList
DROP PROCEDURE Rebuild_CS_Kit
DROP PROCEDURE Rebuild_CS_Login
DROP PROCEDURE Rebuild_CS_Order
DROP PROCEDURE Rebuild_CS_OrderLineItem
DROP PROCEDURE Rebuild_CS_Payment
DROP PROCEDURE Rebuild_CS_Payment_Masonite
DROP PROCEDURE Rebuild_CS_Product
DROP PROCEDURE Rebuild_CS_ProductAvailability
DROP PROCEDURE Rebuild_CS_ProductAvailability_tmp01_AllEvents
DROP PROCEDURE Rebuild_CS_ProductAvailabilityForRole
DROP PROCEDURE Rebuild_CS_ProductLive_ActivityPeriods
DROP PROCEDURE Rebuild_CS_ProductOrder
DROP PROCEDURE Rebuild_CS_ProductVariantSelection
DROP PROCEDURE Rebuild_CS_ProductVersion
DROP PROCEDURE Rebuild_CS_Roles
DROP PROCEDURE Rebuild_CS_Site
DROP PROCEDURE Rebuild_CS_SiteCatalog
DROP PROCEDURE Rebuild_CS_SiteRoles
DROP PROCEDURE Rebuild_CS_User
DROP PROCEDURE Rebuild_CS_User_CustomMetaData_MASONITEDATA
DROP PROCEDURE Rebuild_CS_User_Masonite
DROP PROCEDURE Rebuild_CS_UserCatalog
DROP PROCEDURE Rebuild_CS_UserRoles
DROP PROCEDURE Rebuild_CS_Variation
DROP PROCEDURE Rebuild_CS_VariationAvailability
DROP PROCEDURE Rebuild_CS_VariationAvailability_tmp01_AllEvents
DROP PROCEDURE Rebuild_CS_VariationAvailabilityForRole
DROP PROCEDURE Rebuild_CS_VariationProduct
DROP PROCEDURE Rebuild_CS_VariationVersion
DROP PROCEDURE Rebuild_CS_Wright_OptOutUsers
Test_CurrentStateLayer_table_pk


-- Sites and Catalogs
SELECT
  s.SiteId,
	s.SiteName,
	c.CatalogId,
	c.CatalogName
FROM DWH_Stage_CurrentStateLayer..CS_SiteCatalog sc
LEFT JOIN DWH_Stage_CurrentStateLayer..CS_Site s ON s.SiteId = sc.SiteId
LEFT JOIN DWH_Stage_CurrentStateLayer..CS_Catalog c ON c.CatalogId = sc.CatalogId
ORDER BY c.CatalogId
;

7	Kohler	2	Kohler
73	CaseIH	4	Case IH
482	KHS	6	KHS
547	TestSite	8	TestSite
1049	Marek	16	Marek
16087	Employee	16	Marek
72029	ASQ	16	Marek
1102	ATI Gunstocks	17	ATI Gunstocks
1125	Generac Printshop	18	Generac Printshop
1653	Ariat	19	ariat
1923	FIS	20	FIS
1992	AEM Safety	31	AEM Safety
10283	AEM Store	31	AEM Safety
2549	Tomahawks	33	Kohler Rebates
2584	Kaiser	34	Kaiser
2641	AEM MOD	35	AEM MOD
3286	InPRO	36	InPRO
3356	CooperVision	37	CooperVision
3453	UHC EI	38	UHC EI
3483	BCBSMN	39	BCBSMN
12901	BCBSMN-CM	39	BCBSMN
3626	New Holland	40	New Holland
3689	CIS Generac MOD	42	Dealer Generac MOD
3713	Dealer Generac MOD	42	Dealer Generac MOD
3753	Mobile Generac MOD	42	Dealer Generac MOD
3784	Wholesale Generac MOD	42	Dealer Generac MOD
3817	Latam Generac MOD	42	Dealer Generac MOD
3853	Retail Generac MOD	42	Dealer Generac MOD
3883	Industrial Generac MOD	42	Dealer Generac MOD
3908	Honeywell Generac MOD	42	Dealer Generac MOD
12959	Service Training Generac MOD	42	Dealer Generac MOD
4794	GElabels	50	GElabels
4960	Wine Group	52	Wine Group
5700	NM Print	53	NM Print
7636	Bausch	55	Bausch
8094	UHC	56	UHC
11682	AEM MOD CECA	59	AEM MOD CECA
11756	NMNetwork Store	60	NMNetwork Store
17988	AEM WOA	66	AEM WOA
18654	Fastenal MOD	67	Fastenal MOD
19270	Marek Clients	68	Marek Clients
19471	UHC Global	69	UHC Global
19580	Russell	70	Russell
19633	AGG1	71	AGG1
20070	MyVisitor	72	MyVisitor
24799	AmFam Maui	73	AmFam Maui
24856	Titan Tires	74	Titan Tires
26257	GE Healthcare	75	GE Healthcare
26435	Channel Resources	76	Channel Resources
26555	YMCA Marketplace	77	YMCA Marketplace
27670	Team Groupie	78	Team Groupie
28709	UHG Employer Brand Central	79	UHG Employer Brand Central
29074	Noodles	80	Noodles
29291	TWG Vault	81	TWG Vault
29482	UHC Group Retiree Marketing	82	UHC Group Retiree Marketing
31820	Masonite	83	Masonite
32724	Uptown Employee Rewards	84	Uptown Employee Rewards
36035	SAP Concur CMAST	85	SAP Concur CMAST
36184	RDO Market Ready	86	RDO Market Ready
39500	Speed Queen	87	Speed Queen
41127	Huebsch	87	Speed Queen
41149	UniMac	87	Speed Queen
94288	Primus	87	Speed Queen
42715	Triptych Thanks	90	Triptych Thanks
43464	Bruno	91	Bruno
43776	Masonite Arch	92	Masonite Arch
44146	AEM CELA	93	AEM CELA
45026	Advisar	94	Advisar
45757	Baird	95	Baird
46424	Wright MOD	96	Wright MOD
46627	Broan	97	Broan
46766	Speed Queen Shop	98	Speed Queen Shop
46788	Huebsch Shop	99	Huebsch Shop
49274	ABB	100	ABB
50192	Wacker Neuson	101	Wacker Neuson
52018	Titan Tires Gives	102	Titan Tires Gives
54694	Blackhawk	103	Blackhawk
54965	Franchise MOD	104	Franchise MOD
63880	Combined MOD	105	Combined MOD
107456	Combined MOD SP	105	Combined MOD
67853	Batteries Plus	106	Batteries Plus
72135	HH Global	108	HH Global
73154	Caleffi	109	Caleffi
76437	Covid19 Store	110	Covid19 Store
79119	Profile Health	111	Profile Health
81233	MPS	112	MPS
89115	Sandbox1	113	Sandbox1
90380	UHC Dental TX	114	UHC Dental TX
93345	MGIC	115	MGIC
105945	United Way GMWC	117	United Way GMWC
111273	Project Energy Savers	119	Project Energy Savers