USE DWH_Stage_CurrentStateLayer
GO
  -- Generate events: new product version creation
  DROP TABLE IF EXISTS #ProductLive_Step01_VersionAppearedEvents;
  SELECT
    -- event details
    ver.ObjectId AS Product_ObjectId,
    ver.WorkId AS Version_WorkId,
    ver.Created AS EventDate,
    -- product type
    ce.ClassTypeId,
    ce.CatalogId,
    -- product version properties
    is_publ.Boolean AS IsPublished,
    is_unpubl.Date AS ExpirationDate,
    impstat.Number AS ImplementationStatus,
    is_retired.Boolean AS IsRetired,
    deny_access.LongString AS DenyAccess,
    allow_access.LongString AS AllowAccess,
    1 AS EventType,                   -- 1 = product version created
    ver.WorkId AS ActualVersion_WorkId
  INTO #ProductLive_Step01_VersionAppearedEvents
  FROM dbMarekCommerceManager_production..CatalogEntry ce
  LEFT JOIN dbMarekCommerceManager_production..ecfVersion ver ON
    ce.CatalogEntryId = ver.ObjectId
  LEFT JOIN dbMarekCommerceManager_production..ecfVersionProperty is_publ ON
    is_publ.WorkId = ver.WorkId AND
    is_publ.MetaFieldName = 'Epi_IsPublished'
  LEFT JOIN dbMarekCommerceManager_production..ecfVersionProperty is_unpubl ON
    is_unpubl.WorkId = ver.WorkId AND
    is_unpubl.MetaFieldName = 'Epi_StopPublish'
  LEFT JOIN dbMarekCommerceManager_production..ecfVersionProperty impstat ON
    impstat.WorkId = ver.WorkId AND
    impstat.MetaFieldName = 'ImplementationStatus'
  LEFT JOIN dbMarekCommerceManager_production..ecfVersionProperty is_retired ON
    is_retired.WorkId = ver.WorkId AND
    is_retired.MetaFieldName = 'IsRetired'
  LEFT JOIN dbMarekCommerceManager_production..ecfVersionProperty deny_access ON
    deny_access.WorkId = ver.WorkId AND
    deny_access.MetaFieldName = 'DenyAccess'
  LEFT JOIN dbMarekCommerceManager_production..ecfVersionProperty allow_access ON
    allow_access.WorkId = ver.WorkId AND
    allow_access.MetaFieldName = 'AllowAccess'
  ORDER BY ver.Created DESC
  ;

SELECT * FROM #ProductLive_Step01_VersionAppearedEvents WHERE Product_ObjectId IN (1054, 1103, 38151) ORDER BY Product_ObjectId, EventDate DESC, Version_WorkId DESC;
SELECT * FROM DWH_Stage_CurrentStateLayer..ProductLive_GeneralActivityPeriods WHERE Product_ObjectId IN (1054, 1103, 38151) ORDER BY Product_ObjectId, PeriodStartDate DESC;


