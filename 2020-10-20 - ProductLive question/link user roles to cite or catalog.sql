-- goal: take catalog-tied events and make cross-product with all customer's user roles
-- have:
-- - catalog-tied events
-- - site-catalog mapping
-- need: list of all user roles with access to catalog/site


-- Site-Catalog connection
SELECT
  cp.fkContentID,          -- StartPage pk in tblContent
  -- Site data
  sd.StartPage as site_id, -- same, nut with the name how it's called in views usually
  sd.Name as site_name,
  -- how it's connected to catalog
  cp.LinkGuid,
  -- Catalog data
  c.CatalogId as catalog_id,
  c.Name as catalog_name  
FROM dbMarekCMS_production..tblSiteDefinition sd               -- sites
INNER JOIN dbMarekCMS_production..tblContentProperty cp        -- site 'CommerceCatalog' property
  ON sd.StartPage = cp.fkContentId AND
  cp.fkPropertyDefinitionId IN (SELECT pkID FROM dbMarekCMS_production..tblPropertyDefinition WHERE Name = 'CommerceCatalog')  
INNER JOIN dbMarekCommerceManager_production..Catalog c on cp.LinkGuid = c.ContentGuid -- catalog
ORDER BY 
-- sd.Name,
c.Name
;
-- for tests: Speed Queen catalog shared by many sites
-- 39500	39500	Speed Queen     	01E89724-E566-49B1-9E79-C5B8D885D2E6	87	Speed Queen
-- 41127	41127	Huebsch	            01E89724-E566-49B1-9E79-C5B8D885D2E6	87	Speed Queen
-- 41149	41149	UniMac	            01E89724-E566-49B1-9E79-C5B8D885D2E6	87	Speed Queen
-- 94288	94288	Primus	            01E89724-E566-49B1-9E79-C5B8D885D2E6	87	Speed Queen
-- 46766	46766	Speed Queen Shop	C9081C85-EFBE-44A7-91D8-1DEA6792BEB1	98	Speed Queen Shop

SELECT
  ur.fkContentId,
  ct.Name ContentType,
  ur.LongString as UserRoles
FROM (
	SELECT
	  *
	FROM dbMarekCMS_production..tblContentProperty cp
	WHERE cp.fkPropertyDefinitionId IN (SELECT pkID FROM dbMarekCMS_production..tblPropertyDefinition WHERE Name = 'SiteRoles')
) ur
LEFT JOIN dbMarekCMS_production..tblContent c on c.pkID = ur.fkContentId
LEFT JOIN dbMarekCMS_production..tblContentType ct ON ct.pkID = c.fkContentTypeID
LEFT JOIN dbMarekCMS_production..tblContent c_parent on c_parent.pkID = c.fkParentID

WHERE ct.Name = 'StartPage'
ORDER BY ur.LongString
;

SELECT
  SiteId, 
  value AS UserRole
FROM (
SELECT
  sd.StartPage as SiteId,
  cp1.LongString as UserRoles
FROM (
  SELECT
    *
  FROM dbMarekCMS_production..tblContent c
  WHERE c.fkContentTypeID IN (SELECT pkID FROM dbMarekCMS_production..tblContentType WHERE Name = 'StartPage')
) start_page
INNER JOIN dbMarekCMS_production..tblContentProperty cp1 ON 
  start_page.pkID = cp1.fkContentId AND 
  cp1.fkPropertyDefinitionId IN (SELECT pkID FROM dbMarekCMS_production..tblPropertyDefinition WHERE Name = 'SiteRoles')
INNER JOIN dbMarekCMS_production..tblSiteDefinition sd ON
  sd.StartPage = start_page.pkID
) t  
  CROSS APPLY STRING_SPLIT(UserRoles, ',');  

