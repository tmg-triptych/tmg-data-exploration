USE [dbMarekCommerceManager_production]
GO

/****** Object:  View [dbo].[vw_Elasticube_Product_Info_v2_5]    Script Date: 25.09.2020 2:04:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_Elasticube_Product_Info_v2_5] AS 
select 
  distinct AllowAccess, 
  CatalogEntryId, 
  CatalogId, 
  CatalogName, 
  CreatedDate, 
  DenyAccess, 
  expiredate, 
  FuseDescription, 
  HideOnSearch, 
  ImplementationStatus, 
  KeyWordsMetadata, 
  LastPublishDate, 
  ManagementRating, 
  materialtype, 
  MeasureUnit, 
  MeasureUnitValue, 
  name, 
  ProductType, 
  publishdate, 
  Revision, 
  ShopValues, 
  ShortDescription, 
  sku, 
  ToolTypeName, 
  VariantType, 
  Variations, 
  Vendor, 
  VersioningMetadata, 
  URLName, 
  ParentEntryId, 
  Code, 
  IsRetired,
  ClassTypeId,
  CASE
    -- Conditions when Product or Variation can be inactive
    WHEN ClassTypeId IN ('Product', 'Variation') AND PublishDate IS NULL THEN 'No'
    WHEN ClassTypeId IN ('Product', 'Variation') AND ExpireDate < CAST(GETDATE() AS DATE) THEN 'No'
    -- Conditions when Product can be inactive
	  WHEN ClassTypeId = 'Product' AND IsRetired = 'True'  THEN 'No'
    WHEN ClassTypeId = 'Product' AND (ImplementationStatus != 'Live') THEN 'No' 
    -- Rest cases 
    WHEN ClassTypeId IN ('Product', 'Variation') THEN 'Yes'
    WHEN ClassTypeId = 'Package' THEN 'NA'
  END AS IsActive
from 
  (
    select 
      c.Name as CatalogName, 
      ce.CatalogEntryId, 
      ce.CatalogId, 
      cer.ParentEntryId as ParentEntryId, 
      ce.name, 
      ccp.MetaFieldName, 
      ccp.LongString, 
      ce.startdate as publishdate, 
      ce.EndDate as [expiredate], 
      mc.FriendlyName as ProductType, 
      rat.Number as ManagementRating, 
      cx.Created as CreatedDate, 
      cx.Modified as LastPublishDate, 
      ce.code, 
      (
        case 
          when ce.MetaClassId = 1035 then 'Both' 
          when ce.[MetaClassId] in (1044, 30)  and ce.[MetaClassId] not in (1029, 1043) then 'Print'
          when ce.[MetaClassId] not in (1044, 30)  and ce.[MetaClassId] in (1029, 1043) then 'Download'
          else ''
        end
      ) as VariantType, 
      (
        case 
          when imp.Number = 1 then 'Configuring' 
          when imp.Number = 2 then 'Testing' 
          when imp.Number = 3 then 'Approved' 
          when imp.Number = 4 then 'Live' 
          else ''
        end
      ) as ImplementationStatus, 
      (
        case when sea.Boolean = 1 then 'Yes' else 'No' end
      ) as HideOnSearch, 
      cie.UriSegment as URLName, 
      (
        case when ret.Boolean = 1 then 'True' else 'False' end
      ) as IsRetired, 
      stuff(
        (
          select 
            distinct ' | ' + sub.Name + ':' + cn.Name 
          from[dbMarekCommerceManager_production].dbo.[CatalogNode] cn with(nolock) left join[dbMarekCommerceManager_production].dbo.[CatalogNode] sub with(nolock) on cn.ParentNodeId = sub.CatalogNodeId 
          left join NodeEntryRelation ner with(nolock) on ner.CatalogNodeId = cn.CatalogNodeId 
          where 
            isnull(sub.name, '') != '' 
            and isnull(cn.name, '')!= '' 
            and ce.CatalogEntryId = ner.CatalogEntryId FOR XML PATH('')
        ), 
        1, 
        3, 
        ''
      ) as ShopValues ,
      ce.ClassTypeId
    from 
      dbMarekCommerceManager_production.[dbo].[CatalogEntry] ce with(nolock) 
      left join dbMarekCommerceManager_production..CatalogEntryRelation cer with(nolock) on ce.CatalogEntryId = cer.ChildEntryId
      left join dbMarekCommerceManager_production.[dbo].[Catalog] c with(nolock) on c.CatalogId = ce.CatalogId 
      left join dbMarekCommerceManager_production.[dbo].[CatalogContentProperty] ccp with(nolock) on ce.CatalogEntryId = ccp.ObjectId AND ce.MetaClassId = ccp.MetaClassId
      left join dbMarekCommerceManager_production.[dbo].[CatalogContentProperty] hid with(nolock) on ce.CatalogEntryId = hid.ObjectId 
        and hid.MetaFieldName = 'PriceIsHidden'
        AND ce.MetaClassId = hid.MetaClassId
      left join dbMarekCommerceManager_production.[dbo].[CatalogContentProperty] rat with(nolock) on ce.CatalogEntryId = rat.ObjectId 
        and rat.MetaFieldName = 'Rating'
        AND ce.MetaClassId = rat.MetaClassId
      left join dbMarekCommerceManager_production.[dbo].[CatalogContentProperty] app with(nolock) on ce.CatalogEntryId = app.ObjectId 
        and app.MetaFieldName = 'ApprovalType' 
        and app.LongString != '00000000-0000-0000-0000-000000000000' 
        and isnull(app.LongString, '')!= '' 
        AND ce.MetaClassId = app.MetaClassId
      left join dbMarekCommerceManager_production.[dbo].[CatalogContentProperty] imp with(nolock) on ce.CatalogEntryId = imp.ObjectId 
        and imp.MetaFieldName = 'ImplementationStatus' 
        AND ce.MetaClassId = imp.MetaClassId
      left join dbMarekCommerceManager_production.[dbo].[CatalogContentProperty] sea with(nolock) on ce.CatalogEntryId = sea.ObjectId 
        and sea.MetaFieldName = 'HideOnSearch' 
        AND ce.MetaClassId = sea.MetaClassId
      left join dbMarekCommerceManager_production.[dbo].[CatalogContentProperty] ret with(nolock) on ce.CatalogEntryId = ret.ObjectId 
        and ret.MetaFieldName = 'IsRetired' 
        AND ret.MetaClassId = sea.MetaClassId
      left join dbMarekCommerceManager_production.[dbo].[MetaClass] mc with(nolock) on ce.MetaClassId = mc.MetaClassId 
      left join dbMarekCommerceManager_production..[CatalogItemSeo] cie with(nolock) on cie.CatalogEntryId = ce.CatalogEntryId 
      left join dbMarekCommerceManager_production.[dbo].[CatalogContentEx] cx with(nolock) on ce.CatalogEntryId = cx.ObjectId 
      left join (
        select 
          distinct nr.CatalogEntryId, 
          stuff(
            (
              select 
                distinct ' | ' + replace(cni.name, char(38), '&') + ',' + subi.Name 
              from [dbMarekCommerceManager_production].dbo.[CatalogNode] cni with(nolock) 
              left join [dbMarekCommerceManager_production].dbo.[CatalogNode] subi with(nolock) on cni.CatalogNodeId = subi.ParentNodeId 
              left join [dbMarekCommerceManager_production]..NodeEntryRelation neri with(nolock) on cni.CatalogNodeId = neri.CatalogNodeId 
              where 
                nr.CatalogEntryId = neri.CatalogEntryId FOR XML PATH('')
            ), 
            1, 
            3, 
            ''
          ) as ShopCategory 
        from NodeEntryRelation nr with(nolock) 
        left join (
          select 
            cn.CatalogNodeId, 
            cn.Name as Shop, 
            sub.Name as SubShop 
          from [dbMarekCommerceManager_production].dbo.[CatalogNode] cn with(nolock) 
          left join [dbMarekCommerceManager_production].dbo.[CatalogNode] sub with(nolock) on cn.CatalogNodeId = sub.ParentNodeId
        ) as shops on nr.CatalogNodeId = shops.CatalogNodeId
      ) as shops on ce.CatalogEntryId = shops.CatalogEntryId 
    where 
      ce.CatalogId != 7
  ) as piv PIVOT (
    min(piv.Longstring) for piv.MetaFieldName in (
      [sku], ShortDescription, ContentDescription, 
      Revision, Variations, MetaInformation, 
      ToolTypeName, QuantitiesInfo, MeasureUnit, 
      MeasureUnitValue, Vendor, materialtype, 
      ExpirationDateForNewStatus, EngagementSettings, 
      AllowAccess, DenyAccess, KeyWordsMetadata, 
      DownloadFileTypes, FuseDescription, 
      PdfFile, PdfPrintFile, EmailPdfFile, 
      FilingTemplateId, VersioningMetadata, 
      AllowDataAcquisitionRoles
    )
  ) as ds



GO


