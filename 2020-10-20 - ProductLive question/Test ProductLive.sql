USE DWH_Stage_CurrentStateLayer;

----------------------
-- Functional tests --
----------------------
-- Check product versions
SELECT * FROM DWH_Stage_CurrentStateLayer..CS_ProductVersion WHERE ProductId IN (13082, 65264, 1131, 48831, 70448, 2691)  ORDER BY ProductId, CreateDate DESC, ProductVersionId DESC;
-- Check events generated for them
SELECT * FROM DWH_Stage_CurrentStateLayer..CS_ProductAvailability_tmp01_AllEvents WHERE ProductId IN (13082, 65264, 1131, 48831, 70448, 2691)  ORDER BY ProductId, EventDate DESC, ProductVersionId DESC, EventType DESC;
-- Check availability periods
SELECT * FROM DWH_Stage_CurrentStateLayer..CS_ProductAvailability WHERE ProductId IN (13082, 65264, 1131, 48831, 70448, 2691) ORDER BY ProductId DESC, PeriodStartDate DESC;
-- Check availability per roles
SELECT * FROM DWH_Stage_CurrentStateLayer..CS_ProductAvailabilityForRole WHERE ProductId IN (13082, 65264, 1131, 48831, 70448, 2691) ORDER BY ProductId, UserRole DESC, PeriodStartDate DESC;


---------------------------------------------------
-- Availability per role calculation performance --
---------------------------------------------------
-- Catalog roles --
   SELECT COUNT(*) FROM (
    SELECT DISTINCT
      c.CatalogId,
	    sr.UserRole
    FROM CS_Catalog c
    LEFT JOIN CS_SiteCatalog sc ON
      c.CatalogId = sc.CatalogId
    LEFT JOIN CS_SiteRoles sr ON
      sc.SiteId = sr.SiteId
	) r
	 -- 2 513

	SELECT CatalogId, COUNT(*) FROM (
    SELECT DISTINCT
      c.CatalogId,
	  sr.UserRole
    FROM CS_Catalog c
    LEFT JOIN CS_SiteCatalog sc ON
      c.CatalogId = sc.CatalogId
    LEFT JOIN CS_SiteRoles sr ON
      sc.SiteId = sr.SiteId
	) r
	GROUP BY CatalogId
	ORDER BY COUNT(*) DESC
	;
	-- 56	280
    -- 42	260
    -- 39	187
    -- 87	115
    -- 83	71
    -- 16	62
    -- 69	53
	-- ...

-- Events --
SELECT
	COUNT(*)
FROM CS_ProductAvailability_tmp01_AllEvents e 
-- 254 272
		
SELECT IsActive, COUNT(*) FROM #ProductAvailability_StepA05_AllEvents GROUP BY IsActive;
-- No	     95 083
-- Yes	159 189
DROP TABLE IF EXISTS #ProductAvailability_StepA05_AllEvents;
SELECT
*,
CASE
    WHEN IsPublished IS NULL OR IsPublished != 1 THEN 'No'
    WHEN ExpirationDate <= EventDate THEN 'No'
    WHEN IsRetired = 'True'  THEN 'No'
    WHEN EventDate >= CONVERT(DATETIME, '2017-08-22 08:18:00.080', 20) AND ImplementationStatus IS NULL OR ImplementationStatus != 4 THEN 'No' 
    ELSE 'Yes'
END AS IsActive
INTO #ProductAvailability_StepA05_AllEvents
FROM CS_ProductAvailability_tmp01_AllEvents
;

SELECT CatalogId, COUNT(*) FROM #ProductAvailability_StepA05_AllEvents GROUP BY CatalogId ORDER BY COUNT(*) DESC;
-- 56	66198
-- 60	21953
-- 69	21834
-- 42	18426
-- 79	12474
-- 34	10341


-- Events x Roles --
SELECT
COUNT(*)
FROM CS_ProductAvailability_tmp01_AllEvents e
LEFT JOIN (
SELECT DISTINCT
    c.CatalogId,
    sr.UserRole
FROM CS_Catalog c
LEFT JOIN CS_SiteCatalog sc ON
    c.CatalogId = sc.CatalogId
LEFT JOIN CS_SiteRoles sr ON
    sc.SiteId = sr.SiteId
) cr ON 
cr.CatalogId = e.CatalogId
;
-- 30 534 419

SELECT
cr.CatalogId, COUNT(*)
FROM CS_ProductAvailability_tmp01_AllEvents e
LEFT JOIN (
SELECT DISTINCT
    c.CatalogId,
    sr.UserRole
FROM CS_Catalog c
LEFT JOIN CS_SiteCatalog sc ON
    c.CatalogId = sc.CatalogId
LEFT JOIN CS_SiteRoles sr ON
    sc.SiteId = sr.SiteId
) cr ON 
cr.CatalogId = e.CatalogId
GROUP BY cr.CatalogId
ORDEr BY COUNT(*) DESC
;
-- 56	18 535 440 -- UHC
-- 42	4 790 760  -- Dealer Generac MOD
-- 39	1 496 748  -- BCBSMN
-- 69	1 157 202  -- UHC Global

-- 87	  875 840  -- Speed Queen
-- 60	  636 637  -- NMNetwork Store
-- ...
  

-- How widely roles are used per top-4
SELECT
  CatalogId,
  COUNT(*),
  COUNT(AllowAccess),
  COUNT(DenyAccess)
FROM CS_ProductAvailability_tmp01_AllEvents
WHERE CatalogId IN (56, 42, 39, 69)
GROUP BY CatalogId
;
-- 39	8005	4544	5766
-- 42	18426	720	17724
-- 56	66342	63137	17062
-- 69	21834	20331	16

------------------------
-- Performance tuning --
------------------------
  DROP TABLE IF EXISTS #ProductAvailability_StepB05_AllEvents;
  SELECT
    e.ProductId,
  	e.ProductVersionId,
  	e.EventDate,
	  e.CatalogId, 
		e.EventType,
    cr.UserRole,
    CASE
      WHEN e.IsPublished IS NULL OR e.IsPublished != 1 THEN 'No'
      WHEN e.ExpirationDate <= e.EventDate THEN 'No'
      WHEN e.IsRetired = 'True'  THEN 'No'
      WHEN e.EventDate >= CONVERT(DATETIME, '2017-08-22 08:18:00.080', 20) AND e.ImplementationStatus IS NULL OR e.ImplementationStatus != 4 THEN 'No' 
      WHEN e.AllowAccess IS NOT NULL AND COALESCE(CHARINDEX(cr.UserRole, e.AllowAccess), 0) = 0 THEN 'No'
      WHEN e.DenyAccess IS NOT NULL AND COALESCE(CHARINDEX(cr.UserRole, e.AllowAccess), 0) != 0 THEN 'No'
      ELSE 'Yes'
    END AS IsActive,
		CASE
		  WHEN e.CatalogId = 56 THEN 0 -- UHC
			WHEN e.CatalogId = 42 THEN 1 -- Dealer Generac MOD
			WHEN e.CatalogId = 39 THEN 2 -- BCBSMN
			WHEN e.CatalogId = 69 THEN 3 -- UHC Global
			ELSE 4                       -- OTHERS
		END AS PartitionId 
  INTO #ProductAvailability_StepB05_AllEvents
  FROM CS_ProductAvailability_tmp01_AllEvents e
  LEFT JOIN (
    SELECT
      c.CatalogId,
      sr.UserRole
    FROM CS_Catalog c
    LEFT JOIN CS_SiteCatalog sc ON
      c.CatalogId = sc.CatalogId
    LEFT JOIN CS_SiteRoles sr ON
      sc.SiteId = sr.SiteId
  ) cr ON 
    cr.CatalogId = e.CatalogId
  ;
	-- 1:24
  SELECT COUNT(*) FROM #ProductAvailability_StepB05_AllEvents;
	-- 30 710 704

	--------------------------------
	-- Performance testing: indexes
	--------------------------------

	DROP TABLE IF EXISTS Slice_1mln;
	SELECT TOP 1000000 * INTO Slice_1mln FROM #ProductAvailability_StepB05_AllEvents;                     																																																				 
																																																					 
	DROP TABLE IF EXISTS Slice_10mln;
	SELECT TOP 10000000 * INTO Slice_10mln FROM #ProductAvailability_StepB05_AllEvents;               
	
	-- reference
  SELECT CatalogId, ProductId, COUNT(*) OVER (PARTITION BY CatalogId, ProductId) FROM Slice_1mln;             -- 31 sec		

	DROP TABLE IF EXISTS #TestNonclustIndex;
	SELECT * INTO #TestNonclustIndex FROM Slice_1mln;                                                     
	CREATE NONCLUSTERED INDEX IX_POC_DEMO ON #TestNonclustIndex (CatalogId, ProductId);                     
	SELECT CatalogId, ProductId, COUNT(*) OVER (PARTITION BY CatalogId, ProductId) FROM #TestNonclustIndex;  -- 24 sec			
	-- check estimated plan: index is used, scan cost 13%, parrallelism (repartition streams) 33%, inner join 69%

	DROP TABLE IF EXISTS #TestClustIndex;
	SELECT * INTO #TestClustIndex FROM Slice_1mln;                                                 
	CREATE CLUSTERED INDEX IX_POC_DEMO ON #TestClustIndex (CatalogId, ProductId);                          
	SELECT CatalogId, ProductId, COUNT(*) OVER (PARTITION BY CatalogId, ProductId) FROM #TestClustIndex;     -- 24 sec		
	-- check estimated plan: index is used, scan cost 34%, parrallelism (repartition streams) 25%, inner join 52%


	-----------------------------------------
	-- Performance testing: pk - unavailable
	-----------------------------------------	
	DROP TABLE IF EXISTS #TestPK;
	SELECT * INTO #TestPK FROM Slice_1mln WHERE UserRole IS NOT NULL;                                             --  1 sec				   -- 
	ALTER TABLE #TestPK ALTER COLUMN CatalogId INT NOT NULL
	ALTER TABLE #TestPK ALTER COLUMN ProductId INT NOT NULL
	ALTER TABLE #TestPK ALTER COLUMN ProductVersionId INT NOT NULL
	ALTER TABLE #TestPK ALTER COLUMN EventType INT NOT NULL
	ALTER TABLE #TestPK ALTER COLUMN UserRole NVARCHAR(MAX) NOT NULL
  ALTER TABLE #TestPK ADD CONSTRAINT PK_double PRIMARY KEY NONCLUSTERED (CatalogId, ProductId, ProductVersionId, EventType, UserRole);      
	-- ERROR: UserRole is part of primary key but can't be used for indexing
  GO
	--doesn't work due to NULLs in UserRole
	SELECT CatalogId, ProductId, COUNT(*) OVER (PARTITION BY CatalogId, ProductId) FROM #TestPK;  --  sec				 -- 
	
	SELECT ProductId, ProductVersionId, EventType, UserRole, COUNT(*) FROM #TestPK GROUP BY ProductId, ProductVersionId, EventType, UserRole HAVING COUNT(*) > 1;

	SELECT * FROM #TestPK WHERE ProductId = 2554 AND ProductVersionId = 10189 AND EventType = 1 AND UserRole = 'Exclude from reports';


	-----------------------------------
	-- Performance testing: partitions
	-----------------------------------
	-- https://www.mssqltips.com/sqlservertip/2888/how-to-partition-an-existing-sql-server-table/

	DROP TABLE IF EXISTS TestPartitioning;                                   
	SELECT * INTO TestPartitioning FROM Slice_1mln;                                          
	
  DROP PARTITION SCHEME myPartitionScheme2
	DROP PARTITION FUNCTION myPartRangePF
	CREATE PARTITION FUNCTION myPartRangePF (INT) AS RANGE RIGHT FOR VALUES (0, 1, 2, 3, 4)
	CREATE PARTITION SCHEME myPartitionScheme2 AS PARTITION myPartRangePF ALL TO ([PRIMARY]) 
	GO
																																														    
	CREATE CLUSTERED INDEX IX_TABLE1_partitioncol ON TestPartitioning(PartitionId)                                      
		WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, 
					ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
		ON myPartitionScheme2(PartitionId)
	GO

	-- check how data distributed between partitions
	SELECT o.name objectname,i.name indexname, partition_id, partition_number, [rows]
	FROM sys.partitions p
	INNER JOIN sys.objects o ON o.object_id=p.object_id
	INNER JOIN sys.indexes i ON i.object_id=p.object_id and p.index_id=i.index_id
	WHERE o.name LIKE '%TestPartitioning%'

	SELECT PartitionId, CatalogId, ProductId, COUNT(*) OVER (PARTITION BY PartitionId, CatalogId, ProductId) FROM TestPartitioning;  	-- 0:31   

	-- Reference:
	SELECT PartitionId, CatalogId, ProductId, COUNT(*) OVER (PARTITION BY PartitionId, CatalogId, ProductId) FROM Slice_1mln;  -- 0:31

