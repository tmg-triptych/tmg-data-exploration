----------------
-- EXPLORATION
----------------

-- current product properties
-- ObjectId = primary key
SELECT TOP 10 * FROM dbMarekCommerceManager_production..CatalogContentProperty;

-- historical product state - product version
-- ObjectId = product primary key
-- WorkId   = product version primary key
SELECT TOP 10 * FROM dbMarekCommerceManager_production..ecfVersion;

-- historical product state - properties
-- ObjectId      = product primary key
-- WorkId        = product version primary key
-- MetaFieldName = property
SELECT TOP 10 * FROM dbMarekCommerceManager_production..ecfVersionProperty;

SELECT MetaFieldName, COUNT(*) FROM dbMarekCommerceManager_production..ecfVersionProperty GROUP BY MetaFieldName;

-- Is Published?
SELECT TOP 10 * FROM dbMarekCommerceManager_production..ecfVersionProperty WHERE MetaFieldName = 'Epi_IsPublished';
SELECT TOP 10 * FROM dbMarekCommerceManager_production..ecfVersionProperty WHERE MetaFieldName = 'Epi_StopPublish';
-- Is Implementation status Live
SELECT TOP 10 * FROM dbMarekCommerceManager_production..ecfVersionProperty WHERE MetaFieldName = 'ImplementationStatus';
-- Is retired?
SELECT TOP 10 * FROM dbMarekCommerceManager_production..ecfVersionProperty WHERE MetaFieldName = 'IsRetired';
-- Deny access
SELECT TOP 10 * FROM dbMarekCommerceManager_production..ecfVersionProperty WHERE MetaFieldName = 'DenyAccess';
-- Allow access
SELECT TOP 10 * FROM dbMarekCommerceManager_production..ecfVersionProperty WHERE MetaFieldName = 'AllowAccess';


