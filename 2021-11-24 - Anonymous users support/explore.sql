-- QuestionsL how DWH will handle anonymous users events: Logins and Orders - from new clients (sites 166510, 166554, 166904).

-- Orders of new clients
SELECT * FROM dbMarekEF_production..OrderHistories WHERE SiteId IN (166510, 166554, 166904);
-- ...not much right now
-- will explore in general

-- How anonymous user Orders are supported BY DWH:
-- 1. we generate "Anonymous" CS_User records for such orders, here's how much of these orders are:
SELECT 
  s.SiteName,
	COUNT(*) AS AnonymousOrdersCount
FROM DWH_CurrentStateLayer..CS_Order o
JOIN DWH_CurrentStateLayer..CS_User u ON o.ContactId = u.ContactId
JOIN DWH_CurrentStateLayer..CS_Site s ON s.SiteId = o.SiteId
WHERE u.FirstName = 'Anonymous'
GROUP BY s.SiteName
ORDER BY AnonymousOrdersCount DESC
;
-- there are such orders on many sites

-- 2. check if there still are orders referencing non-xistant user:
SELECT 
  s.SiteName,
	COUNT(*) AS AnonymousOrdersCount
FROM DWH_CurrentStateLayer..CS_Order o
LEFT JOIN DWH_CurrentStateLayer..CS_User u ON o.ContactId = u.ContactId
JOIN DWH_CurrentStateLayer..CS_Site s ON s.SiteId = o.SiteId
WHERE u.ContactId IS NULL
GROUP BY s.SiteName
ORDER BY AnonymousOrdersCount DESC
;
-- none... all is ok!

-- How anonymous user Logins are supported:
-- 3. check for supported ones
SELECT 
  s.SiteName,
	COUNT(*) AS AnonymousOrdersCount
FROM DWH_CurrentStateLayer..CS_Login l
JOIN DWH_CurrentStateLayer..CS_User u ON l.ContactId = u.ContactId
JOIN DWH_CurrentStateLayer..CS_Site s ON s.SiteId = l.SiteId
WHERE u.FirstName = 'Anonymous'
GROUP BY s.SiteName
ORDER BY AnonymousOrdersCount DESC
;
-- there are some, but probably because they'ev made some orders

-- 4. check for UNsupported ones
SELECT 
  s.SiteName,
	COUNT(*) AS AnonymousOrdersCount
FROM DWH_CurrentStateLayer..CS_Login l
LEFT JOIN DWH_CurrentStateLayer..CS_User u ON l.ContactId = u.ContactId
JOIN DWH_CurrentStateLayer..CS_Site s ON s.SiteId = l.SiteId
WHERE u.ContactId IS NULL
GROUP BY s.SiteName
ORDER BY AnonymousOrdersCount DESC
;
-- UHC	220
-- Mobile Generac MOD	3
-- Dealer Generac MOD	1
-- there are some, need to add anonymous users support


