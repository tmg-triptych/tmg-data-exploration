-- Build lineitem single product purchases table
DROP TABLE IF EXISTS ComponentLineitemPriceComparaison;
SELECT 
	ce.CatalogId,
	YEAR(oih.CreationDate) AS OrderYear,
	ce.ContentGuid,
	oih.EpiLineItemId,
	oich.Id AS oich_Id, 
	oih.Price AS oih_Price,
	oich.Price AS oich_Price,
	ccp.LongString AS ExternalPriceServiceUrl,
	CASE
		WHEN oih.Price = oich.Price THEN 'Prices match'
		WHEN ccp.MetaFieldName = 'ExternalPriceServiceUrl' THEN 'Prices discrepancy: dynamic price'
		ELSE 'Prices discrepancy: reason unknown'
	END AS CaseClass
INTO ComponentLineitemPriceComparaison
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekEF_production..OrderItemComponentsHistories oich ON oich.EpiLineItemId = oih.EpiLineItemId
LEFT JOIN dbMarekCommerceManager_production..CatalogEntry ce ON ce.ContentGuid = oih.SkuId
LEFT JOIN dbMarekCommerceManager_production..CatalogContentProperty ccp ON ccp.ObjectId = ce.CatalogEntryId AND ccp.MetaFieldName = 'ExternalPriceServiceUrl'
WHERE ce.ClassTypeId = 'Variation' -- exclude Kits, for which oih:oich may be 1:M and there's no Price equality expectation
;

-- pivot stat
SELECT
  OrderYear,
	COUNT(CASE WHEN CaseClass = 'Prices match' THEN 1 END),
	COUNT(CASE WHEN CaseClass = 'Prices discrepancy: dynamic price' THEN 1 END),
	COUNT(CASE WHEN CaseClass = 'Prices discrepancy: reason unknown' THEN 1 END)
FROM ComponentLineitemPriceComparaison
GROUP BY OrderYear
ORDER BY OrderYear
;

-- by catalog
SELECT
  c.CatalogId,
	c.Name,
	COUNT(CASE WHEN CaseClass = 'Prices match' THEN 1 END),
	COUNT(CASE WHEN CaseClass = 'Prices discrepancy: dynamic price' THEN 1 END),
	COUNT(CASE WHEN CaseClass = 'Prices discrepancy: reason unknown' THEN 1 END)
FROM ComponentLineitemPriceComparaison t
LEFT JOIN dbMarekCommerceManager_production..Catalog c ON c.CatalogId = t.CatalogId
GROUP BY c.CatalogId, c.Name
;

-- all cases with discrepancy
SELECT 
  c.Name,
	t.OrderYear,
	t.EpiLineItemId,
	t.oih_Price,
	t.oich_Price,
	t.ExternalPriceServiceUrl,
	t.CaseClass
FROM ComponentLineitemPriceComparaison t
LEFT JOIN dbMarekCommerceManager_production..Catalog c ON c.CatalogId = t.CatalogId
WHERE CaseClass != 'Prices match'
ORDER BY CaseClass, OrderYear, EpiLineItemId
;

