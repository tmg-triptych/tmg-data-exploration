-- Example 1: 148-625132 820282
SELECT
	EpiOrderId, CreationDate, SubTotal
FROM dbMarekEF_production..OrderHistories
WHERE OrderTrackId = '148-625132'
-- 820282 2021-02-02 17:49:16.657  129.52

SELECT
	EpiOrderId, EpiLineItemId, ToolId, SkuId, Price, Quantity, *
FROM dbMarekEF_production..OrderItemsHistories
WHERE EpiOrderId = 820282 
-- 820282	2082714	61AD216F-DF38-4E2A-A29B-D46DFFAE8543	0AC7A3E9-768E-405C-9D7B-9E5FB145C528	63.80	1.00
-- 820282	2082715	BE7FD9F2-90B4-42E6-89A6-113F96CEA970	6365C6E6-91A8-4804-94CF-EE1C7CB4001D	65.72	1.00

SELECT 
 Id, EpiOrderId, EpiLineItemId, ToolId, SkuId, Price, Quantity
FROM dbMarekEF_production..OrderItemComponentsHistories
WHERE EpiOrderId = 820282
-- 880929	820282	2082714	61AD216F-DF38-4E2A-A29B-D46DFFAE8543	0AC7A3E9-768E-405C-9D7B-9E5FB145C528	63.80	1.00
-- 880930	820282	2082715	BE7FD9F2-90B4-42E6-89A6-113F96CEA970	6365C6E6-91A8-4804-94CF-EE1C7CB4001D	1.00	1.00

SELECT 
  CatalogEntryId, Code
FROM dbMarekCommerceManager_production..CatalogEntry
WHERE ContentGuid = '6365C6E6-91A8-4804-94CF-EE1C7CB4001D' --- Variant
-- 73746 AH18-0047_1

SELECT * FROM dbMarekCommerceManager_production..CatalogContentProperty ccp WHERE ccp.ObjectId =  73746 AND ccp.MetaFieldName = 'ExternalPriceServiceUrl'
 
SELECT
  pg.PriceGroupId, pg.Created, pg.Modified, pv.*
FROM dbMarekCommerceManager_production..PriceGroup pg
LEFT JOIN dbMarekCommerceManager_production..PriceValue pv ON pv.PriceGroupId = pg.PriceGroupId
WHERE pg.CatalogEntryCode = 'AH18-0047_1'
;
-- 46164	2020-09-22 16:25:09.783	2020-09-22 16:25:09.783

SELECT * FROM dbMarekCommerceManager_production..PriceDetail WHERE CatalogEntryCode = 'AH18-0047_1';

-- Example 2: 148-812290 812571
SELECT
	EpiOrderId
FROM dbMarekEF_production..OrderHistories
WHERE OrderTrackId = '148-812290'
-- 812571

SELECT
	EpiOrderId, EpiLineItemId, ToolId, SkuId, Price, Quantity
FROM dbMarekEF_production..OrderItemsHistories
WHERE EpiOrderId = 812571 
-- 812571	2064000	22831951-A1BD-4EA5-9010-589E92E21DFD	A219E559-556A-4933-A9BF-2D1BDA28B0D6	65.72	1.00
SELECT 
 Id, EpiOrderId, EpiLineItemId, ToolId, SkuId, Price, Quantity
FROM dbMarekEF_production..OrderItemComponentsHistories
WHERE EpiOrderId = 812571
-- 871818	812571	2064000	22831951-A1BD-4EA5-9010-589E92E21DFD	A219E559-556A-4933-A9BF-2D1BDA28B0D6	1.00	1.00
 
SELECT * 
FROM dbMarekCommerceManager_production..CatalogEntry
WHERE ContentGuid = 'A219E559-556A-4933-A9BF-2D1BDA28B0D6' -- Variant   
-- 63140

SELECT * FROM dbMarekCommerceManager_production..CatalogContentProperty ccp WHERE ccp.ObjectId =  63140 AND ccp.MetaFieldName = 'ExternalPriceServiceUrl'

SELECT 
  *
FROM dbMarekCommerceManager_production..CatalogContentProperty
WHERE ObjectId IN (63140, 73746)
ORDER BY ObjectId
;
