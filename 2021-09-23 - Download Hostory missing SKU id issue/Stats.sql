------------------------------
-- Explore Problem frequency
------------------------------

-- Table for aggregation
DROP TABLE IF EXISTS #DhIssue_CS_OrderLineItem;
SELECT
	c.Name AS ClientName,
	ShipmentType,
	YEAR(o.OrderDate) YYYY,
	MONTH(o.OrderDate) MMMM,
	CASE
		WHEN VariationId = '00000000-0000-0000-0000-000000000000' THEN 1
	END AS IsVariationIdBroken
INTO #DhIssue_CS_OrderLineItem
FROM CS_OrderLineItem li
LEFT JOIN CS_Order o ON o.OrderId = li.OrderId
JOIN DWH_Stage_CurrentStateLayer..CS_Client c ON c.SiteId = o.SiteId

-- By clients
DROP TABLE IF EXISTS #DhIssue_Clients_stat;
SELECT
  ClientName,
	COUNT(CASE WHEN IsVariationIdBroken = 1 THEN 1 END) AS BrokenLIRecords,
	COUNT(*) AS TotalLIRecords,
	100*COUNT(CASE WHEN IsVariationIdBroken = 1 THEN 1 END)/COUNT(*) PercentageBroken
INTO #DhIssue_Clients_stat
FROM #DhIssue_CS_OrderLineItem
GROUP BY ClientName
HAVING COUNT(CASE WHEN IsVariationIdBroken = 1 THEN 1 END) > 0
;
SELECT * FROm #DhIssue_Clients_stat ORDER BY PercentageBroken DESC

-- Clients with issue data - by ShipmentType
SELECT
  ClientName,
	ShipmentType,
	COUNT(CASE WHEN IsVariationIdBroken = 1 THEN 1 END) AS BrokenLIRecords,
	COUNT(*) AS TotalLIRecords,
	100*COUNT(CASE WHEN IsVariationIdBroken = 1 THEN 1 END)/COUNT(*) PercentageBroken
FROM #DhIssue_CS_OrderLineItem
WHERE ClientName In (SELECT DISTINCT ClientName FROM #DhIssue_Clients_stat)
GROUP BY ClientName, ShipmentType
HAVING COUNT(CASE WHEN IsVariationIdBroken = 1 THEN 1 END) > 0
ORDER BY ClientName, ShipmentType


-- Clients with issue data - by Year
SELECT
  ClientName,
	YYYY,
	COUNT(CASE WHEN IsVariationIdBroken = 1 THEN 1 END) AS BrokenLIRecords,
	COUNT(*) AS TotalLIRecords,
	100*COUNT(CASE WHEN IsVariationIdBroken = 1 THEN 1 END)/COUNT(*) PercentageBroken
FROM #DhIssue_CS_OrderLineItem
WHERE ClientName In (SELECT DISTINCT ClientName FROM #DhIssue_Clients_stat)
GROUP BY ClientName, YYYY
HAVING COUNT(CASE WHEN IsVariationIdBroken = 1 THEN 1 END) > 0
ORDER BY ClientName, YYYY
