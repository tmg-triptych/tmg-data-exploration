-- Download without SkuId
SELECT * FROM dbMarekEF_production..DownloadHistories dh WHERE id = 1436524

-- Downloaded Product (from dh.ToolId)
SELECT * FROM dbMarekCommerceManager_production..CatalogEntry ce WHERE ce.ContentGuid = 'B0384682-4094-43B9-BFAB-064AB2AFB451'

-- Way to link product and Variation from Kate:
SELECT * FROM CS_VariationProduct WHERE ProductId =  'B0384682-4094-43B9-BFAB-064AB2AFB451'

--  Variant
SELECT * FROM dbMarekCommerceManager_production..CatalogEntry ce WHERE ce.CatalogEntryId = 126119
SELECT * FROM dbMarekCommerceManager_production..CatalogEntry ce WHERE ce.Name = 'EMAIL-125'


SELECT MAX(CreationDate) FROM dbMarekEF_production..DownloadHistories dh WHERE SkuId = '00000000-0000-0000-0000-000000000000'
-- 2016-11-03 19:57:33.407

SELECT MAX(CreationDate) FROM dbMarekEF_production..OrderItemsHistories oh WHERE SkuId = '00000000-0000-0000-0000-000000000000'
-- 2016-11-03 19:57:33.437