﻿-- Order Details
SELECT EpiOrderId, Total, ShippingTotal, DiscountSum FROM dbMarekEF_production..OrderHistories oh WHERE oh.OrderTrackId = '999-912393';

-- Appleid Discounts
SELECT
  oih.EpiOrderId,
  c.pkId AS OrderPromotionId,
	c2.Name
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekCommerceManager_production.dbo.LineItem li ON li.LineItemId = oih.EpiLineItemID
LEFT JOIN dbMarekCommerceManager_production..OrderForm ofd ON ofd.OrderFormId = li.OrderFormId
LEFT JOIN dbMarekCommerceManager_production..OrderFormEx ofx ON ofd.OrderFormId = ofx.ObjectId
LEFT JOIN dbMarekCommerceManager_production..PromotionInformation pin ON pin.OrderFormId = ofd.OrderFormId
LEFT JOIN dbMarekCMS_production..tblContent c ON c.ContentGUID = pin.PromotionGuid
LEFT JOIN dbMarekCMS_production..tblContentType c2 ON c2.pkId = c.fkContentTypeId
where
	oih.EpiOrderId = 912394
;
--- Applied discount properties
SELECT
	cp.fkContentID AS OrderPromotionId,
	MAX(CASE WHEN pd.Name = 'Code' THEN LongString END) Code,
	MAX(CASE WHEN pd.Name = 'CampaignLink' THEN ContentLink END) CampaignId,
	MAX(CASE WHEN pd.Name = 'Percentage' THEN String END) Percentage
FROM [dbMarekCMS_production].[dbo].[tblContentProperty] cp
LEFT JOIN [dbMarekCMS_production].[dbo].[tblPropertyDefinition] pd ON pd.pkId = cp.fkPropertyDefinitionID
WHERE pd.Name IN ('Code', 'CampaignLink', 'Amounts') AND cp.fkContentID IN (132168, 132170)
GROUP BY cp.fkContentID

-- Link Campaign to Site
SELECT 
  ContentLink as CampaignId,
  fkContentID as SiteId
FROM [dbMarekCMS_production].[dbo].[tblContentProperty]
WHERE fkPropertyDefinitionID IN (
  SELECT 
	  pkId
  FROM [dbMarekCMS_production].[dbo].[tblPropertyDefinition]
  WHERE Name = 'MarketingCampaign'
) 
AND ContentLink IN (38238, 79643)

