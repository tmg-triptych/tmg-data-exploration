﻿USE ExploreData;

-- Order-Discount with properties
DROP TABLE #DisountsAppliedToOrders
SELECT
  od.EpiOrderId,
	od.CreationDate AS OrderDate,
	YEAR(od.CreationDate) AS OrderYear,
	od.SiteId AS OrderSiteId,
	od.OrderPromotionId,
	p.Code,
	c.SiteIds AS CampaignSiteIds
INTO #DisountsAppliedToOrders
-- Orders + applied promotions
FROM (
	SELECT DISTINCT
		oih.EpiOrderId,
		oh.SiteId,
		oh.CreationDate,
		c.pkId AS OrderPromotionId
	FROM dbMarekEF_production..OrderItemsHistories oih
	LEFT JOIN dbMarekEF_production..OrderHistories oh ON oh.EpiOrderId = oih.EpiOrderId
	LEFT JOIN dbMarekCommerceManager_production.dbo.LineItem li ON li.LineItemId = oih.EpiLineItemID
	LEFT JOIN dbMarekCommerceManager_production..OrderForm ofd ON ofd.OrderFormId = li.OrderFormId
	LEFT JOIN dbMarekCommerceManager_production..OrderFormEx ofx ON ofd.OrderFormId = ofx.ObjectId
	LEFT JOIN dbMarekCommerceManager_production..PromotionInformation pin ON pin.OrderFormId = ofd.OrderFormId
	LEFT JOIN  dbMarekCMS_production..tblContent c ON c.ContentGUID = pin.PromotionGuid
	WHERE c.pkId IS NOT NULL
	--WHERE oih.EpiOrderId = 912394 -- DEBUG
) od
-- Promotion properties
LEFT JOIN (
	SELECT
		cp.fkContentID AS OrderPromotionId,
		MAX(CASE WHEN pd.Name = 'Code' THEN LongString END) Code,
		MAX(CASE WHEN pd.Name = 'CampaignLink' THEN ContentLink END) CampaignId
	FROM [dbMarekCMS_production].[dbo].[tblContentProperty] cp
	LEFT JOIN [dbMarekCMS_production].[dbo].[tblPropertyDefinition] pd ON pd.pkId = cp.fkPropertyDefinitionID
	WHERE pd.Name IN ('Code', 'CampaignLink')
	--	AND cp.fkContentID IN (132168, 132170) -- DEBUG
	GROUP BY cp.fkContentID
) p ON p.OrderPromotionId = od.OrderPromotionId
-- Campaign properties
LEFT JOIN (
	SELECT 
		ContentLink as CampaignId,
		STRING_AGG(fkContentID, ', ') as SiteIds
	FROM [dbMarekCMS_production].[dbo].[tblContentProperty]
	WHERE fkPropertyDefinitionID IN (
		SELECT 
			pkId
		FROM [dbMarekCMS_production].[dbo].[tblPropertyDefinition]
		WHERE Name = 'MarketingCampaign'
	) 
	GROUP BY ContentLink
) c ON c.CampaignId = p.CampaignId 
;
SELECT * FROM #DisountsAppliedToOrders;

-- Orders with multiple promotions
SELECT
  OrderYear, EpiOrderId, OrderSiteId, Code,
	COUNT(OrderPromotionId) as PromotionsCount,
	STRING_AGG(OrderPromotionId, ', ') AppliedPromotions,
	STRING_AGG(CampaignSiteIds, '; ') AppliedFromSites
FROM #DisountsAppliedToOrders
GROUP BY OrderYear, EpiOrderId, OrderSiteId, Code
HAVING COUNT(OrderPromotionId) > 1



---------- Second order with mult promotions

-- Order Details
SELECT EpiOrderId, Total, DiscountSum FROM dbMarekEF_production..OrderHistories oh WHERE EpiOrderId = 188638

-- Appleid Discounts
SELECT
  oih.EpiOrderId,
  c.pkId AS OrderPromotionId,
	c2.Name
FROM dbMarekEF_production..OrderItemsHistories oih
LEFT JOIN dbMarekCommerceManager_production.dbo.LineItem li ON li.LineItemId = oih.EpiLineItemID
LEFT JOIN dbMarekCommerceManager_production..OrderForm ofd ON ofd.OrderFormId = li.OrderFormId
LEFT JOIN dbMarekCommerceManager_production..OrderFormEx ofx ON ofd.OrderFormId = ofx.ObjectId
LEFT JOIN dbMarekCommerceManager_production..PromotionInformation pin ON pin.OrderFormId = ofd.OrderFormId
LEFT JOIN dbMarekCMS_production..tblContent c ON c.ContentGUID = pin.PromotionGuid
LEFT JOIN dbMarekCMS_production..tblContentType c2 ON c2.pkId = c.fkContentTypeId
where
	oih.EpiOrderId = 188638
;
--- Applied discount properties
SELECT
	cp.fkContentID AS OrderPromotionId,
	MAX(CASE WHEN pd.Name = 'Code' THEN LongString END) Code,
	MAX(CASE WHEN pd.Name = 'CampaignLink' THEN ContentLink END) CampaignId,
	MAX(CASE WHEN pd.Name = 'Percentage' THEN String END) Percentage
FROM [dbMarekCMS_production].[dbo].[tblContentProperty] cp
LEFT JOIN [dbMarekCMS_production].[dbo].[tblPropertyDefinition] pd ON pd.pkId = cp.fkPropertyDefinitionID
WHERE pd.Name IN ('Code', 'CampaignLink', 'Amounts') AND cp.fkContentID IN (24415, 24416)
GROUP BY cp.fkContentID

-- Link Campaign to Site
SELECT 
  ContentLink as CampaignId,
  fkContentID as SiteId
FROM [dbMarekCMS_production].[dbo].[tblContentProperty]
WHERE fkPropertyDefinitionID IN (
  SELECT 
	  pkId
  FROM [dbMarekCMS_production].[dbo].[tblPropertyDefinition]
  WHERE Name = 'MarketingCampaign'
) 
AND ContentLink IN (22258)


